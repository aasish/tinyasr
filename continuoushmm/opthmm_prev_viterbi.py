#!/usr/bin/env python
import math
import numpy 
try:
    import cPickle as pickle
except:
    import pprint

import argparse
import os

from string import strip
from heapq import heappush, heappop, heapify
import cProfile

observations = ('o', 'n', 'e') #k, m
 
start_probability = {}

NO_ZERO_TRANS = True 
transition_probability = { }
 
emission_probability = {
   '1' : {'o': 0.1, 'n': 0.4, 'e': 0.5},
   '2' : {'o': 0.6, 'n': 0.3, 'e': 0.1},
   '3' : {'o': 0.6, 'n': 0.3, 'e': 0.1},
   '4' : {'o': 0.6, 'n': 0.3, 'e': 0.1},
   '5' : {'o': 0.6, 'n': 0.3, 'e': 0.1}
   }

CONST = 0.000001
DIM = 36

digits = {
'0' : 'zero',
'1' : 'one',
'2' : 'two',
'3' : 'three',
'4' : 'four',
'5' : 'five',
'6' : 'six',
'7' : 'seven',
'8' : 'eight',
'9' : 'nine',
's' : 'sil',
'o' : 'oh'
}

dummy_transitions = {}
local_start_states = {}
local_end_states = {}
logpi = numpy.log(2 * math.pi)
#partial_denominator = 0.

means_array = []
covars_array = []
partdenoms_array = []

class BPNode:
    def __init__(self,previous,word,cost,time):
        self.previous = previous # previous should be a BPNode
        self.word = word
        self.cost = cost
        self.time = time


class HMM:
    def __init__(self, start_probs, trans_probs, means, covars, states):
        self.start_probs = start_probs
        self.trans_probs = trans_probs
        self.means = means
        self.covars = covars
        self.states = states

    def __init__(self, start_state, trans_probs, means, covars, partdenoms):
        self.start_state = start_state
        self.trans_probs = trans_probs
        self.means = means
        self.covars = covars
        self.partdenoms = partdenoms


def dec2log(prob):
    if prob == 0:
        return float('inf')
    return -math.log(prob)

def log2dec(logprob):
    return math.exp(logprob)


def save_model(model_file, start_probs, trans_probs, means, covars, states):
    hmm_obj = HMM(start_probs, trans_probs, means, covars, states)
    pickle.dump(hmm_obj, open(model_file, 'wb'))
    

def load_model(model_file):
    hmm_obj = pickle.load(open(model_file, 'rb'))
    return hmm_obj


def compute_emit_variables(current_obs, dynamic_states, means, covars, partdenoms):
    num_states = len(dynamic_states)

    #construct numpy arrays from dynamic means, covars, partdenoms
    
    dynamic_means = numpy.empty((num_states, DIM))
    dynamic_covars = numpy.empty((num_states, DIM))
    dynamic_partdenoms = numpy.empty((num_states))


    for i, state in enumerate(dynamic_states):
        dynamic_means[i:,] = means[state]
        dynamic_covars[i:,] = covars[state]
        dynamic_partdenoms[i] = partdenoms[state]
    
    emit_probs = calc_diag_gaussian_comb(current_obs, 
                                         dynamic_means, 
                                         dynamic_covars, 
                                         dynamic_partdenoms)

    
    del dynamic_means
    del dynamic_covars
    del dynamic_partdenoms
    #print emit_probs.shape
    return emit_probs


# in log domain
def calc_diag_gaussian(o_t, mean, covar):
    D = len(o_t)
    
    #print o_t.shape, mean.shape, covar.shape
    denominator = D * math.log(2 * math.pi) + numpy.sum(numpy.diag(covar))
    
    numerator = 0
    for i in range(D):
        numerator += math.pow((o_t[i] - mean[i]), 2)/covar[i][i]
    
    return 0.5 * (numerator + denominator)


def calc_diag_gaussian2(o_t, mean, covar): # covar is a list
    D = len(o_t)
    
    #print o_t.shape, mean.shape, covar.shape
    denominator = D * logpi + numpy.sum(covar)
    
    numerator = 0
    for i in range(D):
        numerator += ((o_t[i] - mean[i])**2)/covar[i]
    
    return 0.5 * (numerator + denominator)

def calc_partial_denominator(covar):
    return numpy.sum(covar)

def calc_diag_gaussian_comb(o_t, means, covars, partdenoms):
    D = len(o_t)
    #print o_t.shape, mean.shape, covar.shape
    denominator = D * logpi + partdenoms
    
    numerator = 0
        
    #numerator = numpy.sum((o_t - mean)**2/covar)
    tmp1 = numpy.square(o_t - means)
    tmp2 = numpy.divide(tmp1, covars)
    numerator = numpy.sum(tmp2, axis=1)
    #numerator = numpy.sum(numpy.divide(numpy.square(o_t - mean), covar))
    #numerator = numpy.sum(numpy.divide((o_t - mean)**2, covar))
    #numerator = sum([((o_t[i] - mean[i])**2)/covar[i] for i in range(D)])
    return_val = 0.5 * (numerator + denominator)
    return return_val

def calc_diag_gaussian3(o_t, mean, covar, partdenom): # covar is a list
    D = len(o_t)
    
    #print o_t.shape, mean.shape, covar.shape
    denominator = D * logpi + partdenom
    
    numerator = 0
        
    #numerator = numpy.sum((o_t - mean)**2/covar)
    tmp1 = numpy.square(o_t - mean)
    tmp2 = numpy.divide(tmp1, covar)
    numerator = numpy.sum(tmp2)
    #numerator = numpy.sum(numpy.divide(numpy.square(o_t - mean), covar))
    #numerator = numpy.sum(numpy.divide((o_t - mean)**2, covar))
    #numerator = sum([((o_t[i] - mean[i])**2)/covar[i] for i in range(D)])
    return_val = 0.5 * (numerator + denominator)
    return return_val
    



def calc_mean_diag_covar(states, obs_in_state, obs):
    means = {}
    covars = {}
    for state in states:
        x = get_obs_from_state(state, obs_in_state, obs)
        shape_of_x = x.shape
        D = shape_of_x[1]
        #print "printing covars and means"
        means[state] = numpy.mean(x, axis=0)
        covars[state] = numpy.zeros((D, D))

        for i in range(D):
            covars[state][i][i] = numpy.var(x.T[i], axis=0) + CONST
    
    return means, covars


def get_obs_from_state(state, obs_in_state, obs):
    #x = [obs[k][n] for k,n in obs_in_state if state==obs_in_state[(k,n)]]
    x = []
    for k, n in obs_in_state:
        if state == obs_in_state[(k, n)]:
            x += [obs[k][n]]
    #print x
    return numpy.array(x)

def calc_start(states, obs_in_state, obs):
    start = {}
    
    for i in obs:
        state = obs_in_state[(i, 0)]
        if state not in start:
            start[state] = 0.0
        start[state]+=1.0

    for state in states:
        if state not in start:
            start[state] = 0.0
        start[state]/=len(obs)
        
    return start

def calc_transition(states, obs_in_state, obs):
    trans = {}
    counts= {}
    
    for y0 in states:
        trans[y0] = {}
        for y in states:
            if y not in trans[y0]:
                trans[y0][y] = 0.0


    for i in range(len(obs)):
        for t in range(1, len(obs[i])):
            y0 = obs_in_state[(i, t-1)]
            y = obs_in_state[(i, t)]

            if y0 not in counts:
                counts[y0] = 0

            counts[y0] += 1.0
            trans[y0][y] += 1.0

    
    for y0 in states:
        if y0 in counts:
            count = counts[y0]
            for y in states:
                trans[y0][y]/=count
    
            #print trans

    return trans


def sequential_assign(obs, states):
    # initial segment the examples into states
    # construct the obs_in_state dictionary
    obs_in_state = {}

    for k in obs:
        k_len = len(obs[k])
        k_norm = float(k_len)/len(states)
        
        for j in range(len(obs[k])):
            new_ind = (math.floor(j/k_norm)%len(states))
            obs_in_state[(k, j)] = list(states)[int(new_ind)]
            
#    for y in obs_in_state:
#        if y[0] == 0:
#            print y[0],y[1], obs_in_state[y]
    return obs_in_state


def get_all_children2(dummy_tran, trans_p, dummy_state, dummy_parent, children):

    # catenate parents
    concat_parents = (dummy_state, ) + dummy_parent
    if dummy_state not in trans_p:
        return children

    for dummy_child in trans_p[dummy_state]:
        if dummy_child not in dummy_tran:
            children.append((dummy_child, concat_parents))
        else:
            get_all_children(dummy_tran, dummy_child, concat_parents, children)
    return children

def get_all_children(dummy_state, dummy_parent, children):

    # catenate parents
    concat_parents = (dummy_state, ) + dummy_parent
    global transition_probability
    if dummy_state not in transition_probability:
        return children

    for dummy_child in transition_probability[dummy_state]:
        if dummy_child not in dummy_transitions:
            children.append((dummy_child, concat_parents))
        else:
            get_all_children(dummy_child, concat_parents, children)
    return children


def fill_obs_from_file(files):
    obs = []
    for i in range(len(files)):
        obs.append(numpy.loadtxt(files[i]))#, usecols=(1,2,3,4,5,6,7,8,9,10,11,12))
        #print obs[i].shape
    return obs


"""
N_States: 2
Start_State: 0
Terminal_States: 1
Edge 0 1 "0"
Edge 0 1 "1"
Edge 1 0
"""
def load_fsg(fsg_lines):
    #lines = open(fsg_file).readlines()
    #lines = map(strip, lines)
    #line 1 is num states
    lines = fsg_lines
    num_states = lines[0].split(': ')[1]
    #line 2 start state
    start_state = lines[1].split(': ')[1]
    #line 3 term state
    end_state = lines[2].split(': ')[1]
    #rest are edges until penultimate line
    edge_tuples = []
    for i in range(3, len(lines)):
        #print lines[i]
        line_entries = lines[i].split(' ')
        if len(line_entries) > 3:
            label, from_state, to_state, edge_id = lines[i].split(' ')
            edge_tuples += [(from_state, to_state, edge_id.strip('"'))]
        else:
            label, from_state, to_state = lines[i].split(' ')
            edge_tuples += [(from_state, to_state, "lo")]
        
   
    return num_states, start_state, end_state, edge_tuples


"""
while ([edge_source_state, edge_end_state, edge_id] = READ(Edge)) do
    if (edge_id) then
        [n_edgestate, edge_tmat, edge_state_meanvar] = read_hmm(edge_id);
        for i = 1:n_edgestate
            transition_prob [edge_source_state, ngrammhmmstate+i] = PI_edge_model(i)
        end
        for i = 1:n_edgestate
            transition_prob [ngrammhmmstate+i,edge_end_state] = tmat_edge_model(i,edge_absobing_state)
        end
        for i = 1:n_edgestate
            grammar_state_meanvar[ngrammarhmmstate+i] = edge_state_meanvar[i]
        end
        ngrammarhmmstate += n_edgestate - 1;
    else
        transition_prob [edge_source_state,edge_end_state] = insertion_penalty
    endif
done

        self.start_probs = start_probs
        self.trans_probs = trans_probs
        self.means = means
        self.covars = covars
        self.states = states
"""



def process_fsg2(num_states, start_state, end_state, edge_tuples, digit_trans_mat, digit_means, digit_covars, digit_start_mat, insert_penalty):
#def process_fsg2(num_states, start_state, end_state, edge_tuples, model_dir, insert_penalty):

   state_id_map = {}# key: digit ids, e.g., '55'; value: state name, e.g., 'sil_0'   
   transition_prob = {}
   grammar_state_mean = {}
   grammar_state_covars = {}
   grammar_state_partdenoms = {}

   ngrammarhmmstate = num_states
   startstate = start_state
   finalstate = end_state
   dummy_tran = {}
   count = len(edge_tuples)+1 
   for edge in edge_tuples:
       edge_source_state, edge_end_state, edge_id = edge

       if edge_id != "lo":
           digit_name = digits[edge_id]
           """
           model_name = os.path.join(model_dir, digit_name+'.m')
           #print model_name
           edge_hmm = load_model(model_name)
           """
           #if edge_id == 's':
               #print edge_id, edge_hmm.trans_probs
           #edge_states = list(edge_hmm.states)
           edge_states = [0,1,2,3,4]
           #print edge_states
           
           n_edgestate = len(edge_states)

           
           for ss in range(n_edgestate):
               ss_name = digits[edge_id]+'_'+str(ss)
               ss_id = str(count)
               state_id_map[ss_id] = ss_name
               count += 1
           """
           edge_tmat = edge_hmm.trans_probs
           edge_state_mean = edge_hmm.means
           edge_state_covars = edge_hmm.covars
           edge_pi = edge_hmm.start_probs
           """
           
           edge_tmat = digit_trans_mat[digit_name]
           edge_state_mean = digit_means[digit_name]
           edge_state_covars = digit_covars[digit_name]
           edge_pi = digit_start_mat[digit_name]
           
           #print "FOR DIGIT %s" %(digit_name)
           #print edge_tmat
           
           tmp_ngram_hmm_state = str(0)
           for i in range(n_edgestate):
               
               if edge_source_state not in transition_prob:
                   transition_prob[edge_source_state] = {}
                   
               tmp_ngram_hmm_state = str(ngrammarhmmstate + i)
                   
               trans_prob_start2tmp = edge_pi[edge_states[i]] 
               #trans_prob_start2tmp = dec2log(trans_prob_start2tmp)
               if NO_ZERO_TRANS:
                   if trans_prob_start2tmp!= 0 and i==0:
                       transition_prob[edge_source_state][tmp_ngram_hmm_state] = dec2log(trans_prob_start2tmp)
                       local_start_states[tmp_ngram_hmm_state] = edge_source_state #remember where a particular local_start_state of an hmm came from
               else:
                   transition_prob[edge_source_state][tmp_ngram_hmm_state] = dec2log(trans_prob_start2tmp)
                   

           
               if tmp_ngram_hmm_state not in transition_prob:
                    transition_prob[tmp_ngram_hmm_state] = {}
               


               #for end of the HMM probability we will assign same probability to the self-loop and leave the edge-HMM
               if i == n_edgestate - 1:
                   #print i, tmp_ngram_hmm_state, edge_end_state
                   trans_prob_tmp2end = edge_tmat[edge_states[i]][edge_states[-1]]
                   #trans_prob_tmp2end = dec2log(trans_prob_tmp2end)
                   if NO_ZERO_TRANS:
                       if trans_prob_tmp2end != 0:
                           transition_prob[tmp_ngram_hmm_state][tmp_ngram_hmm_state] = dec2log(trans_prob_tmp2end)
                           #transition_prob[tmp_ngram_hmm_state][edge_end_state] = trans_prob_tmp2end
                           transition_prob[tmp_ngram_hmm_state][edge_end_state] = dec2log(insert_penalty)
                           #if digit_name == 'sil':
                           #    transition_prob[tmp_ngram_hmm_state][edge_end_state] = dec2log(1000)
                           #remember where a particular local_end_state will go to 
                           local_end_states[tmp_ngram_hmm_state] = edge_end_state 
                   else:
                       transition_prob[tmp_ngram_hmm_state][tmp_ngram_hmm_state] = dec2log(trans_prob_tmp2end)
                       transition_prob[tmp_ngram_hmm_state][edge_end_state] = dec2log(trans_prob_tmp2end)

               
               grammar_state_mean[tmp_ngram_hmm_state] = edge_state_mean[edge_states[i]]
               # only use the diag of a covars matrix 
               if len(edge_state_covars[edge_states[i]].shape) == 2:
                   grammar_state_covars[tmp_ngram_hmm_state] = numpy.diag(edge_state_covars[edge_states[i]])
               else: # 
                   grammar_state_covars[tmp_ngram_hmm_state] = edge_state_covars[edge_states[i]]
               grammar_state_partdenoms[tmp_ngram_hmm_state] = calc_partial_denominator(edge_state_covars[edge_states[i]])
                   
               ith_state = str(ngrammarhmmstate + i)
               for j in range(n_edgestate):
                   jth_state = str(ngrammarhmmstate + j)   
                   if edge_states[i] in edge_tmat:
                       if edge_states[j] in edge_tmat[edge_states[i]]:
                           trans_prob_i2j = edge_tmat[edge_states[i]][edge_states[j]]
                           #trans_prob_i2j = dec2log(trans_prob_i2j)
                           if NO_ZERO_TRANS:
                               if trans_prob_i2j!= 0:
                                   transition_prob[ith_state][jth_state] = dec2log(trans_prob_i2j)
                           else:
                               transition_prob[ith_state][jth_state] = dec2log(trans_prob_i2j)


           ngrammarhmmstate += n_edgestate

           # enumerate the list of nodes that can reach a dummy state
           if edge_end_state not in dummy_tran:
                   dummy_tran[edge_end_state] = {}
           dummy_tran[edge_end_state][tmp_ngram_hmm_state] = edge_id

       else: 
           if edge_source_state not in transition_prob:
               transition_prob[edge_source_state] = {}

           # this is for adding loops    
           transition_prob[edge_source_state][edge_end_state] = insert_penalty
           
           
           if edge_end_state not in dummy_tran:
               dummy_tran[edge_end_state] = {}
           dummy_tran[edge_end_state][edge_source_state] = edge_id
           
               
           
       #print transition_prob
   #print dummy_transitions

   total_states = count
   return total_states, state_id_map, dummy_tran,transition_prob, grammar_state_mean, grammar_state_covars, grammar_state_partdenoms
       
def process_fsg(num_states, start_state, end_state, edge_tuples, model_dir, insert_penalty):

   
   transition_prob = {}
   grammar_state_mean = {}
   grammar_state_covars = {}
   grammar_state_partdenoms = {}

   ngrammarhmmstate = num_states
   startstate = start_state
   finalstate = end_state


   for edge in edge_tuples:
       edge_source_state, edge_end_state, edge_id = edge

       if edge_id != "lo":
           digit_name = digits[edge_id]
           model_name = os.path.join(model_dir, digit_name+'.m')
           #print model_name
           edge_hmm = load_model(model_name)
           if edge_id == 's':
               print edge_id, edge_hmm.trans_probs
           edge_states = list(edge_hmm.states)
           #print edge_states
           
           n_edgestate = len(edge_states)
           edge_tmat = edge_hmm.trans_probs
           edge_state_mean = edge_hmm.means
           edge_state_covars = edge_hmm.covars
           edge_pi = edge_hmm.start_probs
           
           #print "FOR DIGIT %s" %(digit_name)
           #print edge_tmat
           
           tmp_ngram_hmm_state = str(0)
           for i in range(n_edgestate):
               
               if edge_source_state not in transition_prob:
                   transition_prob[edge_source_state] = {}
                   
               tmp_ngram_hmm_state = str(ngrammarhmmstate + i)
                   
               trans_prob_start2tmp = edge_pi[edge_states[i]] 
               #trans_prob_start2tmp = dec2log(trans_prob_start2tmp)
               if NO_ZERO_TRANS:
                   if trans_prob_start2tmp!= 0:
                       transition_prob[edge_source_state][tmp_ngram_hmm_state] = dec2log(trans_prob_start2tmp)
                       local_start_states[tmp_ngram_hmm_state] = edge_source_state #remember where a particular local_start_state of an hmm came from
               else:
                   transition_prob[edge_source_state][tmp_ngram_hmm_state] = dec2log(trans_prob_start2tmp)
                   

           
               if tmp_ngram_hmm_state not in transition_prob:
                    transition_prob[tmp_ngram_hmm_state] = {}
               


               #for end of the HMM probability we will assign same probability to the self-loop and leave the edge-HMM
               if i == n_edgestate - 1:
                   #print i, tmp_ngram_hmm_state, edge_end_state
                   trans_prob_tmp2end = edge_tmat[edge_states[i]][edge_states[-1]]
                   #trans_prob_tmp2end = dec2log(trans_prob_tmp2end)
                   if NO_ZERO_TRANS:
                       if trans_prob_tmp2end != 0:
                           transition_prob[tmp_ngram_hmm_state][tmp_ngram_hmm_state] = dec2log(trans_prob_tmp2end)
                           #transition_prob[tmp_ngram_hmm_state][edge_end_state] = trans_prob_tmp2end
                           transition_prob[tmp_ngram_hmm_state][edge_end_state] = dec2log(insert_penalty)
                           #remember where a particular local_end_state will go to 
                           local_end_states[tmp_ngram_hmm_state] = edge_end_state 
                   else:
                       transition_prob[tmp_ngram_hmm_state][tmp_ngram_hmm_state] = dec2log(trans_prob_tmp2end)
                       transition_prob[tmp_ngram_hmm_state][edge_end_state] = dec2log(trans_prob_tmp2end)

               
               grammar_state_mean[tmp_ngram_hmm_state] = edge_state_mean[edge_states[i]]
               # only use the diag of a covars matrix 
               if len(edge_state_covars[edge_states[i]].shape) == 2:
                   grammar_state_covars[tmp_ngram_hmm_state] = numpy.diag(edge_state_covars[edge_states[i]])
               else: # 
                   grammar_state_covars[tmp_ngram_hmm_state] = edge_state_covars[edge_states[i]]
               grammar_state_partdenoms[tmp_ngram_hmm_state] = calc_partial_denominator(edge_state_covars[edge_states[i]])
                   
               ith_state = str(ngrammarhmmstate + i)
               for j in range(n_edgestate):
                   jth_state = str(ngrammarhmmstate + j)   
                   if edge_states[i] in edge_tmat:
                       if edge_states[j] in edge_tmat[edge_states[i]]:
                           trans_prob_i2j = edge_tmat[edge_states[i]][edge_states[j]]
                           #trans_prob_i2j = dec2log(trans_prob_i2j)
                           if NO_ZERO_TRANS:
                               if trans_prob_i2j!= 0:
                                   transition_prob[ith_state][jth_state] = dec2log(trans_prob_i2j)
                           else:
                               transition_prob[ith_state][jth_state] = dec2log(trans_prob_i2j)


           ngrammarhmmstate += n_edgestate

           # enumerate the list of nodes that can reach a dummy state
           if edge_end_state not in dummy_transitions:
                   dummy_transitions[edge_end_state] = {}
           dummy_transitions[edge_end_state][tmp_ngram_hmm_state] = edge_id

       else: 
           if edge_source_state not in transition_prob:
               transition_prob[edge_source_state] = {}

           # this is for adding loops    
           transition_prob[edge_source_state][edge_end_state] = insert_penalty
           
           
           if edge_end_state not in dummy_transitions:
               dummy_transitions[edge_end_state] = {}
           dummy_transitions[edge_end_state][edge_source_state] = edge_id
           
               
           
       #print transition_prob
   #print dummy_transitions
   print "PROCESS_FSG:", transition_prob           
   return transition_prob, grammar_state_mean, grammar_state_covars, grammar_state_partdenoms
       

def continuous_viterbi_bp_path(obs_k, source_state, end_state, trans_p, means, covars, partdenoms, beam_width, q_len):
    #print "DUMMY_TRAN: ", dummy_transitions
    V = {}
    path = {}
    back_pointers_table = []
    back_pointer_len = 0
    active_nodes = [] # there is no dummy nodes in active_nodes
    active_parents = [] # there is dummy nodes in active_parents
    
    parent_back_pointers = {} # key: state name (str), value: index in back_pointers_table
    tmp_emit = 1
    #heappush(active_nodes, source_state)    
    active_nodes = [source_state]
    newBPNodeObj = BPNode(None, '*', 1, -1) #previous,word,cost,time

    back_pointers_table.append(newBPNodeObj)
    back_pointer_len += 1

    V[-1] = {}

    active_count = 0
    y = active_nodes[active_count]
    active_nodes = []

    V[-1][y] = tmp_emit
    path[y] = [y]
    src_children = trans_p[y].keys()
    active_parents_dict = {}
    active_parents_dict[y] = 1

    

    for src_child in src_children:
        if src_child in dummy_transitions:
            emit_children = get_all_children(src_child, (y,), [])
            for c in emit_children:
                c_node, c_ancestors = c
                active_nodes.append(c_node)
                
                #heappush(active_nodes, c_node)
                last_dummy_node = c_ancestors[0]
               	#heappush(active_parents, last_dummy_node) 
                active_parents_dict[last_dummy_node] = 1
                V[-1][last_dummy_node] = tmp_emit
                path[last_dummy_node] = path[y] + [last_dummy_node]
                #print last_dummy_node, path[last_dummy_node]
                parent_back_pointers[last_dummy_node] = 0
        else:
            #heappush(active_nodes, src_child)
            active_nodes.append(src_child)

    active_parents = active_parents_dict.keys()
    #heapify(active_parents)
    parent_back_pointers[y] = 0

    obs_len = len(obs_k)

    for t in range(0, obs_len):
        #if t == 7:
        #print "ACTIVE_NODES: ", active_nodes
        
        #print "t: ",t
        #print V
        V[t] = {}
        newpath = {}    
        #unpruned_active = []
        unpruned_active = {}
        current_back_pointers = {}
        relative_beam = 0
        active_nodes_dict = {} # used to maintain the active nodes for next iteration
        pruned_nodes = []
        # step 1. for every active node, find out the best parent,
        #calculate the prob cost, update the current_back_pointers
        #with parent_back_pointers print len(active_nodes)
        active_count = 0

        
        tmp_emit_probs = compute_emit_variables(obs_k[t], 
                                                active_nodes, 
                                                means, 
                                                covars, 
                                                partdenoms)

        
        #while (active_nodes):
        for i, y in enumerate(active_nodes):
            #y = heappop(active_nodes)
            active_count += 1
     
            #tmp_emit = calc_diag_gaussian3(obs_k[t], means[y], covars[y], partdenoms[y])
            tmp_emit = tmp_emit_probs[i]
     
            (prob, best_parent_state) = min([(V[t-1][y0] + trans_p[y0][y] + tmp_emit, y0) 
                                             for y0 in active_parents if y in trans_p[y0]])
      
            V[t][y] = prob
            #print path
            #print best_parent_state, y
            newpath[y] = path[best_parent_state] + [y]
            current_back_pointers[y] = parent_back_pointers[best_parent_state]
            
            #heappush(unpruned_active,(int(prob), y))
            if y in unpruned_active:
                if unpruned_active[y] > prob:
                    unpruned_active[y] = prob
            else:
                unpruned_active[y] = prob
        path = newpath
        # step 2. prune the active nodes with relative beam 
        #print len(unpruned_active)
      
        count = 0
        len_unpruned = len(unpruned_active)
        dummy_nodes = []
        active_parents = []    

        for current_node in sorted(unpruned_active, key=unpruned_active.get):
            node_cost = unpruned_active[current_node]
            if count == 0:
                relative_beam = node_cost + beam_width
            
            if node_cost > relative_beam or count > q_len:
                break
            pruned_nodes.append(current_node)
            count += 1 
        #print count, len_unpruned
        # step 3. expand the pruned nodes to their children        
        # update active_parents, active_nodes
        #for node in pruned_nodes:
            #heappush(active_parents,node)
            active_parents.append(current_node)
            #children = trans_p[node].keys()
            for child in trans_p[current_node]:
                #if child == '1':
                    #print "find child 1"
                if child not in dummy_transitions:
                    active_nodes_dict[child] = 1
                else:
                    dummy_nodes.append(child)

        

        #print "DUMMY NODES:", dummy_nodes
        # create a new entry in back_pointers_table
        for dummy_node in dummy_nodes:
            # find out the best parent entering this dummy node
            # create a new entry in back_pointers_table
            # point the current_back_pointers to itself
         

            (prob, best_parent_state) = min([(V[t][y0] + trans_p[y0][dummy_node], y0) 
                                             for y0 in pruned_nodes if dummy_node in trans_p[y0]])

            previousBPNodeIndex = current_back_pointers[best_parent_state]
            previousBPNode = back_pointers_table[previousBPNodeIndex]
            word = dummy_transitions[dummy_node][best_parent_state]
            newBPNodeObj = BPNode(previousBPNode, word, prob, t)
            back_pointers_table.append(newBPNodeObj)
            back_pointer_len += 1
            #print "created a new BPNode", previousBPNode.word, "->", word

            current_back_pointers[dummy_node] = back_pointer_len - 1
            #if dummy_node == '1':
            #    print len(back_pointers_table)
            
            # find out all paths towards normal nodes
            children = []
            dummy_node_children = get_all_children(dummy_node, (best_parent_state,), children)

            #print "dummy node:", dummy_node
            #print "dummy node children: ", dummy_node_children
            #print len(dummy_node_children)
            last_dummy_node_dict = {}
            # add that normal node to active_nodes for next iteration/observation
            # update the backpointers for nodes in that path
            for child in dummy_node_children:
                child_node, ancestors = child
                active_nodes_dict[child_node] = 1    
                for i, e in reversed(list(enumerate(ancestors[:-1]))):
                    if i > 0:
                        prob += trans_p[ancestors[i]][ancestors[i-1]]
                        current_back_pointers[ancestors[i-1]] = current_back_pointers[ancestors[i]]
                        
                # add the last dummy node to V[t][]
                last_dummy_node = ancestors[0]
                #print best_parent_state, last_dummy_node
                newpath[last_dummy_node] = newpath[best_parent_state] + [last_dummy_node]
                
                #if (last_dummy_node == '2'):
                #    print newpath[last_dummy_node]
                V[t][last_dummy_node] = prob
                # add the last dummy node to the parent_nodes for next iteration
                last_dummy_node_dict[last_dummy_node] = 1
            # add last_dummy_node_dict to active_parents
            for ldn in last_dummy_node_dict:
                #heappush(active_parents, ldn)
                active_parents.append(ldn)
             


        # prepare for the next iteration
        # heapify the active_nodes_dict to active_nodes
        # active_parents is updated already
        # current_back_pointers become parent_back_pointers
        active_nodes = active_nodes_dict.keys()
        #print "total active nodes seen in this timestamp %d" %(active_count)
        #print len(active_nodes)
        #heapify(active_nodes)
        parent_back_pointers = current_back_pointers
        path = newpath        
    # process the final result

    (prob, state) = min([(V[obs_len-1][y], y) for y in V[obs_len-1]])
    #print path
    print "BEST_PATH", end_state, path[state]
    print "BEST_PROB", prob
    result = backtrace(back_pointers_table, obs_len)    
    result = ' '.join(result)
    return result

def continuous_viterbi_bp_path2(state_id_map, dummy_tran, obs_k, source_state, end_state, trans_p, means, covars, partdenoms, beam_width, q_len):
    V = {}
    path = {}
    back_pointers_table = []
    back_pointer_len = 0
    active_nodes = [] # there is no dummy nodes in active_nodes
    active_parents = [] # there is dummy nodes in active_parents
    
    parent_back_pointers = {} # key: state name (str), value: index in back_pointers_table
    tmp_emit = 1
    #heappush(active_nodes, source_state)    
    active_nodes = [source_state]
    newBPNodeObj = BPNode(None, '*', 1, -1) #previous,word,cost,time

    back_pointers_table.append(newBPNodeObj)
    back_pointer_len += 1

    V[-1] = {}

    active_count = 0
    y = active_nodes[active_count]
    active_nodes = []

    V[-1][y] = tmp_emit
    path[y] = []
    src_children = trans_p[y].keys()
    active_parents_dict = {}
    active_parents_dict[y] = 1

    

    for src_child in src_children:
        if src_child in dummy_tran:
            emit_children = get_all_children2(dummy_tran,trans_p,src_child, (y,), [])
            for c in emit_children:
                c_node, c_ancestors = c
                active_nodes.append(c_node)
                
                #heappush(active_nodes, c_node)
                last_dummy_node = c_ancestors[0]
               	#heappush(active_parents, last_dummy_node) 
                active_parents_dict[last_dummy_node] = 1
                V[-1][last_dummy_node] = tmp_emit

                #path[last_dummy_node] = path[y] + [last_dummy_node]
                path[last_dummy_node] = path[y]
                #print last_dummy_node, path[last_dummy_node]
                parent_back_pointers[last_dummy_node] = 0
        else:
            #heappush(active_nodes, src_child)
            active_nodes.append(src_child)

    active_parents = active_parents_dict.keys()
    #heapify(active_parents)
    parent_back_pointers[y] = 0

    obs_len = len(obs_k)

    for t in range(0, obs_len):
        #if t == 7:
        #print "ACTIVE_NODES: ", active_nodes
        
        #print "t: ",t
        #print V
        V[t] = {}
        newpath = {}    
        #unpruned_active = []
        unpruned_active = {}
        current_back_pointers = {}
        relative_beam = 0
        active_nodes_dict = {} # used to maintain the active nodes for next iteration
        pruned_nodes = []
        # step 1. for every active node, find out the best parent,
        #calculate the prob cost, update the current_back_pointers
        #with parent_back_pointers print len(active_nodes)
        active_count = 0

        #print active_nodes
        #print state_id_map
        tmp_emit_probs = compute_emit_variables(obs_k[t], 
                                                active_nodes, 
                                                means, 
                                                covars, 
                                                partdenoms)
        
        
        #while (active_nodes):
        for i, y in enumerate(active_nodes):
            #y = heappop(active_nodes)
            active_count += 1
     
            #tmp_emit = calc_diag_gaussian3(obs_k[t], means[y], covars[y], partdenoms[y])
            tmp_emit = tmp_emit_probs[i]
     
            (prob, best_parent_state) = min([(V[t-1][y0] + trans_p[y0][y] + tmp_emit, y0) 
                                             for y0 in active_parents if y in trans_p[y0]])
      
            V[t][y] = prob
            #print path
            #print best_parent_state, y
            newpath[y] = path[best_parent_state] + [state_id_map[y]]
            current_back_pointers[y] = parent_back_pointers[best_parent_state]
            
            #heappush(unpruned_active,(int(prob), y))
            if y in unpruned_active:
                if unpruned_active[y] > prob:
                    unpruned_active[y] = prob
            else:
                unpruned_active[y] = prob
        path = newpath
        # step 2. prune the active nodes with relative beam 
        #print len(unpruned_active)
      
        count = 0
        len_unpruned = len(unpruned_active)
        dummy_nodes = []
        active_parents = []    

        #for current_node in sorted(unpruned_active, key=unpruned_active.get):
        for current_node in unpruned_active:
            node_cost = unpruned_active[current_node]
            if count == 0:
                relative_beam = node_cost + beam_width
            
         #   if node_cost > relative_beam or count > q_len:
         #       break
            pruned_nodes.append(current_node)
            count += 1 
        #print count, len_unpruned
        # step 3. expand the pruned nodes to their children        
        # update active_parents, active_nodes
        #for node in pruned_nodes:
            #heappush(active_parents,node)
            active_parents.append(current_node)
            #children = trans_p[node].keys()
            for child in trans_p[current_node]:
                #if child == '1':
                #    print "find child 1"
                if child not in dummy_tran:
                    active_nodes_dict[child] = 1
                else:
                    dummy_nodes.append(child)

        

        # create a new entry in back_pointers_table
        #print "DUMMY NODES:", dummy_nodes
        for dummy_node in dummy_nodes:
            # find out the best parent entering this dummy node
            # create a new entry in back_pointers_table
            # point the current_back_pointers to itself
         

            (prob, best_parent_state) = min([(V[t][y0] + trans_p[y0][dummy_node], y0) 
                                             for y0 in pruned_nodes if dummy_node in trans_p[y0]])

            previousBPNodeIndex = current_back_pointers[best_parent_state]
            previousBPNode = back_pointers_table[previousBPNodeIndex]
            word = dummy_tran[dummy_node][best_parent_state]
            newBPNodeObj = BPNode(previousBPNode, word, prob, t)
            back_pointers_table.append(newBPNodeObj)
            back_pointer_len += 1
            #print "created a new BPNode", previousBPNode.word, "->", word

            current_back_pointers[dummy_node] = back_pointer_len - 1
            #if dummy_node == '1':
            #    print len(back_pointers_table)
            
            # find out all paths towards normal nodes
            children = []
            dummy_node_children = get_all_children2(dummy_tran,trans_p, dummy_node, (best_parent_state,), children)
            #print "dummy node:", dummy_node
            #print "dummy node children: ", dummy_node_children
            #print len(dummy_node_children)
            last_dummy_node_dict = {}
            # add that normal node to active_nodes for next iteration/observation
            # update the backpointers for nodes in that path
            for child in dummy_node_children:
                child_node, ancestors = child
                active_nodes_dict[child_node] = 1    
                for i, e in reversed(list(enumerate(ancestors[:-1]))):
                    if i > 0:
                        prob += trans_p[ancestors[i]][ancestors[i-1]]
                        current_back_pointers[ancestors[i-1]] = current_back_pointers[ancestors[i]]
                        
                # add the last dummy node to V[t][]
                last_dummy_node = ancestors[0]
                #print best_parent_state, last_dummy_node

                #newpath[last_dummy_node] = newpath[best_parent_state] + [last_dummy_node]
                newpath[last_dummy_node] = newpath[best_parent_state]
                
                #if (last_dummy_node == '2'):
                #    print newpath[last_dummy_node]
                V[t][last_dummy_node] = prob
                # add the last dummy node to the parent_nodes for next iteration
                last_dummy_node_dict[last_dummy_node] = 1
            # add last_dummy_node_dict to active_parents
            for ldn in last_dummy_node_dict:
                #heappush(active_parents, ldn)
                active_parents.append(ldn)
             


        # prepare for the next iteration
        # heapify the active_nodes_dict to active_nodes
        # active_parents is updated already
        # current_back_pointers become parent_back_pointers
        active_nodes = active_nodes_dict.keys()
        #print "total active nodes seen in this timestamp %d" %(active_count)
        #print len(active_nodes)
        #heapify(active_nodes)
        parent_back_pointers = current_back_pointers
        path = newpath        
    # process the final result


    final_path = path[end_state]
    
    del V 
    del back_pointers_table
    del active_nodes
    del active_parents
    del parent_back_pointers
    del path
    
    
    return prob, final_path


def backtrace(back_pointers_table, len_obs):
    last_node = back_pointers_table[-1]

    last_node_id = len(back_pointers_table) - 1
    min_cost = 9999999
    while (back_pointers_table[last_node_id].time == len_obs-1):       
        if back_pointers_table[last_node_id].cost < min_cost:
            last_node = back_pointers_table[last_node_id]
            min_cost = last_node.cost
        last_node_id -= 1

    previous = last_node.previous
    result = []

    while previous:
        result.append(last_node.word)
        last_node = previous
        previous = last_node.previous

    # reverse the result list
    result.reverse()
    return result

def print_normal_path(path):
    result = []
    for i in range(len(path)-1):
        #print i, i+1
        #print path[i], path[i+1]
        if path[i+1] in dummy_transitions:
            if path[i] in dummy_transitions[path[i+1]]:
                word = dummy_transitions[path[i+1]][path[i]]
                if word != "lo":
                    result.append(word)
    return ' '.join(result)



def init_models(fsg_file, model_dir, insert_penalty):
    num_states, start_state, end_state, edge_tuples = load_fsg(fsg_file)
    num_states = int(num_states)
    print num_states, start_state, end_state, edge_tuples 
    trans_probs, means, covars, partdenoms = process_fsg(num_states, 
                                             start_state, 
                                             end_state, 
                                             edge_tuples,
                                             model_dir,
                                             insert_penalty)

    big_hmm_model = HMM(start_state, trans_probs, means, covars, partdenoms)
    #print trans_probs    
    return big_hmm_model

def batch_decode(ctl_file, back_pointer_flag, big_hmm_model, beam_width, q_length):
    test_files = open(ctl_file).readlines()
    test_files = map(strip, test_files)
    obs = fill_obs_from_file(test_files)
    
    start_state = big_hmm_model.start_state
    trans_probs = big_hmm_model.trans_probs    
    means = big_hmm_model.means
    covars = big_hmm_model.covars
    partdenoms = big_hmm_model.partdenoms

    global transition_probability
    transition_probability = trans_probs

    for i, o in enumerate(obs):
        if back_pointer_flag:
            result = continuous_viterbi_bp_path(o, 
                                           start_state, 
                                           '70', 
                                           trans_probs, 
                                           means, 
                                           covars, 
                                           partdenoms,
                                           beam_width, q_length)
            print "%s\t(test-%d)" %(result, i+1)
        
        else:
            result = continuous_viterbi(o, 
                                        start_state, 
                                        trans_probs, 
                                        means, 
                                        covars, 
                                        beam_width)
        #yield result

           
def live_decode(test_file, back_pointer_flag, big_hmm_model, beam_width, q_length):

    obs = fill_obs_from_file([test_file])

    result = ""

                                                                                        
    start_state = big_hmm_model.start_state
    trans_probs = big_hmm_model.trans_probs    
    means = big_hmm_model.means
    covars = big_hmm_model.covars
    partdenoms = big_hmm_model.partdenoms
    # test to pass only diag covars


    global transition_probability
    transition_probability = trans_probs
        
    if back_pointer_flag:
        result = continuous_viterbi_bp(obs[0], 
                                       start_state, 
                                       trans_probs, 
                                       means, 
                                       covars, 
                                       partdenoms,
                                       beam_width, q_length)
    else:
        result = continuous_viterbi(obs[0], 
                                    start_state, 
                                    trans_probs, 
                                    means, 
                                    covars, 
                                    beam_width)

    
    return result
    

def main():
    
    parse = argparse.ArgumentParser()
    #parse.add_argument('-l',  '--learn', action="store_true", dest="learn", default=False, help='training mode=True')
    parse.add_argument('-m','--model', action="store", dest="model", default=2, help='some model name')
    parse.add_argument('-k','--modeldir', action="store", dest="modeldir", default=2, help='set modeldir to some path')
    parse.add_argument('-g', '--fsgfile', action="store", dest="fsgfile", help='set fsg file')
    parse.add_argument('-d', '--dir', action="store", dest="train_dir", help="train_directory")
    parse.add_argument('-t', '--test_file', action="store", dest="test_file", default='', help="test file")
    parse.add_argument('-s', '--num-states', action="store", dest="num_states", default=3, help="num states")
    parse.add_argument('-b', '--beam-width', action="store", dest="beam_width", default=2, help="beam width")
    parse.add_argument('-i', '--insert-penalty', action="store", dest="insert_penalty", default=45, help="insert penalty")
    parse.add_argument('-p', '--back-pointer', action="store_true", dest="back_pointer", default=False, help="enable back pointers")
    parse.add_argument('-c', '--ctl-file', action='store', dest='ctl_file')
    parse.add_argument('-q', '--q-length', action='store', dest='q_length', default=100)

    res = parse.parse_args()
    #train = res.learn
    num_states = int(res.num_states)
    

    model_file = res.model
    test_file = res.test_file
    beam_width = float(res.beam_width)
    q_length = int(res.q_length)
    insert_penalty = float(res.insert_penalty)
    back_pointer_flag = res.back_pointer   
 
    model_dir = res.modeldir
    fsg_file = res.fsgfile
    
            
    
    big_hmm_model = init_models(fsg_file, model_dir, insert_penalty)
    if res.ctl_file:
        batch_decode(res.ctl_file, back_pointer_flag, big_hmm_model, beam_width, q_length)
        
    else:
        result = live_decode(test_file, back_pointer_flag, big_hmm_model, beam_width, q_length)
        print result


if __name__ == "__main__":
    #cProfile.run('main()')
    main()
