#!/usr/bin/env python
import math
import numpy 
try:
    import cPickle as pickle
except:
    import pprint

import argparse
import os
from clint.textui import colored
from string import strip
from heapq import heappush, heappop, heapify

observations = ('o', 'n', 'e') #k, m
 
start_probability = {}

NO_ZERO_TRANS = True 
transition_probability = { }
 
emission_probability = {
   '1' : {'o': 0.1, 'n': 0.4, 'e': 0.5},
   '2' : {'o': 0.6, 'n': 0.3, 'e': 0.1},
   '3' : {'o': 0.6, 'n': 0.3, 'e': 0.1},
   '4' : {'o': 0.6, 'n': 0.3, 'e': 0.1},
   '5' : {'o': 0.6, 'n': 0.3, 'e': 0.1}
   }

CONST = 0.000001

digits = {
'0' : 'zero',
'1' : 'one',
'2' : 'two',
'3' : 'three',
'4' : 'four',
'5' : 'five',
'6' : 'six',
'7' : 'seven',
'8' : 'eight',
'9' : 'nine',
'sil' : 'sil'
}

dummy_transitions = {}
local_start_states = {}
local_end_states = {}

class BPNode:
    def __init__(self,previous,word,cost,time):
        self.previous = previous # previous should be a BPNode
        self.word = word
        self.cost = cost
        self.time = time


class HMM:
    def __init__(self, start_probs, trans_probs, means, covars, states):
        self.start_probs = start_probs
        self.trans_probs = trans_probs
        self.means = means
        self.covars = covars
        self.states = states

    def __init__(self, start_state, trans_probs, means, covars):
        self.start_state = start_state
        self.trans_probs = trans_probs
        self.means = means
        self.covars = covars


def dec2log(prob):
    if prob == 0:
        return float('inf')
    return -math.log(prob)

def log2dec(logprob):
    return math.exp(logprob)


def save_model(model_file, start_probs, trans_probs, means, covars, states):
    hmm_obj = HMM(start_probs, trans_probs, means, covars, states)
    pickle.dump(hmm_obj, open(model_file, 'wb'))
    

def load_model(model_file):
    hmm_obj = pickle.load(open(model_file, 'rb'))
    return hmm_obj


# in log domain
def calc_diag_gaussian(o_t, mean, covar):
    D = len(o_t)
    
    #print o_t.shape, mean.shape, covar.shape
    denominator = D * math.log(2 * math.pi) + numpy.sum(numpy.diag(covar))
    
    numerator = 0
    for i in range(D):
        numerator += math.pow((o_t[i] - mean[i]), 2)/covar[i][i]
    
    return 0.5 * (numerator + denominator)


def calc_mean_diag_covar(states, obs_in_state, obs):
    means = {}
    covars = {}
    for state in states:
        x = get_obs_from_state(state, obs_in_state, obs)
        shape_of_x = x.shape
        D = shape_of_x[1]
        #print "printing covars and means"
        means[state] = numpy.mean(x, axis=0)
        covars[state] = numpy.zeros((D, D))

        for i in range(D):
            covars[state][i][i] = numpy.var(x.T[i], axis=0) + CONST
    
    return means, covars


def get_obs_from_state(state, obs_in_state, obs):
    #x = [obs[k][n] for k,n in obs_in_state if state==obs_in_state[(k,n)]]
    x = []
    for k, n in obs_in_state:
        if state == obs_in_state[(k, n)]:
            x += [obs[k][n]]
    #print x
    return numpy.array(x)

def calc_start(states, obs_in_state, obs):
    start = {}
    
    for i in obs:
        state = obs_in_state[(i, 0)]
        if state not in start:
            start[state] = 0.0
        start[state]+=1.0

    for state in states:
        if state not in start:
            start[state] = 0.0
        start[state]/=len(obs)
        
    return start

def calc_transition(states, obs_in_state, obs):
    trans = {}
    counts= {}
    
    for y0 in states:
        trans[y0] = {}
        for y in states:
            if y not in trans[y0]:
                trans[y0][y] = 0.0


    for i in range(len(obs)):
        for t in range(1, len(obs[i])):
            y0 = obs_in_state[(i, t-1)]
            y = obs_in_state[(i, t)]

            if y0 not in counts:
                counts[y0] = 0

            counts[y0] += 1.0
            trans[y0][y] += 1.0

    
    for y0 in states:
        if y0 in counts:
            count = counts[y0]
            for y in states:
                trans[y0][y]/=count
    
            #print trans

    return trans


def sequential_assign(obs, states):
    # initial segment the examples into states
    # construct the obs_in_state dictionary
    obs_in_state = {}

    for k in obs:
        k_len = len(obs[k])
        k_norm = float(k_len)/len(states)
        
        for j in range(len(obs[k])):
            new_ind = (math.floor(j/k_norm)%len(states))
            obs_in_state[(k, j)] = list(states)[int(new_ind)]
            
#    for y in obs_in_state:
#        if y[0] == 0:
#            print y[0],y[1], obs_in_state[y]
    return obs_in_state


def get_all_children(dummy_state, dummy_parent, children):

    # catenate parents
    concat_parents = (dummy_state, ) + dummy_parent
    global transition_probability
    if dummy_state not in transition_probability:
        return children

    for dummy_child in transition_probability[dummy_state]:
        if dummy_child not in dummy_transitions:
            children.append((dummy_child, concat_parents))
        else:
            get_all_children(dummy_child, concat_parents, children)
    return children


def fill_obs_from_file(files):
    obs = []
    for i in range(len(files)):
        obs.append(numpy.loadtxt(files[i]))#, usecols=(1,2,3,4,5,6,7,8,9,10,11,12))
        #print obs[i].shape
    return obs


"""
N_States: 2
Start_State: 0
Terminal_States: 1
Edge 0 1 "0"
Edge 0 1 "1"
Edge 1 0
"""
def load_fsg(fsg_file):
    lines = open(fsg_file).readlines()
    lines = map(strip, lines)
    #line 1 is num states
    num_states = lines[0].split(': ')[1]
    #line 2 start state
    start_state = lines[1].split(': ')[1]
    #line 3 term state
    end_state = lines[2].split(': ')[1]
    #rest are edges until penultimate line
    edge_tuples = []
    for i in range(3, len(lines)):
        line_entries = lines[i].split(' ')
        if len(line_entries) > 3:
            label, from_state, to_state, edge_id = lines[i].split(' ')
            edge_tuples += [(from_state, to_state, edge_id.strip('"'))]
        else:
            label, from_state, to_state = lines[i].split(' ')
            edge_tuples += [(from_state, to_state, "lo")]
        
   
    return num_states, start_state, end_state, edge_tuples


"""
while ([edge_source_state, edge_end_state, edge_id] = READ(Edge)) do
    if (edge_id) then
        [n_edgestate, edge_tmat, edge_state_meanvar] = read_hmm(edge_id);
        for i = 1:n_edgestate
            transition_prob [edge_source_state, ngrammhmmstate+i] = PI_edge_model(i)
        end
        for i = 1:n_edgestate
            transition_prob [ngrammhmmstate+i,edge_end_state] = tmat_edge_model(i,edge_absobing_state)
        end
        for i = 1:n_edgestate
            grammar_state_meanvar[ngrammarhmmstate+i] = edge_state_meanvar[i]
        end
        ngrammarhmmstate += n_edgestate - 1;
    else
        transition_prob [edge_source_state,edge_end_state] = insertion_penalty
    endif
done

        self.start_probs = start_probs
        self.trans_probs = trans_probs
        self.means = means
        self.covars = covars
        self.states = states
"""
def process_fsg(num_states, start_state, end_state, edge_tuples, model_dir, insert_penalty):

   
   transition_prob = {}
   grammar_state_mean = {}
   grammar_state_covars = {}

   ngrammarhmmstate = num_states
   startstate = start_state
   finalstate = end_state

   for edge in edge_tuples:
       edge_source_state, edge_end_state, edge_id = edge

       if edge_id != "lo":
           digit_name = digits[edge_id]
           model_name = os.path.join(model_dir, digit_name+'.m')
           #print model_name
           edge_hmm = load_model(model_name)
           edge_states = list(edge_hmm.states)
           #print edge_states
           
           n_edgestate = len(edge_states)
           edge_tmat = edge_hmm.trans_probs
           edge_state_mean = edge_hmm.means
           edge_state_covars = edge_hmm.covars
           edge_pi = edge_hmm.start_probs
           
           #print "FOR DIGIT %s" %(digit_name)
           #print edge_tmat
           
           tmp_ngram_hmm_state = str(0)
           for i in range(n_edgestate):
               
               if edge_source_state not in transition_prob:
                   transition_prob[edge_source_state] = {}
                   
               tmp_ngram_hmm_state = str(ngrammarhmmstate + i)
                   
               trans_prob_start2tmp = edge_pi[edge_states[i]]               
               if NO_ZERO_TRANS:
                   if trans_prob_start2tmp!= 0:
                       transition_prob[edge_source_state][tmp_ngram_hmm_state] = trans_prob_start2tmp
                       local_start_states[tmp_ngram_hmm_state] = edge_source_state #remember where a particular local_start_state of an hmm came from
               else:
                   transition_prob[edge_source_state][tmp_ngram_hmm_state] = trans_prob_start2tmp
                   

           
               if tmp_ngram_hmm_state not in transition_prob:
                    transition_prob[tmp_ngram_hmm_state] = {}
               


               #for end of the HMM probability we will assign same probability to the self-loop and leave the edge-HMM
               if i == n_edgestate - 1:
                   #print i, tmp_ngram_hmm_state, edge_end_state
                   trans_prob_tmp2end = edge_tmat[edge_states[i]][edge_states[-1]]
                   if NO_ZERO_TRANS:
                       if trans_prob_tmp2end != 0:
                           transition_prob[tmp_ngram_hmm_state][tmp_ngram_hmm_state] = trans_prob_tmp2end
                           #transition_prob[tmp_ngram_hmm_state][edge_end_state] = trans_prob_tmp2end
                           transition_prob[tmp_ngram_hmm_state][edge_end_state] = insert_penalty
                           #remember where a particular local_end_state will go to 
                           local_end_states[tmp_ngram_hmm_state] = edge_end_state 
                   else:
                       transition_prob[tmp_ngram_hmm_state][tmp_ngram_hmm_state] = trans_prob_tmp2end 
                       transition_prob[tmp_ngram_hmm_state][edge_end_state] = trans_prob_tmp2end

               
               grammar_state_mean[tmp_ngram_hmm_state] = edge_state_mean[edge_states[i]]
               # only use the diag of a covars matrix 
               grammar_state_covars[tmp_ngram_hmm_state] = numpy.diag(edge_state_covars[edge_states[i]])

                   
               ith_state = str(ngrammarhmmstate + i)
               for j in range(n_edgestate):
                   jth_state = str(ngrammarhmmstate + j)   
                   if edge_states[i] in edge_tmat:
                       if edge_states[j] in edge_tmat[edge_states[i]]:
                           trans_prob_i2j = edge_tmat[edge_states[i]][edge_states[j]]
                           if NO_ZERO_TRANS:
                               if trans_prob_i2j!= 0:
                                   transition_prob[ith_state][jth_state] = trans_prob_i2j
                           else:
                               transition_prob[ith_state][jth_state] = trans_prob_i2j


           ngrammarhmmstate += n_edgestate

           # enumerate the list of nodes that can reach a dummy state
           if edge_end_state not in dummy_transitions:
                   dummy_transitions[edge_end_state] = {}
           dummy_transitions[edge_end_state][tmp_ngram_hmm_state] = edge_id

       else: 
           if edge_source_state not in transition_prob:
               transition_prob[edge_source_state] = {}

           # this is for adding loops    
           transition_prob[edge_source_state][edge_end_state] = insert_penalty 

           
           if edge_end_state not in dummy_transitions:
               dummy_transitions[edge_end_state] = {}
           dummy_transitions[edge_end_state][edge_source_state] = edge_id
           
               
           
       #print transition_prob
   #print dummy_transitions
               
   return transition_prob, grammar_state_mean, grammar_state_covars
       

def continuous_viterbi(obs_k, source_state, trans_p, means, covars, beam_width):
    V = {}
    path = {}
    #print trans_p
    #print "LOCAL START STATES"
    #print local_start_states
    #print "LOCAL END STATES"
    #print local_end_states

    active_nodes = []
    active_parents = []

        
    tmp_emit = 1
    heappush(active_nodes, source_state)

    V[-1] = {}

    y = heappop(active_nodes)
    V[-1][y] = tmp_emit
    path[y] = [y]
    src_children = trans_p[y].keys()
    for src_child in src_children:
        heappush(active_nodes, src_child)

    active_parents = [y]


    #print "LENGTH OF OBSERVATION IS %d" %(len(obs_k))
    for t in range(0,len(obs_k)):
        #print "CURRENTLY LOOKING AT vector %d of %d" %(t, len(obs_k))
        V[t] = {}
        newpath = {}
        
        #unpruned_active = []
        unpruned_active = {}
        #print "length of active nodes %d" %(len(active_nodes))
        active_count = 0
        while active_nodes:
            y = heappop(active_nodes)
            active_count += 1
            #print "length of active nodes %d" %(len(active_nodes))
            tmp_emit = 0
            
            # only when y is emitting node
            if y not in dummy_transitions:
                tmp_emit = calc_diag_gaussian2(obs_k[t], means[y], covars[y])
            
        
            # otherwise do something else
            # expand the node to children otherwise we will lose an observation
            # expand all the children from the dummy node and add them to active_nodes
            # then the transition probability for this node will be different
            
            
            (prob, parent_state) = min([(V[t-1][y0] + dec2log(trans_p[y0][y])  + tmp_emit, y0) for y0 in active_parents if y in trans_p[y0]])
            
            # if a node is dummy then we can't compute emit scores so
            # we will do the following: expand all the possible
            # children exhaustively (i.e. until no dummy node is left)
            # then add them to the queue right away
            
            if y in dummy_transitions:
                children = []
                
                dummy_node_children = get_all_children(y, (parent_state, ), children)

                # each child is a tuple with backpointer to its
                # parent(dummy) node and the previous emission node
                for child in dummy_node_children:
                    #create transition probability from existing
                    #active_parents to new child
                    #trans_p[active_parent][child]
                    child_node, ancestors = child
                
                    #print child_node, ancestors
                    #print y, child_node, trans_p[y].keys()
                    trans_p[ancestors[-1]][child_node] = trans_p[ancestors[0]][child_node]


                    if len(ancestors) > 2:
                        #current_back_pointers[] = parent_back_pointers[ancestors[-1]]
                    
                        for i, e in reversed(list(enumerate(ancestors[:-1]))):
                            if i > 0:
                                trans_p[ancestors[-1]][child_node] += trans_p[ancestors[i]][ancestors[i-1]]
                                #print ancestors[i-1],ancestors[i]
                   
                    heappush(active_nodes, child_node)
                #print "length of active nodes %d" %(len(active_nodes))    


            #(prob, state) = max([(V[t-1][y0] * trans_p[y0][y] *
            #tmp_emit, y0) for y0 in states]) 1. update the score for
            #the current node this should not be updated for a dummy
            #node but then everything ends at the dummy node.
            V[t][y] = prob
            if y not in dummy_transitions:
                #print y
                #heappush(unpruned_active, (int(prob), y))
                if y in unpruned_active:
                    if unpruned_active[y] > int(prob):
                        unpruned_active[y] = int(prob)
                else:
                    unpruned_active[y] = int(prob)

            
            # update the path for the current node
            #if we are leaving an HMM then we have to insert intermediate dummy states
            if parent_state in local_end_states and y in local_start_states:
                #check if they share a common dummy node
                end_dummy_node = local_end_states[parent_state]
                start_dummy_node = local_start_states[y]
                if start_dummy_node == end_dummy_node:
                    common_dummy_node = start_dummy_node
                    newpath[y] = path[parent_state] + [common_dummy_node, y]
                else:
                    newpath[y] = path[parent_state] + [end_dummy_node, start_dummy_node, y]
            else:
                newpath[y] = path[parent_state] + [y]
            
        #2. prune the list
        #print "length of unpruned active here %d" %(len(unpruned_active))
            
        active_parents = []
        active_nodes_dict = {}
        
        # update parent_back_pointers with current_back_pointers
        #parent_back_pointers = current_back_pointers
        #print best_parent_cost
        count = 0
        relative_beam = 0
        #print "######################"
        #print "total active nodes seen in this timestamp %d" %(active_count)
        #print unpruned_active
        #while unpruned_active:
        for current_node in sorted(unpruned_active, key=unpruned_active.get):
                
            #node_cost, current_node = heappop(unpruned_active)
            node_cost = unpruned_active[current_node]
            if count == 0:
                relative_beam = node_cost + beam_width
                
            #print node_cost - relative_beam
            if node_cost > relative_beam:
                #print "here"
                break            

            active_parents += [current_node]
            #3. expand the current node
            cur_children = trans_p[current_node].keys()

            #check if there are any non-emitting nodes.
            
            for cur_child in cur_children:
                #heappush(active_nodes, (src_child, trans_p[cur_state][src_child]))
                active_nodes_dict[cur_child] = 1
            count+=1                

        
        path = newpath
        active_nodes = active_nodes_dict.keys()
        heapify(active_nodes)


    (prob, state) = min([(V[len(obs_k)-1][y], y) for y in V[len(obs_k)-1].keys()])    
    #print "BEST PATH"
    result = print_normal_path(path[state])
    
    return result

def calc_diag_gaussian2(o_t, mean, covar): # covar is a list
    D = len(o_t)
    
    #print o_t.shape, mean.shape, covar.shape
    denominator = D * math.log(2 * math.pi) + numpy.sum(covar)
    
    numerator = 0
    for i in range(D):
        numerator += math.pow((o_t[i] - mean[i]), 2)/covar[i]
    
    return 0.5 * (numerator + denominator)

def continuous_viterbi_bp(obs_k, source_state, trans_p, means, covars, beam_width):
    V = {}
    #path = {}
    back_pointers_table = []
    active_nodes = [] # there is no dummy nodes in active_nodes
    active_parents = [] # there is dummy nodes in active_parents
    
    parent_back_pointers = {} # key: state name (str), value: index in back_pointers_table
    tmp_emit = 1
    heappush(active_nodes, source_state)    
    newBPNodeObj = BPNode(None, '*', 1, -1) #previous,word,cost,time
    back_pointers_table.append(newBPNodeObj)

    V[-1] = {}

    y = heappop(active_nodes)
    V[-1][y] = tmp_emit
    #path[y] = [y]
    src_children = trans_p[y].keys()
    active_parents_dict = {}
    active_parents_dict[y] = 1
    for src_child in src_children:
        if src_child in dummy_transitions:
            emit_children = get_all_children(src_child, (y,), [])
            for c in emit_children:
                c_node, c_ancestors = c
                heappush(active_nodes, c_node)
                last_dummy_node = c_ancestors[0]
               	#heappush(active_parents, last_dummy_node) 
                active_parents_dict[last_dummy_node] = 1
                V[-1][last_dummy_node] = tmp_emit
                #path[last_dummy_node] = path[y] + [last_dummy_node]
                #print last_dummy_node, path[last_dummy_node]
                parent_back_pointers[last_dummy_node] = 0
        else:
            heappush(active_nodes, src_child)

    active_parents = active_parents_dict.keys()
    heapify(active_parents)
    parent_back_pointers[y] = 0

    for t in range(0,len(obs_k)):
        #print "t: ",t
        #print V
        V[t] = {}
        #newpath = {}    
        unpruned_active = []
        current_back_pointers = {}
        relative_beam = 0
        active_nodes_dict = {} # used to maintain the active nodes for next iteration
        pruned_nodes = []
        # step 1. for every active node, find out the best parent, calculate the prob cost, update the current_back_pointers with parent_back_pointers
        #print len(active_nodes)
        active_count = 0
        while (active_nodes):
            y = heappop(active_nodes)
            active_count += 1
            #tmp_emit = calc_diag_gaussian(obs_k[t], means[y], covars[y])
            # use diag of covars only
            tmp_emit = calc_diag_gaussian2(obs_k[t], means[y], covars[y])
            (prob, best_parent_state) = min([(V[t-1][y0] + dec2log(trans_p[y0][y]) + tmp_emit, y0) for y0 in active_parents if y in trans_p[y0]])
            '''
            best_parent_state = None
            for y0 in active_parents:
                if y in trans_p[y0]:
                    #print y0,y
                    #print V[t-1][y0]
                    #print trans_p[y0]
                    p = V[t-1][y0] + dec2log(trans_p[y0][y]) + tmp_emit
                    if p < prob:
                        prob = p
                        best_parent_state = y0    
            '''
            V[t][y] = prob
            #print path
            #print best_parent_state, y
            #newpath[y] = path[best_parent_state] + [y]
            current_back_pointers[y] = parent_back_pointers[best_parent_state]
            
            heappush(unpruned_active,(int(prob), y))
        #path = newpath
        # step 2. prune the active nodes with relative beam 
        #print len(unpruned_active)
        best_cost, best_node = heappop(unpruned_active)
        pruned_nodes.append(best_node)
        #relative_beam = best_cost + beam_width * best_cost
        relative_beam = best_cost + beam_width
        
        while unpruned_active:
            current_cost, current_node = heappop(unpruned_active)
            if current_cost > relative_beam:
                break
            pruned_nodes.append(current_node)

        
        # step 3. expand the pruned nodes to their children        
        # update active_parents, active_nodes
        dummy_nodes = []
        active_parents = []    
        for node in pruned_nodes:
            heappush(active_parents,node)
            children = trans_p[node].keys()
            for child in children:
                #if child == '2':
                #    print "find child 2"
                if child not in dummy_transitions:
                    active_nodes_dict[child] = 1
                else:
                    dummy_nodes.append(child)

        

        # create a new entry in back_pointers_table
        for dummy_node in dummy_nodes:
            # find out the best parent entering this dummy node
            # create a new entry in back_pointers_table
            # point the current_back_pointers to itself
            (prob, best_parent_state) = min([(V[t][y0] + dec2log(trans_p[y0][dummy_node]), y0) for y0 in pruned_nodes if dummy_node in trans_p[y0]])
            previousBPNodeIndex = current_back_pointers[best_parent_state]
            previousBPNode = back_pointers_table[previousBPNodeIndex]
            word = dummy_transitions[dummy_node][best_parent_state]
            newBPNodeObj = BPNode(previousBPNode, word, prob, t)
            back_pointers_table.append(newBPNodeObj)
            #print "created a new BPNode", previousBPNode.word, "->", word

            current_back_pointers[dummy_node] = len(back_pointers_table) - 1
            #if dummy_node == '1':
            #    print len(back_pointers_table)
            
            # find out all paths towards normal nodes
            children = []
            dummy_node_children = get_all_children(dummy_node, (best_parent_state,), children)

      
    

            #print len(dummy_node_children)
            last_dummy_node_dict = {}
            # add that normal node to active_nodes for next iteration/observation
            # update the backpointers for nodes in that path
            for child in dummy_node_children:
                child_node, ancestors = child
                active_nodes_dict[child_node] = 1    
                for i, e in reversed(list(enumerate(ancestors[:-1]))):
                    if i > 0:
                        prob += trans_p[ancestors[i]][ancestors[i-1]]
                        current_back_pointers[ancestors[i-1]] = current_back_pointers[ancestors[i]]
                        
                # add the last dummy node to V[t][]
                last_dummy_node = ancestors[0]
                #print best_parent_state, last_dummy_node
                #newpath[last_dummy_node] = newpath[best_parent_state] + [last_dummy_node]
                
                #if (last_dummy_node == '2'):
                #    print newpath[last_dummy_node]
                V[t][last_dummy_node] = prob
                # add the last dummy node to the parent_nodes for next iteration
                last_dummy_node_dict[last_dummy_node] = 1
            # add last_dummy_node_dict to active_parents
            for ldn in last_dummy_node_dict.keys():
                heappush(active_parents, ldn)
             


        # prepare for the next iteration
        # heapify the active_nodes_dict to active_nodes
        # active_parents is updated already
        # current_back_pointers become parent_back_pointers
        active_nodes = active_nodes_dict.keys()
        #print "total active nodes seen in this timestamp %d" %(active_count)
        #print len(active_nodes)
        heapify(active_nodes)
        parent_back_pointers = current_back_pointers
        #path = newpath        

    # process the final result

    (prob, state) = min([(V[len(obs_k)-1][y], y) for y in V[len(obs_k)-1].keys()])
    #print "BEST_PATH", prob, state, path[state]
    result = backtrace(back_pointers_table, len(obs_k))    
    result = ' '.join(result)
    return result


def backtrace(back_pointers_table, len_obs):
    last_node = back_pointers_table[-1]

    last_node_id = len(back_pointers_table) - 1
    min_cost = 9999999
    while (back_pointers_table[last_node_id].time == len_obs-1):       
        if back_pointers_table[last_node_id].cost < min_cost:
            last_node = back_pointers_table[last_node_id]
            min_cost = last_node.cost
        last_node_id -= 1

    previous = last_node.previous
    result = []

    while previous:
        result.append(last_node.word)
        last_node = previous
        previous = last_node.previous

    # reverse the result list
    result.reverse()
    return result

def print_normal_path(path):
    result = []
    for i in range(len(path)-1):
        #print i, i+1
        #print path[i], path[i+1]
        if path[i+1] in dummy_transitions:
            if path[i] in dummy_transitions[path[i+1]]:
                word = dummy_transitions[path[i+1]][path[i]]
                if word != "lo":
                    result.append(word)
    return ' '.join(result)

# def viterbi(obs_k, states, start_p, trans_p, means, covars):
#     V = [{}]
#     path = {}
 
#     # Initialize base cases (t == 0)
#     for y in states:
#         #print y, start_p[y], emit_p[y]
        
#         tmp_emit = calc_diag_gaussian(obs_k[0], means[y], covars[y])
               
#         V[0][y] = dec2log(start_p[y]) + tmp_emit
#         #V[0][y] = start_p[y] * tmp_emit

#         path[y] = [y]
 

#     # Run Viterbi for t > 0
#     for t in range(1,len(obs_k)):
#         V.append({})
#         newpath = {}
 
#         for y in states:
#             tmp_emit = calc_diag_gaussian(obs_k[t], means[y], covars[y])
            
#             if t == 1:
#                 print 1, y, dec2log(trans_p[1][y])

#             (prob, state) = min([(V[t-1][y0] + dec2log(trans_p[y0][y])  + tmp_emit, y0) for y0 in states])
#             #(prob, state) = max([(V[t-1][y0] * trans_p[y0][y]  * tmp_emit, y0) for y0 in states])
#             V[t][y] = prob
#             newpath[y] = path[state] + [y]

#         path = newpath
 

#     (prob, state) = min([(V[len(obs_k)-1][y], y) for y in states])    
#     #(prob, state) = max([(V[1][y], y) for y in states])

#     #PrintTrellisCost(path[state], V, states)
#     return (prob, path[state])

def init_models(fsg_file, model_dir, insert_penalty):
    num_states, start_state, end_state, edge_tuples = load_fsg(fsg_file)
    num_states = int(num_states)
    
    trans_probs, means, covars = process_fsg(num_states, 
                                             start_state, 
                                             end_state, 
                                             edge_tuples,
                                             model_dir,
                                             insert_penalty)

    big_hmm_model = HMM(start_state, trans_probs, means, covars)
    #print trans_probs    
    return big_hmm_model


def batch_decode(ctl_file, back_pointer_flag, big_hmm_model, beam_width):
    test_files = open(ctl_file).readlines()
    test_files = map(strip, test_files)
    obs = fill_obs_from_file(test_files)
    
    start_state = big_hmm_model.start_state
    trans_probs = big_hmm_model.trans_probs    
    means = big_hmm_model.means
    covars = big_hmm_model.covars


    global transition_probability
    transition_probability = trans_probs

    for i, o in enumerate(obs):
        if back_pointer_flag:
            result = continuous_viterbi_bp(o, 
                                           start_state, 
                                           trans_probs, 
                                           means, 
                                           covars, 
                                           beam_width)
            print "%s\t(test-%d)" %(result, i+1)
        
        else:
            result = continuous_viterbi(o, 
                                        start_state, 
                                        trans_probs, 
                                        means, 
                                        covars, 
                                        beam_width)
        #yield result


def live_decode(test_file, back_pointer_flag, big_hmm_model, beam_width):

    obs = fill_obs_from_file([test_file])

    result = ""

    start_state = big_hmm_model.start_state
    trans_probs = big_hmm_model.trans_probs    
    means = big_hmm_model.means
    covars = big_hmm_model.covars

    # test to pass only diag covars

    global transition_probability
    transition_probability = trans_probs
        
    if back_pointer_flag:
        result = continuous_viterbi_bp(obs[0], 
                                       start_state, 
                                       trans_probs, 
                                       means, 
                                       covars, 
                                       beam_width)
    else:
        result = continuous_viterbi(obs[0], 
                                    start_state, 
                                    trans_probs, 
                                    means, 
                                    covars, 
                                    beam_width)

    
    return result
    

def main():
    
    parse = argparse.ArgumentParser()
    #parse.add_argument('-l',  '--learn', action="store_true", dest="learn", default=False, help='training mode=True')
    parse.add_argument('-m','--model', action="store", dest="model", default=2, help='some model name')
    parse.add_argument('-k','--modeldir', action="store", dest="modeldir", default=2, help='set modeldir to some path')
    parse.add_argument('-g', '--fsgfile', action="store", dest="fsgfile", help='set fsg file')
    parse.add_argument('-d', '--dir', action="store", dest="train_dir", help="train_directory")
    parse.add_argument('-t', '--test_file', action="store", dest="test_file", default='', help="test file")
    parse.add_argument('-s', '--num-states', action="store", dest="num_states", default=3, help="num states")
    parse.add_argument('-b', '--beam-width', action="store", dest="beam_width", default=2, help="beam width")
    parse.add_argument('-i', '--insert-penalty', action="store", dest="insert_penalty", default=20, help="insert penalty")
    parse.add_argument('-p', '--back-pointer', action="store_true", dest="back_pointer", default=False, help="enable back pointers")

    parse.add_argument('-c', '--ctl-file', action='store', dest='ctl_file')

    res = parse.parse_args()
    #train = res.learn
    num_states = int(res.num_states)
    

    model_file = res.model
    test_file = res.test_file
    beam_width = float(res.beam_width)
    insert_penalty = float(res.insert_penalty)
    back_pointer_flag = res.back_pointer   
 
    model_dir = res.modeldir
    fsg_file = res.fsgfile
    
    #hmm_obj = load_model(model_file)
    
    
    #num_states, start_state, end_state, edge_tuples = load_fsg(fsg_file)
    #num_states = int(num_states)
    
    
    #print "load_fsg here %d, %s, %s, %s\n" %(num_states, start_state, end_state, edge_tuples)
    #trans_probs, means, covars = process_fsg(num_states, start_state, end_state, edge_tuples, model_dir)
    #print transition_prob
    #global transition_probability
    #transition_probability = trans_probs
    big_hmm_model = init_models(fsg_file, model_dir, insert_penalty)
    #the following function returns a dictionary of files
    if res.ctl_file:
        batch_decode(res.ctl_file, back_pointer_flag, big_hmm_model, beam_width)
        
    else:
    #the following function returns a dictionary of files
        result = live_decode(test_file, back_pointer_flag, big_hmm_model, beam_width)
        print result


    #obs = fill_obs_from_file([test_file])
    
    #print obs.shape
    #print obs[0].shape
    #print transition_probability
    #if back_pointer_flag:
    #    continuous_viterbi_bp(obs[0], start_state, trans_probs, means, covars, beam_width)
    #else:
    #    continuous_viterbi(obs[0], start_state, trans_probs, means, covars, beam_width)

    #print transition_probability.keys()
    
    # children = []
    # children = get_all_children('1', ('10', ), children)
    # ancestors =  children[0][1]
    # print ancestors
    # for i, e in reversed(list(enumerate(ancestors))):
    #     if i > 0:
    #         print "%s, %s" %(ancestors[i], ancestors[i-1])

    
    
    #print hmm_obj.start_probs
    #obs = fill_obs_from_file([test_file])
    #print obs[0].shape
    #prob, path = viterbi(obs[0], hmm_obj.states, hmm_obj.start_probs, hmm_obj.trans_probs, hmm_obj.means, hmm_obj.covars)
    #print "chance of %s file being %s is %f %s" %(test_file, model_file, prob, path)
        #def viterbi(obs_k, states, start_p, trans_p, means, covars)


if __name__ == "__main__":
    main()
