#!/usr/bin/env python
import math
import numpy
from random import randint, shuffle
try:
	import cPickle as pickle
except:
	import pickle
import pprint

import argparse
import os
from clint.textui import colored
from multiprocessing import Pool
from time import time
from tinyasr.continuoushmm.opthmm import init_models as load_big_hmm #init_models(fsg_file, model_dir, insert_penalty)
from tinyasr.continuoushmm.opthmm import continuous_viterbi
from tinyasr.hmm.hmm import init_models as load_digit_models #init_models(model_dir)

#states = ('1', '2', '3', '4', '5')
 
observations = ('o', 'n', 'e') #k, m
 
start_probability = {}
 
transition_probability = { }
 
emission_probability = {
   '1' : {'o': 0.1, 'n': 0.4, 'e': 0.5},
   '2' : {'o': 0.6, 'n': 0.3, 'e': 0.1},
   '3' : {'o': 0.6, 'n': 0.3, 'e': 0.1},
   '4' : {'o': 0.6, 'n': 0.3, 'e': 0.1},
   '5' : {'o': 0.6, 'n': 0.3, 'e': 0.1}
   }

CONST = 0.000001

digits = {
'0' : 'zero',
'1' : 'one',
'2' : 'two',
'3' : 'three',
'4' : 'four',
'5' : 'five',
'6' : 'six',
'7' : 'seven',
'8' : 'eight',
'9' : 'nine',
'sil' : 'sil'
}


class HMM:
	def __init__(self, start_probs, trans_probs, means, covars, states):
		self.start_probs = start_probs
		self.trans_probs = trans_probs
		self.means = means
		self.covars = covars
		self.states = states
	def __init__(self, start_state, trans_probs, means, covars, partdenoms):
		self.start_state = start_state
		self.trans_probs = trans_probs
		self.means = means
		self.covars = covars
		self.partdenoms = partdenoms

		

def PrintTrellisCost(path, V, states):
	#print path 
	for i in range(len(path)):
		tmp_str = ""
		for j in states:
			if path[i] == j:
			   tmp_str += " " + colored.red("\033[1m%s,%s\033[0m:%f" %(i,j,V[i][j]))
			else:
			   tmp_str+= " " + colored.blue("X,X:%f" %(V[i][j])) 

		print tmp_str

def dec2log(prob):
	if prob == 0:
		return float('inf')
	return -math.log(prob)

def log2dec(logprob):
	return math.exp(logprob)

def save_model(model_file, start_probs, trans_probs, means, covars, states):
	hmm_obj = HMM(start_probs, trans_probs, means, covars, states)
	pickle.dump(hmm_obj, open(model_file, 'wb'))
	

def load_model(model_file):
	hmm_obj = pickle.load(open(model_file, 'rb'))
	return hmm_obj


def compute_emit_variables(current_obs, dynamic_states, means, covars, partdenoms):
    num_states = len(dynamic_states)

    #construct numpy arrays from dynamic means, covars, partdenoms
    
    dynamic_means = numpy.empty((num_states, DIM))
    dynamic_covars = numpy.empty((num_states, DIM))
    dynamic_partdenoms = numpy.empty((num_states))


    for i, state in enumerate(dynamic_states):
        dynamic_means[i:,] = means[state]
        dynamic_covars[i:,] = covars[state]
        dynamic_partdenoms[i] = partdenoms[state]
    
    emit_probs = calc_diag_gaussian_comb(current_obs, 
                                         dynamic_means, 
                                         dynamic_covars, 
                                         dynamic_partdenoms)
    
    #print emit_probs.shape
    return emit_probs


def calc_partial_denominator(covar):
    return numpy.sum(covar)

def calc_diag_gaussian_comb(o_t, means, covars, partdenoms):
    D = len(o_t)
    #print o_t.shape, mean.shape, covar.shape
    denominator = D * logpi + partdenoms
    
    numerator = 0
        
    #numerator = numpy.sum((o_t - mean)**2/covar)
    tmp1 = numpy.square(o_t - means)
    tmp2 = numpy.divide(tmp1, covars)
    numerator = numpy.sum(tmp2, axis=1)
    #numerator = numpy.sum(numpy.divide(numpy.square(o_t - mean), covar))
    #numerator = numpy.sum(numpy.divide((o_t - mean)**2, covar))
    #numerator = sum([((o_t[i] - mean[i])**2)/covar[i] for i in range(D)])
    return_val = 0.5 * (numerator + denominator)
    return return_val

def calc_diag_gaussian(o_t, mean, covar, partdenom): # covar is a list
    D = len(o_t)
    
    #print o_t.shape, mean.shape, covar.shape
    denominator = D * logpi + partdenom
    
    numerator = 0
        
    #numerator = numpy.sum((o_t - mean)**2/covar)
    tmp1 = numpy.square(o_t - mean)
    tmp2 = numpy.divide(tmp1, covar)
    numerator = numpy.sum(tmp2)
    #numerator = numpy.sum(numpy.divide(numpy.square(o_t - mean), covar))
    #numerator = numpy.sum(numpy.divide((o_t - mean)**2, covar))
    #numerator = sum([((o_t[i] - mean[i])**2)/covar[i] for i in range(D)])
    return_val = 0.5 * (numerator + denominator)
    return return_val
	


def calc_mean_diag_covar(states, obs_in_state, obs):
	means = {}
	covars = {}
	for state in states:
		x = get_obs_from_state(state, obs_in_state, obs)
		shape_of_x = x.shape
		D = shape_of_x[1]
		#print "printing covars and means"
		means[state] = numpy.mean(x, axis=0)
		covars[state] = numpy.zeros((D, D))

		for i in range(D):
			covars[state][i][i] = numpy.var(x.T[i], axis=0) + CONST
	
	return means, covars

"""
def calc_mean_covar(states, obs_in_state, obs):
	means = {}
	covars = {}
	for state in states:
		x = get_obs_from_state(state, obs_in_state, obs)
		shape_of_x = x.shape
		
		#print "printing covars and means"
		means[state] = numpy.mean(x, axis=0)
		try:
			covars[state] = numpy.cov(x.T)
			
		except:
			print "covars of %s is NaN -- setting it to zeros" %(state)
			#print shape_of_x, x
			covars[state] = numpy.zeros((shape_of_x[1], shape_of_x[1]))
		#print state, covars[state]	
		#print x.T
			#print means[state]
			
			#print state, x

	#for v in covars:
	#	print means[v].shape, covars[v].shape
	return means, covars
"""	
def get_obs_from_state(state, obs_in_state, obs):
	#x = [obs[k][n] for k,n in obs_in_state if state==obs_in_state[(k,n)]]
	x = []
	for k, n in obs_in_state:
		if state == obs_in_state[(k, n)]:
			x += [obs[k][n]]
	#print x
	return numpy.array(x)


def calc_start(states, obs_in_state, obs):
	start = {}
	
	for i in obs:
		state = obs_in_state[(i, 0)]
		if state not in start:
			start[state] = 0.0
		start[state]+=1.0

	for state in states:
		if state not in start:
			start[state] = 0.0
		start[state]/=len(obs)
		
	return start

def calc_transition(states, obs_in_state, obs):
	trans = {}
	counts= {}
	
	for y0 in states:
		trans[y0] = {}
		for y in states:
			if y not in trans[y0]:
				trans[y0][y] = 0.0


	for i in range(len(obs)):
		for t in range(1, len(obs[i])):
			y0 = obs_in_state[(i, t-1)]
			y = obs_in_state[(i, t)]

			if y0 not in counts:
				counts[y0] = 0

			counts[y0] += 1.0
			trans[y0][y] += 1.0

	
	for y0 in states:
		if y0 in counts:
			count = counts[y0]
			for y in states:
				trans[y0][y]/=count
	
			#print trans

	return trans
			
def random_assign(obs, states):
	start_vecs = []
	obs_in_state = {}
	rand_inds = numpy.arange(len(obs))
	shuffle(rand_inds)

	for i in range(len(states)):
		ind = rand_inds[i]
		rand_obs_ind = randint(0,len(obs[ind])-1)		
		start_vecs.append(obs[i][rand_obs_ind])
		obs_in_state[(ind, rand_obs_ind)] = i

	for i in obs:
		for j in range(len(obs[i])):
			(distance, ind) = min([(euclidean_distance(obs[i][j], start_vecs[i_vec]), i_vec) for i_vec in range(len(start_vecs))])
			obs_in_state[(i, j)] = ind
			
   # for y in obs_in_state:
   #	 print y, obs_in_state[y]
			
	return obs_in_state


def sequential_assign(obs, states):
	# initial segment the examples into states
	# construct the obs_in_state dictionary
	obs_in_state = {}

	for k in obs:
		k_len = len(obs[k])
		k_norm = float(k_len)/len(states)
		
		for j in range(len(obs[k])):
			new_ind = (math.floor(j/k_norm)%len(states))
			obs_in_state[(k, j)] = list(states)[int(new_ind)]
			
#	for y in obs_in_state:
#		if y[0] == 0:
#			print y[0],y[1], obs_in_state[y]
	return obs_in_state
		


def euclidean_distance(vec1, vec2):
	dist = numpy.linalg.norm(vec1-vec2)
	return dist

def fill_obs_vectors(k, m, n):
	x = numpy.arange(k*m*n)
	obs = {}
	numpy.random.shuffle(x)
	for i in range(0,k):
		 obs[i] = x[m*n*i:m*n*(i+1)].reshape(m,n)	
	return obs

def fill_obs_from_file(files):
	obs = {}
	for i in range(len(files)):
		obs[i] = numpy.loadtxt(files[i])#, usecols=(1,2,3,4,5,6,7,8,9,10,11,12))
		#print obs[i].shape
	return obs

def change_assignment(k, obs_in_state, new_path):
	#print new_path
	for i in range(len(new_path)):
		obs_in_state[(k, i)] = new_path[i]

	return obs_in_state

def assignment_is_changed(old_path, new_path):
	# return true is any state assignment is changed
	print "old path is ", old_path
	print "new path is ", new_path
	for i in range(len(new_path)):
		if old_path[i] != new_path[i]:
			return True
	return False



def iterative_training(states, obs_in_state, obs):
	change = True

	iteration = 0
	while change:
		iteration += 1
		print "XXXXX doing iteration no. %d  XXXXX" %(iteration)
		#step 2 calculate start and transition
		start_probs = calc_start(states, obs_in_state, obs)

		trans_probs = calc_transition(states, obs_in_state, obs)

		#step 3 calculate means and covar matrix

		means, covars = calc_mean_diag_covar(states, obs_in_state, obs)

		#step 4 calculate emission probs

		#emission_probs = calc_emission(states, obs_in_state, obs, means, covars) 

		#step 5  viterbi for each training example

		#print "start probs", start_probs
		#print "trans probs", trans_probs
		#print "emission_probs", emission_probs

		obs_in_state, change = viterbi_training(states, 
												obs_in_state, 
												obs, 
												start_probs, 
												trans_probs, 
												means, 
												covars)
		
		#print change
		#print obs_in_state
	print "CONVERGED AFTER %d ITERATIONS"  %(iteration)
	return start_probs, trans_probs, means, covars
	
def viterbi_training(states, obs_in_state, obs, start_probs, trans_probs, means, covars):
	#new_obs_in_state = obs_in_state
	change = False
	for k in range(len(obs)):
		print "##### using example number %d ######" %(k + 1)
		obs_k = obs[k]
		cur_path = []
		for j in range(len(obs[k])):
			cur_path.append(obs_in_state[(k, j)])
		#print emission_probs, trans_probs

		prob, new_path = viterbi(obs_k, states, start_probs, trans_probs, means, covars)
		
		if assignment_is_changed(cur_path, new_path):
			obs_in_state = change_assignment(k, obs_in_state, new_path)
			change = True

	return obs_in_state, change

	
def viterbi(obs_k, states, start_p, trans_p, means, covars):
	V = [{}]
	path = {}
 
	# Initialize base cases (t == 0)
	for y in states:
		#print y, start_p[y], emit_p[y]
		
		tmp_emit = calc_diag_gaussian(obs_k[0], means[y], covars[y])
			   
		V[0][y] = dec2log(start_p[y]) + tmp_emit
		#V[0][y] = start_p[y] * tmp_emit

		path[y] = [y]
 

	# Run Viterbi for t > 0
	for t in range(1,len(obs_k)):
		V.append({})
		newpath = {}
 
		for y in states:
			tmp_emit = calc_diag_gaussian(obs_k[t], means[y], covars[y])
			
		

			(prob, state) = min([(V[t-1][y0] + dec2log(trans_p[y0][y])  + tmp_emit, y0) for y0 in states])
			#(prob, state) = max([(V[t-1][y0] * trans_p[y0][y]  * tmp_emit, y0) for y0 in states])
			V[t][y] = prob
			newpath[y] = path[state] + [y]

		path = newpath
 

	(prob, state) = min([(V[len(obs_k)-1][y], y) for y in states])	
	#(prob, state) = max([(V[1][y], y) for y in states])

	#PrintTrellisCost(path[state], V, states)
	return (prob, path[state])

def print_normal_path(path):
    result = []
    for i in range(len(path)-1):
        #print i, i+1
        #print path[i], path[i+1]
        if path[i+1] in dummy_transitions:
            if path[i] in dummy_transitions[path[i+1]]:
                word = dummy_transitions[path[i+1]][path[i]]
                if word != "lo":
                    result.append(word)
    return ' '.join(result)


def run_parallel_viterbi((model_name, hmm_obj, obs)):
	
	#model_name = model_file
	#model_file = os.path.join(model_dir, model_name)		
	#hmm_obj = load_model(model_file)
	prob, path = viterbi(obs, hmm_obj.states, hmm_obj.start_probs, hmm_obj.trans_probs, hmm_obj.means, hmm_obj.covars)
	
	return (model_name, prob)

def live_decode(test_file, hmm_models):

	
	model_probs = {}
	po = Pool()
	obs = fill_obs_from_file([test_file])
	
	res = po.map_async(run_parallel_viterbi, ((model_name, hmm_model, obs[0]) for model_name, hmm_model in hmm_models.items()))
	
	signal.signal(signal.SIGINT, signal_handler)
	
	best_models = sorted(res.get(), key=lambda tup: tup[1])
	return best_models[0][0].strip('.m')

	#best_models = sorted(model_probs, key=model_probs.get)
	#print best_models
	#print "chance of %s file being %s is %f %s" %(test_file, model_file, prob, path)
	#return res.get()
		#def viterbi(obs_k, states, start_p, trans_p, means, covars)

def signal_handler(signal, frame):
		print 'You pressed Ctrl+C!'
		sys.exit(0)

def init_models(model_dir):
	hmm_models = {}
	model_files = os.listdir(model_dir)
	
	for model_file in model_files:
		model_name = model_file
		model_file = os.path.join(model_dir, model_name)		
		hmm_obj = load_model(model_file)
		hmm_models[model_name] = hmm_obj
	return hmm_models

def continuous_training():
	pass

def main():
	
	parse = argparse.ArgumentParser()
	parse.add_argument('-d', '--train_dir', action="store", dest="train_dir", help="train_directory")
	parse.add_argument('-k', '--model_dir', action="store", dest="model_dir", help="model_directory")
	parse.add_argument('-f', '--fsg_dir', action="store", dest="fsg_dir", help="fsg_directory")
	parse.add_argument('-c', '--ctl_file', action="store", dest="ctl_file", default='', help="control file")
	parse.add_argument('-s', '--num_states', action="store", dest="num_states", default=3, help="num states")

	res = parse.parse_args()
	num_states = int(res.num_states)
	train_dir = res.train_dir
	model_dir = res.model_dir
	fsg_dir = res.fsg_dir
	states = set(numpy.arange(num_states))
	ctl_file = res.ctl_file
	ctl_file = open(ctl_file)
	lines = ctl_file.readlines()
	feat_files = [os.path.join(train_dir, x.rstrip()) for x in lines]
	fsg_files = [os.path.join(fsg_dir, x) for x in os.listdir(fsg_dir) if x.endswith('.fsg')]
	# step 0: load all hmms for digits, load obs from each training example, construct big HMM for each fsg file
	models = load_digit_models(model_dir) # a dict of hmm models
	obs = fill_obs_from_file(feat_files)
	BIG_HMM = []
	for f in fsg_files:
		BIG_HMM.append(load_big_hmm(f, model_dir, 45))
	# step 1: segment each files into states (e.g., frame 30-40 in 1st training example of utterance 2 are corresponding to the first state of model 'zero'), an alignment score is also recorded.
	for i, obs_k in enumerate(obs):

		utt_fn = files[i]
		
		#continuous_viterbi(obs_k, source_state, trans_p, means, covars, beam_width):

	# step 2: aggregate the segments to update the word model

	# step 3: if the alignment score does not improve much, stop. Otherwise, go back to step 1

 


	'''
	obs = fill_obs_from_file(files)

	random assignment or sequential assignment

	state = random_assign(obs, states)
	obs_in_state = sequential_assign(obs, states)
	numpy.seterr(all='raise')
	start_probs, trans_probs, means, covars = iterative_training(states, obs_in_state, obs)
	
	print "before writing to model file"
	print start_probs
	print trans_probs
	

	save_model(model_file, start_probs, trans_probs, means, covars, states)

	'''	

#	else:
#		model_file = res.model
#		test_file = res.test_file
#		hmm_obj = load_model(model_file)
#		print "reading from model file"
#		#print hmm_obj.start_probs
#		obs = fill_obs_from_file([test_file])
#		#print obs[0].shape
#		prob, path = viterbi(obs[0], hmm_obj.states, hmm_obj.start_probs, hmm_obj.trans_probs, hmm_obj.means, hmm_obj.covars)
#		print "chance of %s file being %s is %f %s" %(test_file, model_file, prob, path)
#		#def viterbi(obs_k, states, start_p, trans_p, means, covars)


if __name__ == "__main__":
	main()

