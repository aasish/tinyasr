#!/usr/bin/env python
from tinyasr.hmm.hmm import init_models as load_digit_models #init_models(model_dir)
from tinyasr.continuoushmm.hmm import fill_obs_from_file
from tinyasr.continuoushmm.opthmm import load_fsg
from tinyasr.hmm.hmm import HMM
from tinyasr.continuoushmm.opthmm import logpi, dec2log, log2dec, CONST
import argparse
import numpy
import os
import time
import math
import sys
import re

try:
    import cPickle as pickle
except:
    import pprint

import cProfile
from string import strip, replace

INT_PATHS = True
STR_PATHS = False
ALL_PATHS = INT_PATHS and STR_PATHS

digits = {
'0' : 'zero',
'1' : 'one',
'2' : 'two',
'3' : 'three',
'4' : 'four',
'5' : 'five',
'6' : 'six',
'7' : 'seven',
'8' : 'eight',
'9' : 'nine',
's' : 'sil',
}

all_active_states = []
all_dynamic_means = []
all_dynamic_covars = []

int_to_state = ['zero_0', 'zero_1', 'zero_2', 'zero_3', 'zero_4', 'one_0', 'one_1', 'one_2', 'one_3', 'one_4', 'two_0', 'two_1', 'two_2', 'two_3', 'two_4', 'three_0', 'three_1', 'three_2', 'three_3', 'three_4', 'four_0', 'four_1', 'four_2', 'four_3', 'four_4', 'five_0', 'five_1', 'five_2', 'five_3', 'five_4', 'six_0', 'six_1', 'six_2', 'six_3', 'six_4', 'seven_0', 'seven_1', 'seven_2', 'seven_3', 'seven_4', 'eight_0', 'eight_1', 'eight_2', 'eight_3', 'eight_4', 'nine_0', 'nine_1', 'nine_2', 'nine_3', 'nine_4', 'sil_0', 'sil_1', 'sil_2', 'sil_3', 'sil_4']

state_to_int = {'one_4': 9, 'three_3': 18, 'three_0': 15, 'three_1': 16, 'one_0': 5, 'one_1': 6, 'one_2': 7, 'one_3': 8, 'two_2': 12, 'two_3': 13, 'two_0': 10, 'two_1': 11, 'six_1': 31, 'six_2': 32, 'two_4': 14, 'six_3': 33, 'six_4': 34, 'three_2': 17, 'zero_1': 1, 'zero_0': 0, 'zero_3': 3, 'zero_2': 2, 'zero_4': 4, 'seven_1': 36, 'seven_0': 35, 'seven_3': 38, 'seven_2': 37, 'seven_4': 39, 'sil_4': 54, 'sil_0': 50, 'sil_1': 51, 'sil_2': 52, 'sil_3': 53, 'nine_4': 49, 'nine_0': 45, 'three_4': 19, 'nine_3': 48, 'nine_2': 47, 'nine_1': 46, 'four_4': 24, 'four_3': 23, 'four_2': 22, 'four_1': 21, 'four_0': 20, 'six_0': 30, 'eight_3': 43, 'eight_2': 42, 'eight_1': 41, 'eight_0': 40, 'eight_4': 44, 'five_3': 28, 'five_2': 27, 'five_1': 26, 'five_0': 25, 'five_4': 29}



def read_ref_file(ref_file):
    references = open(ref_file).readlines()
    references = [x.replace(' ', '').strip() for x in references]

    return references


def change_keys(d):
    if type(d) is dict:
        return dict([(k.split('_')[1], change_keys(v)) for k, v in d.items()])
    else:
        return d



def preprocess_model(all_trans_mat, all_means, all_covars, num_states):
    digit_trans_mat = {}
    digit_means = {}
    digit_covars = {}


    for digit in all_trans_mat:
        for k in all_trans_mat[digit]:
            val = all_trans_mat[digit][k]
            #use a neg-neg-log before 
            all_trans_mat[digit][k] = log2dec(-val)

    for digit in digits:
        digit_name = digits[digit]

        digit_trans_mat[digit_name] = dict((digit_name+'_'+str(i), all_trans_mat[digit_name+'_'+str(i)]) for i in range(num_states))
        digit_trans_mat[digit_name] = change_keys(digit_trans_mat[digit_name])

        digit_covars[digit_name] = dict((digit_name+'_'+str(i), all_covars[digit_name+'_'+str(i)]) for i in range(num_states))
        digit_covars[digit_name] = change_keys(digit_covars[digit_name])


        digit_means[digit_name] = dict((digit_name+'_'+str(i), all_means[digit_name+'_'+str(i)]) for i in range(num_states))
        digit_means[digit_name] = change_keys(digit_means[digit_name])
        
    return digit_trans_mat, digit_means, digit_covars

        
def save_models(model_dir, all_trans_mat, all_means, all_covars, all_start_probs, num_states):

    
    all_trans_mat, all_means, all_covars = preprocess_models(all_trans_mat, all_means, all_covars, num_states)
    for digit in digits:
        digit_name = digits[digit]
        model_file = os.path.join(model_dir, digit_name + '.m')
        start_probs = all_start_probs[digit_name]
        means = all_means[digit_name]
        covars = all_covars[digit_name]

        save_model(model_file, start_probs, trans_probs, means, covars)
        
    
def get_inter_hmm_connections(reference, start_state, end_state, trans_mat, inv_trans_mat):
#   state_connections = {}
    for i in range(len(reference) - 1):
        j = i + 1
        end_state_i = "%s_%s" %(digits[reference[i]], end_state)
        beg_state_j = "%s_%s" %(digits[reference[j]], start_state)
        #state_connections[end_state_i] = beg_state_j
        trans_mat[end_state_i][beg_state_j] = -0.0
        if beg_state_j not in inv_trans_mat:
            inv_trans_mat[beg_state_j] = {}
            
        inv_trans_mat[beg_state_j][end_state_i] = 1         
    return trans_mat, inv_trans_mat
    
    
    #add new transitions 
    #start_state_prob
    #trans_p['0_beg']
    #trans_p['0_end']
def calc_transition_matrix():
    pass

    

def calc_diag_gaussian(o_t, mean, covar):
    D = len(o_t)
    #print covar.shape
    denominator = D * logpi + numpy.sum(covar)    
    
    numerator = numpy.sum(numpy.divide(numpy.square(o_t - mean), covar), axis=1)
    
    return 0.5 * (numerator + denominator)



def calc_diag_mean_covar(state_obs):
    means = {} # means[state] = numpy.array (36, 1)
    covars = {} # covars[state] = numpy.array (36, 1)

    for state, x in state_obs.iteritems():
        x = numpy.array(x)    
        means[state] = numpy.mean(x, axis=0)
        D = x.shape[1]
        #covars[state] = numpy.diag(numpy.cov(x))
        covars[state] = numpy.zeros((D))
        covars[state] = [numpy.var(x.T[i])+CONST for i in range(D)]


    return means, covars


def get_obs_for_state(count, int_paths, observations):
    obs_for_state = {}
    #print type(observations), len(observations)
    for st in range(count):
        inds = numpy.where(int_paths == st)
        #for i in range(len(inds[0])):
         #   row = inds[0][i]
         #   col = inds[1][i]
            #if col > observations[row].shape[0]:
            #    print row, col, int_paths[row][col], observations[row].shape[0]
        obs_for_state[st] = [observations[inds[0][i]][inds[1][i]] for i in range(len(inds[0]))]
    return obs_for_state


def get_trans_counts(paths, count, obs_for_state):
    trans_mat = {}
    big_line = '\n'.join(paths)
    num_states = 5
    for i in range(count):

        j = i+1
        total_i = len(obs_for_state[i])
        total_i = float(total_i)
        if total_i == 0:
            print "WARNING no observations for state %d" %(i)
            continue

        ith_state = int_to_state[i]
        ith_word = ith_state.split('_')[0]
        ith_substate = ith_state.split('_')[1]

        trans_mat[ith_state] = {}

        trans_i_j = 0

        if j < count:

            jth_state = int_to_state[j]
            jth_word = jth_state.split('_')[0]
            if ith_word == jth_word:
                #trans_i_j = len(re.findall('\\b' + str(i)+' ' + str(j)+'\\b', big_line))
                trans_i_j = re.subn('(?=\\b%d %d\\b)'%(i, j), '', big_line)[1]
                if trans_i_j != 0:
                    #print "%d %d %d/%d = %f" %(i, i+1, trans_i_j, total_i, float(trans_i_j)/total_i)
                    trans_mat[ith_state][jth_state] = dec2log(float(trans_i_j)/total_i)
            
        #trans_i_i = len(re.findall('\\b'+ str(i)+' ' + str(i)+'\\b', big_line))        

        if int(ith_substate) == num_states - 1:
            trans_mat[ith_state][ith_state] = -0.0

        else:            
            trans_i_i = re.subn('(?=\\b%d %d\\b)'%(i, i), '', big_line)[1]
            if trans_i_i != 0:
                trans_mat[ith_state][ith_state] = dec2log(float(trans_i_i)/total_i)
                #print "%d %d %d/%d = %f" %(i, i, trans_i_i, total_i, float(trans_i_i)/total_i)

            elif trans_i_j == 0 and trans_i_i == 0:
                trans_mat[ith_state][ith_state] = 0.0
            
    return trans_mat    

def opt_process_paths(paths, observations, int_paths):
    count = 55
    
    state_obs = get_obs_for_state(count, int_paths, observations)
    trans_mat = get_trans_counts(paths, count, state_obs)
    means, covars = calc_diag_mean_covar(state_obs)

    return trans_mat, means, covars

def process_paths(paths, observations):    
    state_obs = {} #state_obs[state] = numpy.2Darray of observations (36, N_s)
    trans_mat = {} #trans_mat[prev][cur] = counts
    counts_vec = {}#counts_vec[state] = counts

    for i, path in enumerate(paths):
        #if reading from file first split the path from str to list
        obs = observations[i]
        T = len(path)
        for t in range(1, T):
            ob = obs[t - 1]
            #step 1. calculate trans_matition
            curr = path[t]
            current_word = curr.split('_')[0]
            
            prev = path[t - 1]
            previous_word = prev.split('_')[0]

            if prev not in counts_vec:
                counts_vec[prev] = 0.

            if prev not in trans_mat:
                trans_mat[prev] = {}
        
            #between word trans_matitions don't count the trans_matition !!!
            if current_word == previous_word:
                counts_vec[prev] += 1.0
                if curr not in trans_mat[prev]:
                    trans_mat[prev][curr] = 0.
                trans_mat[prev][curr] += 1.0
            else:
                counts_vec[prev] += 1.0
                if prev not in trans_mat[prev]:
                    trans_mat[prev][prev] = 0.
                trans_mat[prev][prev] += 1.0

             
            if prev not in state_obs:
                state_obs[prev] = []

            state_obs[prev].append(obs[t-1])
        
        #end of utterance
        if path[T-1] not in state_obs:
            state_obs[path[T-1]].append(obs[T-1])
        
        
    for prev in trans_mat:
        for curr in trans_mat[prev]:
            if counts_vec[prev] != 0:
                trans_mat[prev][curr] /= counts_vec[prev]
                trans_mat[prev][curr] = dec2log(trans_mat[prev][curr])
    
    
    means, covars = calc_diag_mean_covar(state_obs)
    
    return trans_mat, means, covars
    


def init_global_matrix(models):
    trans_mat = {}
    start_mat = {}
    means = {}
    covars = {}
    inv_trans_mat = {}
    for digit_model in models:
        digit_name = digit_model.strip('.m')
        digit_trans_mat = models[digit_model].trans_probs
        digit_start_probs = models[digit_model].start_probs
        digit_means = models[digit_model].means
        digit_covars = models[digit_model].covars
        digit_states = models[digit_model].states
        start_mat[digit_name] = {}
        for i in digit_states:
            i_state = "%s_%s" %(digit_name, i)
            start_mat[digit_name][i_state] = dec2log(digit_start_probs[i])
            means[i_state] = digit_means[i]
            covars[i_state] = numpy.diag(digit_covars[i])
            trans_mat[i_state] = {}
    
            for j in digit_trans_mat[i]:
                if digit_trans_mat[i][j] != 0:
                    j_state = "%s_%s" %(digit_name, j)
                    trans_mat[i_state][j_state] = dec2log(digit_trans_mat[i][j])

                    if(j not in inv_trans_mat):
                        inv_trans_mat[j_state] = {}

                    inv_trans_mat[j_state][i_state] = 1

    
    
    return trans_mat, inv_trans_mat, start_mat, means, covars



def iterative_train(trans_mat, inv_trans_mat, start_mat, means, covars, reference_file, feat_files):
    references = read_ref_file(reference_file)
    observations = fill_obs_from_file(feat_files)
    prev_llh = 100
    current_llh = 0
    threshold = 10
    MAX_ITERATIONS = 1

    
    count = 0
    while count < MAX_ITERATIONS and math.fabs(current_llh - prev_llh) > threshold:
        
        print prev_llh, current_llh
        current_llh, paths = force_align(trans_mat, 
                                           inv_trans_mat, 
                                           start_mat, 
                                           means, 
                                           covars, 
                                           references, 
                                           observations)

        #Step 1: Change the observation assignment
        trans_mat, means, covars = process_paths(paths, observations)
        #Step 2: Compute the new means, covars, trans_mat
        print current_llh
        count += 1
        
        
        

def force_align(trans_mat, inv_trans_mat, start_mat, means, covars, references, observations):

    len_references = len(references)
    
    paths = [[]]*len_references
    total_llh = 0
    start_state = 'sil_0'
    end_state = 'sil_4'
    
    for i, ref in enumerate(references[:1]):
        start_digit = digits[ref[0]]

        obs = observations[i]
        llh, path = viterbi(obs, 
                            ref, 
                            trans_mat, 
                            inv_trans_mat, 
                            start_mat[start_digit], 
                            means, 
                            covars, 
                            start_state,
                            end_state)
        print llh, path
        total_llh += llh
        paths[i] = path
        
    return total_llh, paths
        
   
def viterbi(obs, ref, trans_mat, inv_trans_mat, start_mat, means, covars, start_state, end_state, int_path, new_int_path):

    trans_mat, inv_trans_mat = get_inter_hmm_connections(ref, '0', '4', trans_mat, inv_trans_mat)
    #run viterbi here
    #global all_active_states, all_dynamic_means, all_dynamic_covars

    all_active_states = set([item for x in trans_mat for item in trans_mat[x]])
    all_dynamic_means = numpy.vstack([means[y] for y in all_active_states])
    all_dynamic_covars = numpy.vstack([covars[y] for y in all_active_states])
    
    
    V_prev = {}
    V_curr = {}
    path = {}
    #if INT_PATHS:
    #int_path = numpy.empty((55, 800))
    #int_path.fill(-1)
    #int_path[:,0] = numpy.arange(0, 55)
    if STR_PATHS:
        str_path = {}

    
    V_curr[start_state] = start_mat[start_state]
    #path[start_state] = [start_state]
    path[state_to_int[start_state]] = str(state_to_int[start_state])
    if STR_PATHS:
        str_path[start_state] = start_state
    T = len(obs)

    dynamic_means = numpy.empty((55, 36))
    dynamic_covars = numpy.empty((55, 36))
  
    
    all_states = False
    active_states = []
    #all_active_states = set([item for x in trans_mat for item in trans_mat[x]])
    prev_len = 0
    len_active_states = 0
    for t in range(1, T):
        newpath = {}
        if INT_PATHS:
            #new_int_path = [[]]*55
            new_int_path.fill(-1)
        if STR_PATHS:
            new_str_path = {}

        prev_len = len_active_states
        emissions = []
        if all_states:
            emissions = calc_diag_gaussian(obs[t], all_dynamic_means, all_dynamic_covars)
        
        else:

            active_states = set([item for x in V_curr for item in trans_mat[x]])
            len_active_states = len(active_states)

            if prev_len == len_active_states == len(all_active_states):
                all_states = True
                active_states = all_active_states
                emissions = calc_diag_gaussian(obs[t], all_dynamic_means, all_dynamic_covars)
            else:                         
                dynamic_means = numpy.vstack([means[y] for y in active_states])
                dynamic_covars = numpy.vstack([covars[y] for y in active_states])
                emissions = calc_diag_gaussian(obs[t], dynamic_means, dynamic_covars)
   
        V_prev = V_curr
        V_curr = {}
        
        
        for i, y in enumerate(active_states):

            tmp_emit = emissions[i]

            (prob, state) = min([(V_prev[y0] + 
                                  trans_mat[y0][y] + 
                                  tmp_emit, y0) 
                                 for y0 in V_prev 
                                 if y in trans_mat[y0]])
            V_curr[y] = prob
            
            #newpath[y] = path[state] + [y] #normal path
            newpath[state_to_int[y]] = path[state_to_int[state]] + ' ' + str(state_to_int[y])
            #integer based path for numpy matrix loading
            if INT_PATHS:
                new_int_path[state_to_int[y],:t] = int_path[state_to_int[state],:t] 
                new_int_path[state_to_int[y], t] = state_to_int[y]
            #string based path for calculating bigram, unigram counts
            if STR_PATHS:
                new_str_path[y] = str_path[state] + ' ' + y
        
        
        path = newpath   
        if INT_PATHS:
            int_path[:] = new_int_path[:]
        if STR_PATHS:
            str_path = new_str_path

    
    #prob, state =  min([(V_curr[y], y) for y in V_curr])
    if ALL_PATHS:
        return V_curr[end_state], path[end_state], int_path[state_to_int[end_state]], str_path[state]
    elif INT_PATHS:
        return V_curr[end_state], path[state_to_int[end_state]], int_path[state_to_int[end_state]]
    elif STR_PATHS:
        return V_curr[end_state], path[end_state], str_path[state]


    return V_curr[end_state], path[end_state]
    
    

def main():
	
	parse = argparse.ArgumentParser()
	parse.add_argument('-d', '--train_dir', action="store", dest="train_dir", help="train_directory")
	parse.add_argument('-k', '--model_dir', action="store", dest="model_dir", help="model_directory")
	parse.add_argument('-f', '--train_ref', action="store", dest="train_ref", help="train reference file")
	parse.add_argument('-c', '--ctl_file', action="store", dest="ctl_file", default='', help="control file")
	parse.add_argument('-s', '--num_states', action="store", dest="num_states", default=5, help="num states")
        
	res = parse.parse_args()
	num_states = int(res.num_states)
	train_dir = res.train_dir
	model_dir = res.model_dir
	train_ref = res.train_ref
	states = set(numpy.arange(num_states))
	ctl_file = res.ctl_file
	ctl_file = open(ctl_file)
	lines = ctl_file.readlines()

	feat_files = [os.path.join(train_dir, x.rstrip()) for x in lines]
        

	# step 0: load all hmms for digits, load obs from each
	# training example, construct big HMM for each fsg file
	models = load_digit_models(model_dir) # a dict of hmm models

	

        trans_mat, inv_trans_mat, start_mat, means, covars = init_global_matrix(models)        
        
        
        observations = fill_obs_from_file(feat_files)
        references = read_ref_file(train_ref)
        
        #this datastructure holds all the reference paths
        int_path_matrix = numpy.empty((len(references), 800), dtype=numpy.int8)
        str_path_matrix = ['']*len(references)
        #this datastructure holds state-by-path matrix
        int_path = numpy.empty((55, 800), dtype=numpy.int8)
        new_int_path =  numpy.empty((55, 800), dtype=numpy.int8)
                #int_path_matrix.fill(-1)

        #iterative_train(trans_mat, inv_trans_mat, start_mat, means, covars, train_ref, feat_files)
        
        start_time = time.time()
        for i in range(len(references)):
            int_path.fill(-1)
            int_path[:,0] = numpy.arange(0, 55)
            new_int_path.fill(-1)
        
            ref = references[i]
            #sys.stderr.write("###REFERENCE####\n")
            #sys.stderr.write(' '.join(list(ref))+"\n")
            #start_time = time.time()
            ob = observations[i]
            len_ob  = len(ob)
            
        
            start_digit = digits[ref[0]]
            end_digit = digits[ref[-1]]
            
            start_dig_mat = start_mat[start_digit]
            
            results = viterbi(ob, 
                              ref, 
                              trans_mat, 
                              inv_trans_mat, 
                              start_dig_mat, 
                              means, 
                              covars, 
                              'sil_0', 'sil_4', 
                              int_path, new_int_path)

            if INT_PATHS:
                llh, path, int_path_ref = results
                int_path_matrix[i,:] = int_path_ref
                str_path_matrix[i] = path
                #print zip(int_path_ref, path.split(' '))
            elif STR_PATHS:
                llh, path, str_path = results
            elif ALL_PATHS:
                llh, path, int_path_ref, str_path = results
            else:
                llh, path = results
            #print ' '.join(path)
            #time_took = time.time() - start_time
            #print str_path
            #print "Done with reference %d: %s of RT:%f at %f" %(i, ref, time_took/(len_ob/100), time_took)
            #sys.stderr.write("Done with reference %d: %s of RT:%f at %f\n" %(i, ref, time_took/(len_ob/100), time_took))

        sys.stderr.write("END of force alignment %f\n" %(time.time() - start_time))  
        #Following code saves the matrices into files
        """   
        int_path_matrix_f = open('train.path.matrix', 'w')
        int_path_matrix_txt = open('train.path.matrix.txt', 'w')
        numpy.save(int_path_matrix_f, int_path_matrix)
        int_path_matrix_txt.writelines(str_path_matrix)
        int_path_matrix_f.close()
        int_path_matrix_txt.close()
        """
        
        #following code reads a paths file to test process_paths function
        '''
        int_path_matrix = numpy.load(open('train.path.matrix'))
        str_path_matrix = open('train.path.matrix.txt').readlines()
        '''
        start_time = time.time()
        #following code updates the transition, means and covars
        new_trans_mat, means, covars = opt_process_paths(str_path_matrix, observations, int_path_matrix)
        sys.stderr.write("END of Updating means and covars %f\n" %(time.time() - start_time))
        #print "OLD MATRIX"
        #print trans_mat
        #print "NEW MATRIX"
        #print new_trans_mat

        """
        digit_trans_mat, digit_means, digit_covars = preprocess_model(trans_mat, means, covars, 5)
        print digit_trans_mat
        """

if __name__ == "__main__":
    #cProfile.run('main()')
    main()
