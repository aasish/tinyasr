#!/usr/bin/env python
import sys

from tinyasr.hmm.hmm import init_models as load_digit_models #init_models(model_dir)
from tinyasr.hmm.hmm import fill_obs_from_file, save_model
#from tinyasr.continuoushmm.opthmm import load_fsg
#from tinyasr.hmm.hmm import HMM
from tinyasr.continuoushmm.opthmm_prev_viterbi import logpi, dec2log, log2dec, CONST, process_fsg2, continuous_viterbi_bp_path2, HMM, load_fsg
import argparse
import numpy
import os, signal
import time
import math
from copy import deepcopy
from multiprocessing import Pool
try:
    import cPickle as pickle
except:
    import pprint

import cProfile
import itertools

from string import strip, replace

digits = {
'0' : 'zero',
'1' : 'one',
'2' : 'two',
'3' : 'three',
'4' : 'four',
'5' : 'five',
'6' : 'six',
'7' : 'seven',
'8' : 'eight',
'9' : 'nine',
's' : 'sil',
'o' : 'oh'
}

NUM_STATES = 5
NUM_SIL_STATES = 1
NS = len(digits) * NUM_STATES
D = 36
MAX_ITERS = None
PARALLEL = False


all_active_states = []
all_dynamic_means = []
all_dynamic_covars = []



def read_ref_file(ref_file):
    references = open(ref_file).readlines()
    references = [x.replace(' ', '').strip() for x in references]

    return references


def change_keys(d):
    if type(d) is dict:
        return dict([(int(k.split('_')[1]), change_keys(v)) for k, v in d.items()])
    else:
        return d

def preprocess_model(all_trans_mat, all_means, all_covars, all_start_mat, num_states):
    digit_trans_mat = {}
    digit_means = {}
    digit_covars = {}
    digit_start = {}

    for digit in all_trans_mat:
        for k in all_trans_mat[digit]:
            val = all_trans_mat[digit][k]
            #use a neg-neg-log before 
            all_trans_mat[digit][k] = log2dec(-val)
    
    for digit in all_start_mat:
        for k in all_start_mat[digit]:
            val = all_start_mat[digit][k]
            #use a neg-neg-log before 
            all_start_mat[digit][k] = log2dec(-val)

    for digit in digits:
        digit_name = digits[digit]

        dig_states = ['_'.join([digit_name,str(i)]) for i in range(num_states)]
        digit_trans_mat[digit_name] = dict((state, all_trans_mat[state]) for state in dig_states if state in all_trans_mat)
        digit_trans_mat[digit_name] = change_keys(digit_trans_mat[digit_name])

        digit_covars[digit_name] = dict((state, all_covars[state]) for state in dig_states if state in all_covars)
        digit_covars[digit_name] = change_keys(digit_covars[digit_name])


        digit_means[digit_name] = dict((state, all_means[state]) for state in dig_states if state in all_means)
        digit_means[digit_name] = change_keys(digit_means[digit_name])
        
        digit_start[digit_name] = dict((state, all_start_mat[digit_name][state]) for state in dig_states if state in all_start_mat[digit_name])
        digit_start[digit_name] = change_keys(digit_start[digit_name])
    return digit_trans_mat, digit_means, digit_covars, digit_start

        
def save_models(model_dir, all_trans_mat, all_means, all_covars, all_start_probs, states):

    
    #all_trans_mat, all_means, all_covars = preprocess_models(all_trans_mat, all_means, all_covars, num_states)
    for digit in digits:
        digit_name = digits[digit]
        model_file = os.path.join(model_dir, digit_name + '.m')
        trans_probs = all_trans_mat[digit_name]
        start_probs = all_start_probs[digit_name]
        means = all_means[digit_name]
        covars = all_covars[digit_name]
        #print "when saving model ", covars
        save_model(model_file, start_probs, trans_probs, means, covars, states)
        
    
def get_inter_hmm_connections(reference, start_state, end_state, trans_mat, inv_trans_mat):
#   state_connections = {}

    for i in range(len(reference) - 1):
        j = i + 1
        end_state_i = "%s_%s" %(digits[reference[i]], end_state)
        beg_state_j = "%s_%s" %(digits[reference[j]], start_state)
        #state_connections[end_state_i] = beg_state_j
        trans_mat[end_state_i][beg_state_j] = -0.0
        #trans_mat[end_state_i][beg_state_j] = dec2log(0.5)
        #trans_mat[end_state_i][end_state_i] = dec2log(0.5)
        if beg_state_j not in inv_trans_mat:
            inv_trans_mat[beg_state_j] = {}
            
        inv_trans_mat[beg_state_j][end_state_i] = 1       


    # reach the end of reference
    #end_state = "%s_%s" %(digits[reference[-1]], end_state)
    #trans_mat[end_state_i][beg_state_j] = dec2log(0.5)
    #trans_mat[end_state_i][end_state_i] = dec2log(0.5)
  
    return trans_mat, inv_trans_mat
    
    
    #add new transitions 
    #start_state_prob
    #trans_p['0_beg']
    #trans_p['0_end']
def calc_transition_matrix():
    pass

    

def calc_diag_gaussian(o_t, mean, covar):
    D = len(o_t)
    #print covar.shape
    denominator = D * logpi + numpy.sum(covar)    
    #print o_t.shape, mean.shape, covar.shape 
    numerator = numpy.sum(numpy.divide(numpy.square(o_t - mean), covar), axis=1)

    return 0.5 * (numerator + denominator)



def calc_diag_mean_covar(state_obs, means, covars):
    #means = {} # means[state] = numpy.array (36, 1)
    #covars = {} # covars[state] = numpy.array (36, 1)
    #print "STATE_OBS", state_obs
    weights = (0.65, 0.35)
    for state, x in state_obs.iteritems():
        x = numpy.array(x)    
        #means[state] = weights[0]*means[state] + weights[1]*numpy.mean(x, axis=0)
        means[state] = numpy.mean(x, axis=0)
        D = x.shape[1]
        #covars[state] = numpy.diag(numpy.cov(x))
        #covars[state] = numpy.zeros((D))
        x = x.T
        #covars[state] = weights[0]*covars[state] + weights[1]*numpy.array([numpy.var(x[i]) for i in range(D)])
        covars[state] = numpy.array([numpy.var(x[i])+CONST for i in range(D)])
        #print "shape of covar for state %s" %(state), covars[state].shape 
    #print "COVARS SHAPE", len(covars)
        
        #for i in range(D):
        #    covars[state][i] = numpy.var(x.T[i], axis=0) + CONST

        
#    print "shape of one state is ", covars['three_2'].shape
    return means, covars


def sequential_assignment(observations, references, num_states):
    paths = []
    for i in range(len(observations)):
        ref = references[i]
        obs_len = len(observations[i])
        y = [ob for ob in xrange(obs_len)]
        x = [digits[ch]+'_'+str(state) for ch in ref for state in range(num_states)]

        k_norm = float(obs_len)/float(len(x))
        
        x = [i for z in x for i in itertools.repeat(z, int(k_norm))]

	path = []
        path = [e for e in itertools.izip_longest(y, x, fillvalue=x[-1])]

        inds, path = zip(*path)
        paths.append(list(path))

    return paths


def process_paths(paths, observations, curr_means, curr_covars):    
    state_obs = {} #state_obs[state] = numpy.2Darray of observations (36, N_s)
    trans_mat = {} #trans_mat[prev][cur] = counts
    counts_vec = {}#counts_vec[state] = counts
    #print 'OBS', observations
    #print paths
    for i, path in enumerate(paths):
        #if reading from file first split the path from str to list
        
        obs = observations[i]
        T = len(path)
        for t in range(1, T):
            ob = obs[t - 1]
            #print ob, path[t-1]
            #step 1. calculate trans_matition
            curr = path[t]
            current_word, current_state_id = curr.split('_')
            
            prev = path[t - 1]
            previous_word, previous_state_id = prev.split('_')

            if prev not in counts_vec:
                counts_vec[prev] = 0.

            if prev not in trans_mat:
                trans_mat[prev] = {}
        
            #between word trans_matitions don't count the trans_matition !!!
            if (current_word == previous_word) and (current_state_id >= previous_state_id):
                counts_vec[prev] += 1.0
                if curr not in trans_mat[prev]:
                    trans_mat[prev][curr] = 0.
                trans_mat[prev][curr] += 1.0
            else:
                counts_vec[prev] += 1.0
                if prev not in trans_mat[prev]:
                    trans_mat[prev][prev] = 0.
                trans_mat[prev][prev] += 1.0

             
            if prev not in state_obs:
                state_obs[prev] = []

            state_obs[prev].append(obs[t-1])
        
        #end of utterance
        
        #print obs[T-1],path[T-1]
        if path[T-1] not in state_obs:
            state_obs[path[T-1]] = []
        state_obs[path[T-1]].append(obs[T-1])
        
        
    for prev in trans_mat:
        for curr in trans_mat[prev]:
            if counts_vec[prev] != 0:
                trans_mat[prev][curr] /= counts_vec[prev]
                trans_mat[prev][curr] = dec2log(trans_mat[prev][curr])
    
    
    means, covars = calc_diag_mean_covar(state_obs, curr_means, curr_covars)
    print "END OF PROCESS PATHS"
    #print len(means), len(covars), covars['three_2'].shape 
    return trans_mat, means, covars
    


def init_global_matrix(models):
    trans_mat = {}
    start_mat = {}
    means = {}
    covars = {}
    inv_trans_mat = {}
    for digit_model in models:
        digit_name = digit_model.strip('.m')
        digit_trans_mat = models[digit_model].trans_probs
        digit_start_probs = models[digit_model].start_probs
        digit_means = models[digit_model].means
        digit_covars = models[digit_model].covars
        digit_states = models[digit_model].states
        start_mat[digit_name] = {}
        for i in digit_states:
            i_state = "%s_%s" %(digit_name, i)
            start_mat[digit_name][i_state] = dec2log(digit_start_probs[i])
            means[i_state] = digit_means[i]
            
            #if digit_name == "sil":
            #means[i_state] = numpy.zeros((36,)) + CONST

   #         if INIT_COUNT == 0:
   #             #print "HERE"
   #             covars[i_state] = numpy.diag(digit_covars[i])
   #         #print "digit_covars[%d] is " %(i), digit_covars[i].shape
   #         else:
   #             covars[i_state] = numpy.array(digit_covars[i])
            if (len(digit_covars[i].shape) == 2):
                covars[i_state] = numpy.diag(digit_covars[i])
            else:
                covars[i_state] = numpy.array(digit_covars[i])
            #print "after array opt: ", covars[i_state].shape
            trans_mat[i_state] = {}
    
            for j in digit_trans_mat[i]:
                if digit_trans_mat[i][j] != 0:
                    j_state = "%s_%s" %(digit_name, j)
                    trans_mat[i_state][j_state] = dec2log(digit_trans_mat[i][j])

                    if(j not in inv_trans_mat):
                        inv_trans_mat[j_state] = {}

                    inv_trans_mat[j_state][i_state] = 1

    
    return trans_mat, inv_trans_mat, start_mat, means, covars



def iterative_train(trans_mat, inv_trans_mat, start_mat, means, covars, reference_file, observations, model_dir,new_model_dir, states):

    '''
    # code that has been tested in main() function
    # process the paths file (which is the alignment result), together with the observations
    # to get the statistics of trans, means, and covars to update the current digit models 
    paths = open('train.paths').readlines()
    paths = map(strip, paths)
    paths = [path.split(' ') for path in paths]
    trans_mat, means, covars = process_paths(paths, observations)
    digit_trans_mat, digit_means, digit_covars = preprocess_model(trans_mat, means, covars, 5)

    '''
    print "iterative_train starts ... "
    references = read_ref_file(reference_file)
    #observations = fill_obs_from_file(feat_files)
    total_length = 0.0
    for o in observations:
        total_length += len(observations[o])
    total_length /= 100.0
    print "TOTAL LENGTH OF TRAINING DATA: %f seconds" %(total_length)
    
    prev_llh = 10e10
    current_llh = 0
    threshold = 10
    global MAX_ITERS
    MAX_ITERATIONS = MAX_ITERS 

    
    count = 0
    step_model_dir = model_dir

    digit_trans_mat, digit_means, digit_covars, digit_start = preprocess_model(trans_mat, means, covars, start_mat, 5)
    
    while count < MAX_ITERATIONS:
        
        print "ITERATION %d" %(count)
        start_t = time.time()
        '''
        current_llh, paths = force_align(trans_mat, 
                                           inv_trans_mat, 
                                           start_mat, 
                                           means, 
                                           covars, 
                                           references, 
                                           observations, model_dir, 10)
        '''
        #current_llh, paths = force_align(references, observations, step_model_dir, 35)
        
        current_llh, paths = force_align(references, observations, digit_trans_mat, digit_means, digit_covars, digit_start, 1)
        print current_llh, prev_llh - current_llh
        
        fn = 'path'+str(count)
        f = open(fn,'wb')
        f.write(current_llh)
        for p in paths:
            f.write('\n')
            f.write(' '.join(p))
        f.close()
        

        #if math.fabs(prev_llh - current_llh) < threshold:
        #if (prev_llh - current_llh) < threshold:
        #   break
        
        prev_llh = current_llh

        #Step 1: Change the observation assignment
#       print "Changing the observation assignment ... "
        trans_mat, means, covars = process_paths(paths, observations, means, covars)
        
        step_model_dir = os.path.join(new_model_dir, 'step_%d'%(count))
        if not os.path.exists(step_model_dir):
            os.makedirs(step_model_dir)

        digit_trans_mat, digit_means, digit_covars, digit_start = preprocess_model(trans_mat, means, covars, start_mat, 5)
        
        
        save_models(step_model_dir, digit_trans_mat, digit_means, digit_covars, digit_start, states)
        
        #print "COVARS: ", covars
#       print "covars['three_2'].shape = ", covars['three_2'].shape 
        #Step 2: Compute the new means, covars, trans_mat
#       print "Compute the new means, covars, trans_mat"
        
#       digit_trans_mat, digit_mean, digit_covars, digit_start = preprocess_model(trans_mat, means, covars, start_mat, 5)
#       print digit_covars['three'][2], len(digit_covars['three'][2])
        count += 1
        end_t = time.time()
        total_time = end_t - start_t
        print "TOTAL TIME ELAPSED: ", total_time
        print "RT: %f" %(total_time / total_length)
        
    #Save models

    #digit_trans_mat, digit_mean, digit_covars, digit_start = preprocess_model(trans_mat, means, covars, start_mat, 5)

    #save_models(new_model_dir, digit_trans_mat, digit_mean, digit_covars, digit_start, states)


#        models = load_digit_models(model_dir) # a dict of hmm models
        
#        trans_mat, inv_trans_mat, start_mat, means, covars = init_global_matrix(models)
#        print "length of covars is %d" %(len(covars))
        #print "COVARS: ", covars
#       	print "covars['three_2'].shape is ", covars['three_2'].shape 

def create_fsg(ref):
    #fsg = open('test.fsg','wb')
    ref = list(ref.rstrip())
    #print ref
    len_ref = len(ref)
    '''
    fsg.write("N_State: %d\n" %(len_ref + 1))
    fsg.write("Start_state: 0\n")
    fsg.write("Terminal_States: %d\n" %(len_ref))
    count = 0
    for i in ref:
        fsg.write( 'Edge %d %d "%s"\n' %(count, count+1,i))
        count += 1
#    return fsg.split('\n')[:-1]
   '''
    fsg = []    
    fsg.append("N_State: %d" %(len_ref + 1))
    fsg.append("Start_state: 0")
    fsg.append("Terminal_States: %d" %(len_ref))
    count = 0
    for i in ref:
        fsg.append( 'Edge %d %d "%s"' %(count, count+1,i))
        count += 1
    return fsg
'''
def load_fsg(fsg_file):
    lines = map(strip, fsg_file)
    #line 1 is num states
    num_states = lines[0].split(': ')[1]
    #line 2 start state
    start_state = lines[1].split(': ')[1]
    #line 3 term state
    end_state = lines[2].split(': ')[1]
    #rest are edges until penultimate line
    edge_tuples = []
    for i in range(3, len(lines)):
        print lines[i]
        line_entries = lines[i].split(' ')
        if len(line_entries) > 3:
            label, from_state, to_state, edge_id = lines[i].split(' ')
            edge_tuples += [(from_state, to_state, edge_id.strip('"'))]
        else:
            label, from_state, to_state = lines[i].split(' ')
            edge_tuples += [(from_state, to_state, "lo")]

    return num_states, start_state, end_state, edge_tuples
'''

def parallel_viterbi((ref, observation, digit_trans_mat, digit_means, digit_covars, digit_start_mat, insert_penalty)):
#def parallel_viterbi((ref, observation, model_dir, insert_penalty)):
    fsg = create_fsg(ref)
    num_states, start_state, end_state, edge_tuples = load_fsg(fsg)
    num_states = int(num_states)

    total_states, state_id_map, dummy_transition, trans_probs, m, c, partdenoms = process_fsg2(num_states,
                                                                                               start_state,
                                                                                               end_state,
                                                                                               edge_tuples,
                                                                                               digit_trans_mat,
                                                                                               digit_means,
                                                                                               digit_covars,
                                                                                               digit_start_mat,
                                                                                               insert_penalty
                                                                                               )
            
    start_digit = digits[ref[0]]
    end_state = str(total_states - 1)
    
        # step 3: run the viterbi_alignment
    prob, path = continuous_viterbi_bp_path2(state_id_map, dummy_transition,
                                             observation,
                                             start_state,
                                             end_state,
                                             trans_probs,
                                             m,
                                             c,
                                             partdenoms,
                                             500.0, 100)
    
            
            
    del state_id_map
    del dummy_transition
    del trans_probs
    del m
    del c
    del partdenoms

    """
    #   print num_states, start_state, end_state, edge_tuples
    total_states, state_id_map, dummy_transition, trans_probs, m, c, partdenoms = process_fsg2(num_states,
                                                                                               start_state,
                                                                                               end_state,
                                                                                               edge_tuples,
                                                                                               model_dir,
                                                                                               insert_penalty
                                                                                               )

    start_digit = digits[ref[0]]
    end_state = str(total_states - 1)

        # step 3: run the viterbi_alignment
    prob, path = continuous_viterbi_bp_path2(state_id_map, dummy_transition,
                                             observation,
                                             start_state,
                                             end_state,
                                             trans_probs,
                                             m,
                                             c,
                                             partdenoms,
                                             500.0, 100)
    """

    return prob, path
#def force_align(trans_mat, inv_trans_mat, start_mat, means, covars, references, observations, model_dir, insert_penalty):
def force_align(references, observations, digit_trans_mat, digit_means, digit_covars, digit_start_mat, insert_penalty):
#def force_align(references, observations, model_dir, insert_penalty):

    len_references = len(references)
    '''
    global all_active_states, all_dynamic_means, all_dynamic_covars
    all_active_states = set([x for x in trans_mat])

    all_dynamic_means = numpy.vstack([means[y] for y in all_active_states])
    all_dynamic_covars = numpy.vstack([covars[y] for y in all_active_states])
    
    total_llh = 0
    start_state = 'sil_0'
    end_state = 'sil_4'
    '''


    if PARALLEL:
    #multiprocessing 
        
        po = Pool()
        res = po.map_async(parallel_viterbi,((ref, observations[i], digit_trans_mat, digit_means, digit_covars, digit_start_mat, insert_penalty) for i, ref in enumerate(references)))
        
        signal.signal(signal.SIGINT, signal_handler)
        
        llh_list, paths = zip(*res.get())
        total_llh = sum(llh_list)
        paths = list(paths)
        po.close()
        po.join()
    
    else:
    #sequential
        total_llh = 0
        paths = [[]]*len_references
        
        for i, ref in enumerate(references):
            
            # step 1: create a fsg for the reference first, create a dict mapping from (str) states to (int) state
            fsg = create_fsg(ref)
            num_states, start_state, end_state, edge_tuples = load_fsg(fsg)
            num_states = int(num_states)

            """
            total_states, state_id_map, dummy_transition, trans_probs, m, c, partdenoms = process_fsg2(num_states,
                                                                                                       start_state,
                                                                                                       end_state,
                                                                                                       edge_tuples,
                                                                                                       model_dir,
                                                                                                       insert_penalty
                                                                                                       )
            """
            total_states, state_id_map, dummy_transition, trans_probs, m, c, partdenoms = process_fsg2(num_states,
                                                                                                       start_state,
                                                                                                       end_state,
                                                                                                       edge_tuples,
                                                                                                       digit_trans_mat,
                                                                                                       digit_means,
                                                                                                       digit_covars,
                                                                                                       digit_start_mat,
                                                                                                       insert_penalty
                                                                                                       )
            
            start_digit = digits[ref[0]]
            end_state = str(total_states - 1)

        # step 3: run the viterbi_alignment
            prob, path = continuous_viterbi_bp_path2(state_id_map, dummy_transition,
                                                     observations[i],
                                                     start_state,
                                                     end_state,
                                                     trans_probs,
                                                     m,
                                                     c,
                                                     partdenoms,
                                                     500.0, 100)

            
            
            del state_id_map
            del dummy_transition
            del trans_probs
            del m
            del c
            del partdenoms
            
            #print path
            total_llh += prob
        # step 4: change to path to strings such as 'sil_0 sil_1 sil_2 ... '
            paths[i] = path
        #print path
        
    return total_llh, paths

def signal_handler(signal, frame):
        print 'You pressed Ctrl+C!'
        sys.exit(0)


'''
def viterbi2(obs, ref, trans_mat, inv_trans_mat, start_mat, means, covars, start_state, end_state):
    trans_mat, inv_trans_mat = get_inter_hmm_connections(ref, '0', '4', trans_mat, inv_trans_mat)
    V_prev = {}
    V_curr = {}
    path = {}
    V_curr[start_state] = start_mat[start_state]
    path[start_state] = [start_state]
    T = len(obs)
    for t in range(1,T):
        newpath = {}
        for st in trans_mat:
            
    pass
'''        

#TODO: Move the numpy matrices outside viterbi

   
def viterbi(obs, ref, trans_mat, inv_trans_mat, start_mat, means, covars, start_state, end_state):

    
    #It was said that cPickle is better than deepcopy
    custom_trans_mat = pickle.loads(pickle.dumps(trans_mat, -1))
    #custom_trans_mat = deepcopy(trans_mat)
    custom_trans_mat, inv_trans_mat = get_inter_hmm_connections(ref, 
                                                                '0', 
                                                                '4', 
                                                                custom_trans_mat, 
                                                                inv_trans_mat)
    #run viterbi here
    global all_active_states, all_dynamic_means, all_dynamic_covars
    
    """
    all_active_states = set([x for x in trans_mat])
                             #for item in custom_trans_mat[x]])
    all_dynamic_means = numpy.vstack([means[y] for y in all_active_states])
    all_dynamic_covars = numpy.vstack([covars[y] for y in all_active_states])
    """
    # for y in all_active_states:
    #     print covars[y].shape
    #     print means[y].shape
        
    #print all_dynamic_means.shape
    #print all_dynamic_covars.shape
    
    V_prev = {}
    V_curr = {}
    path = {}
    
    V_curr[start_state] = start_mat[start_state]
    path[start_state] = [start_state]

    T = len(obs)
    
    
    dynamic_means = numpy.empty((NS, D))
    dynamic_covars = numpy.empty((NS, D))
  
    
    all_states = False
    active_states = []
    #all_active_states = set([item for x in trans_mat for item in trans_mat[x]])
    prev_len = 0
    len_active_states = 0
    for t in range(1, T):
        newpath = {}
        #print t 
        prev_len = len_active_states
        emissions = []
        if all_states:
            emissions = calc_diag_gaussian(obs[t], all_dynamic_means, all_dynamic_covars)
        
        else:

            active_states = set([item for x in V_curr for item in custom_trans_mat[x]])
            len_active_states = len(active_states)
            if prev_len == len_active_states == len(all_active_states):
               #print "######### %d" %(t)
               # print active_states
                all_states = True
                #print "all_dynamic_shapes:", all_dynamic_means.shape, all_dynamic_covars.shape
                active_states = all_active_states
                emissions = calc_diag_gaussian(obs[t], all_dynamic_means, all_dynamic_covars)
            else:                         
                dynamic_means = numpy.vstack([means[y] for y in active_states])
                dynamic_covars = numpy.vstack([covars[y] for y in active_states])
                emissions = calc_diag_gaussian(obs[t], dynamic_means, dynamic_covars)
   
        V_prev = V_curr
        V_curr = {}
        
        #print "active states are: ", active_states
        #print "V_prev is: ", V_prev 
        for i, y in enumerate(active_states):
            tmp_emit = emissions[i]
                
            (prob, state) = min([(V_prev[y0] + 
                                  custom_trans_mat[y0][y] + 
                                  tmp_emit, y0) 
                                 for y0 in V_prev 
                                 if y in custom_trans_mat[y0]])
            V_curr[y] = prob

            newpath[y] = path[state] + [y]
            
        path = newpath        
        

    del custom_trans_mat
    del dynamic_means
    del dynamic_covars
    #prob, state =  min([(V_curr[y], y) for y in V_curr])
    return V_curr[end_state], path[end_state]
    
    


def main():
	
	parse = argparse.ArgumentParser()
	parse.add_argument('-d', '--train_dir', action="store", dest="train_dir", help="train_directory")
	parse.add_argument('-k', '--model_dir', action="store", dest="model_dir", help="model_directory")
	parse.add_argument('-n', '--new_model_dir', action="store", dest="new_model_dir", help="new model_directory")
	parse.add_argument('-f', '--train_ref', action="store", dest="train_ref", help="train reference file")
	parse.add_argument('-c', '--ctl_file', action="store", dest="ctl_file", default='', help="control file")
	parse.add_argument('-s', '--num_states', action="store", dest="num_states", default=5, help="num states")
        parse.add_argument('-i', '--max_iterations', action="store", dest="max_iters", default=5, help="max number of iterations")
        parse.add_argument('-p', '--is_parallel', action="store_true", dest="is_parallel", default=False, help="parallel or not")
        
	res = parse.parse_args()
	num_states = int(res.num_states)
	train_dir = res.train_dir
	model_dir = res.model_dir
        new_model_dir = res.new_model_dir
	train_ref = res.train_ref
	states = set(numpy.arange(num_states))

        global MAX_ITERS
        MAX_ITERS = int(res.max_iters)
        
        global PARALLEL
        PARALLEL = res.is_parallel

	ctl_file = res.ctl_file
	ctl_file = open(ctl_file)


        
	lines = ctl_file.readlines()

	feat_files = [os.path.join(train_dir, x.rstrip()) for x in lines]


	# step 0: load all hmms for digits, load obs from each
	# training example, construct big HMM for each fsg file
	models = load_digit_models(model_dir) # a dict of hmm models

        trans_mat, inv_trans_mat, start_mat, means, covars = init_global_matrix(models)       

        #print "length of covar is %d" %(len(covars)) 
        #print "COVARS: ", covars
        #print "covars['three_2'].shape is ", covars['three_2'].shape

        observations = fill_obs_from_file(feat_files)
        references = read_ref_file(train_ref)

        '''
        curr_means = {}
        curr_covars = {}
        paths = sequential_assignment(observations, references, 5)
        trans_mat, means, covars = process_paths(paths, observations, curr_means, curr_covars)
        '''


        ''' 
        paths = open('paths.test').readline().rstrip()
        paths = list(paths.split(' '))
        paths = [paths]
        #observations = [[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]];
        trans_mat, means, covars = process_paths(paths, observations)	
        #print trans_mat
        print means

        print "\n\nCOVARS:"
        print covars
        '''

        if not os.path.exists(new_model_dir):
            os.makedirs(new_model_dir)

        iterative_train(trans_mat, inv_trans_mat, start_mat, means, covars, train_ref, observations, model_dir, new_model_dir, states)
    



        
      	''' 
        for i in range(len(references)):
            ref = references[i]
            sys.stderr.write("###REFERENCE####\n")
            sys.stderr.write(' '.join(list(ref))+"\n")
            start_time = time.time()
            ob = observations[i]
            len_ob  = len(ob)
            start_digit = digits[ref[0]]
            #print start_digit
            #print start_mat[start_digit]
            end_digit = digits[ref[-1]]
            
            llh, path = viterbi(ob, ref, trans_mat, inv_trans_mat, start_mat[start_digit], means, covars, 'sil_0', 'sil_4')
            print ' '.join(path)
            time_took = time.time() - start_time
            #print "Done with reference %d: %s of RT:%f at %f" %(i, ref, time_took/(len_ob/100), time_took)
            sys.stderr.write("Done with reference %d: %s of RT:%f at %f\n" %(i, ref, time_took/(len_ob/100), time_took))
        
	
        
        # process the paths file (which is the alignment result), together with the observations
        # to get the statistics of trans, means, and covars to update the current digit models 
        paths = open('train.paths').readlines()
        paths = map(strip, paths)
        paths = [path.split(' ') for path in paths]
        trans_mat, means, covars = process_paths(paths, observations)
        digit_trans_mat, digit_means, digit_covars = preprocess_model(trans_mat, means, covars, 5)
        '''

        
        
        

if __name__ == "__main__":
   # cProfile.run('main()')
    main()
