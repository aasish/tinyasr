#!/usr/bin/env python

import argparse

parse = argparse.ArgumentParser()
parse.add_argument('-n','--num_states', action="store", dest="num_states", default=2, help='number of states')
parse.add_argument('-l','--loopback', action="store_true", dest="loopback", default=False, help='loopback')
parse.add_argument('-s','--start-digit', action="store", dest="start_digit", default=0, help='start digit')
parse.add_argument('-e','--end-digit', action="store", dest="end_digit", default=9, help='end digit')

res = parse.parse_args()
num_states = int(res.num_states)
start_digit = int(res.start_digit)
end_digit = int(res.end_digit)
print "N_States: %s" %(res.num_states)
print "Start_State: 0"
print "Terminal_States: %d" %(num_states - 1)
for i in range(num_states - 1):
    for d in range(start_digit, end_digit + 1):
        print 'Edge %d %d "%s"' %(i, i + 1, d)


if res.loopback:
    print 'Edge %d %d' %(num_states - 1, 0)
