#!/usr/bin/perl
use File::Spec;
my $res_dir = "./Result";
my @BW = (0.001,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9); # BEAM WIDTH
open LOG, ">experiment_log" or die "cannot open log file";

foreach my $bw (@BW)
{
	my $res_fn = "prob1.res_b_$bw"."_p_sil"; #prob1.res_b_0.01_p_211s_sil
	$res_fn = File::Spec->catfile($res_dir, $res_fn);
	my $cmd = "./evaluate.pl -c prob1.ctl -r $res_fn -md ../data/ming/model/ -td ../data/ming -f prob1.fsg.sil -b $bw -p 1";
	print "$cmd\n";
	$time = `$cmd`;
	my $word_align_cmd = "./word_align.pl prob1.trans $res_fn";
	$out = `$word_align_cmd`;
	my @lines = split(/\n/,$out);
	my $acc = $lines[-2];
	chomp ($acc);
	print LOG "$res_fn\t$acc\t$time\n";		
}

close(LOG);

