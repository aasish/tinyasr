#!/usr/bin/python
import math
import time
import operator
import sys
import argparse
import networkx as nx

def init_lexical_tree(filename):
	# filename points to a file contains the template dictionary
	# in this function, three dictionaries are structed:
	# 1) parent_dict, record the parent of each node: n, p(n)
	# 2) label_dict, record the letter corresponds to the label: n, l(n)
	# 3) end_word_dict, record the label of end node and the word: n, w(n)
	parent_dict = {}
	label_dict = {}
	end_word_dict = {}
	DG = nx.DiGraph() # the lexical tree, 
	# initialize DG: add * as node 0.
	DG.add_node(0)
	label_dict['*'] = 0
	count = 1
	
	lines = open(filename).readlines()
	for line in lines:
		line = '*' + line.strip()
		print line
		letters = list(line)
		print "line is ",line
		prev_id = 0 # must be in the tree already
		# absorb line using current tree
		for i in range(len(letters):
			if letters[i] in DG.successors(prev_id):
				 

		'''
		for l in letters:
			# if the current lexical tree can absorb part of the line:
			

			# else: append the rest of the line to the lexical tree with labels
				
				prev_l = l
				print 'letter ', l, ' is already there and the prev_l is matched'
				continue 
			parent_dict[l] = prev_l
			label_dict[count] = l
			print 'adding letter ',l,' to parent_dict with prev_l = ',prev_l
			prev_l = l
			count += 1
		'''
	return parent_dict
	pass

def update_active_node(node_value_dict):
	# in node_dict, key is the node label
	# value is the edit distance so far
	pass



def beamSearch(parent_dict,label_dict,end_word_dict,input_letters, beam_width):
	# in this function, the best matched word and cost is returned
	pass

p_dict = init_lexical_tree('temp.dict')
for k in p_dict:
	print k, 'parent is', p_dict[k]
