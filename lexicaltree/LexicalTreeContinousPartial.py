#!/usr/bin/env python
from time import time
import argparse
from string import strip
from clint.textui import indent, puts
from operator import itemgetter
#NodeCount = 0
#WordCount = 0

# path_row: dictionary. Key: a tuple of path so far. Value: a list: currentRow
path_row = {}
class MyQUEUE:
	def __init__(self):
		self.holder = []
	def enqueue(self,val):
		self.holder.append(val)
	def dequeue(self):
		val = None
		try:
			val = self.holder[0]
			if len(self.holder) == 1:
				self.holder = []
			else:
				self.holder = self.holder[1:]
		except:
			pass
		return val
	def IsEmpty(self):
		result = False
		if len(self.holder) == 0:
			result = True
		return result

class LexicalTree:
#	def __init__(self):
#		self.word = None
#		self.children = {}
#		self.path = []
	def __init__(self,l,p):
		self.letter = l
		self.word = None
		self.children = {}
		self.path = p
	def insert (self, word):
		# insert a word to the tree
		node = self
		# record the full word
		actual_word = word
		# split the word into letters, the last letter combined with a $
		word = list(word)
		word_len = len(word)
		word[word_len - 1] += '$'
		
		# loop over the length of the splitted word
		p = []
		for i in range(word_len):
			# print "i = %d" %(i)
			letter = word[i]
			# if the letter cannot be absorbed by the current node (not in the children set of current node)
			# add the letter to the children set of the current node
			if letter not in node.children: 
	#			print "letter %s is not in node's children" %(letter)
				#if letter != '*':
				node.children[letter] = LexicalTree(letter,tuple(p[:]))
				#else:
				#	node.children[letter] = LexicalTree([])
	#			print node.children[letter].path
	#		else:
	#			print "letter %s is in children" %(letter)
	#			print node.children[letter].path
				#node.children[letter].path = p_tmp
				#print "path till this letter is ",node.children[letter].path
			#if node.children[letter].word:
				#print "warning there is a word here %s" %(letter)

			# absorb one letter
			# print "absorbed one letter %s, going to this letter branch" %(letter)
			node = node.children[letter]
			#if node.path == []:
			#	node.path = p
			p.append(letter)
			#for c in node.children:
			#	print c,
		# at the end of the branch, associate the actual full word with the leaf		
		head = self.children['*']
		node.children['*'] = head
		node.word = actual_word	
# read dictionary file into a trie
def load_dictionary(words):
	# init a lexical tree
	trie = LexicalTree('',tuple([]))
	# for each word in the dictionary, insert them to the current lexical tree
	for word in words:
		word = '*' + word
		#WordCount += 1
	#	print "============\nword is %s" %word
		trie.insert( word )

	# return the head of the lexical tree	
	return trie

def traverse(trie, char, ind):
	# given a lexical tree ???
	print_str = '%s:%s' %(char,trie.path)
	#print_str = '%s %s' %(char, trie.children.keys())
	with indent(ind):  puts(print_str)
	for letter in trie.children:
		#if trie.children[letter].word:
		#	print "don't add here"
		#print "letter = %s" %(letter)
		c = trie.children[letter]
		#print letter,c.path
		#traverse(trie.children[letter], letter,trie.children[letter].path, ind + 1) 
		traverse(c, letter, ind+1)
	#print "\n"
	
def find_good_children(path_cost, cur_row_matrix_tmp, upper, queue_len):
	#results = [] # a list of paths which have low cost
	row_matrix = {}
	actual_q_len = len(cur_row_matrix_tmp)
	
	if actual_q_len > queue_len: # sort and return the first [:queue_len] 

		sorted_path_cost = sorted(path_cost.items(), key=itemgetter(1))
		#print "#########"
		#print cur_row_matrix_tmp
		# for each TrieNode in cur_row_matrix_tmp, find out the ones with a cost not exceed upper
		# cur_row_matrix_tmp is a dictionary: key is a tuple of path, value is a row of cost: currentRow
		count = 0
		for item in sorted_path_cost[:queue_len]:
			path,cost = item
			if cost <= upper:
				row_matrix[path] = cur_row_matrix_tmp[path]
	else: 
		for item in cur_row_matrix_tmp: 
			r = cur_row_matrix_tmp[item]
			if r[-1] <= upper:
				row_matrix[item] = r
	
	print len(row_matrix)
	#path_cost.clear()
	#cur_row_matrix_tmp.clear()
	return row_matrix

def BFS(trie, Q, word, width, wordPunish, remember_factor, queue_len):
	# given a queue Q (a list of TrieNode), word (target word, e.g. *apple), and width(BeamWidth)
	# return a dictionary (key: candidate words; value: cost)
	results = {}
	#replacePunish = 1
	#wordPunish = 2
	#remember_factor = 5
	# path_row: dictionary. Key: a tuple of path so far. Value: a list: currentRow
	global path_row
	# initialize prev_row_matrix structure
	# for the dummy star '*', its path is (), add a dummy row [0,1,2,3] to prev_row_matrix
	#path = tuple([])
	#path = list(path)
	#path.append('*')
	#print path
	#path = tuple(path)
	word_len = len(word)
	word = list(word)
	#word[-1] += '$'
	#currentRow = range( word_len)
	cur_row=[0] # for the first '*'
	#prev_row_matrix = {}
	#prev_row_matrix[path] = currentRow
	path_row[tuple([trie.children['*']])] = cur_row
	columns = word_len
	min_prev_cost = [9999]
	#print prev_row_matrix
	
	in_count = 0
	out_count = 0
	for i in range( 1,word_len):
		letter_start_t = time()
		cost = []
		cur_row_matrix_tmp = {} # key: tuple of path; value: list (cur_row)
		path_cost = {} # key: tuple of path; value: cost
		#print "==============="
		print word[i]
		while(Q.IsEmpty() == False):
			path = Q.dequeue()
			node = path[-1]
			parent_letter = node.letter
			prev_row = path_row[path]
			#print "================"
			#print "p_letter: ",parent_letter
			#print prev_row
			#construct a row for each of its children
			for letter in node.children:
				cur_node = node.children[letter]
			#	if letter == '*':
			#		print "------"
			#		print cur_node.letter
				cur_path = path + tuple([cur_node])
				#print cur_path
				# append the prev_row
				if parent_letter == word[i]:
					prev_row.append(prev_row[-1])
				else:
					prev_row.append(prev_row[-1] + 1)
				if letter == '*':
					cur_row = [x + wordPunish for x in prev_row]
					Q.enqueue(cur_path)
					path_row[cur_path] = cur_row
					#cur_row_matrix_tmp[cur_path] = cur_row
					#cost.append(cur_row[-1])
					continue
				
				# add prev_row to cost & cur_row_matrix_tmp
				cur_row_matrix_tmp[path] = prev_row 
				path_cost[path] = prev_row[-1]
				cost.append(prev_row[-1])


				# calculate the cur_row
				# if partically there:
				if cur_path in path_row:
					in_count += 1
					cur_row = path_row[cur_path]
					row_len = len(cur_row)
					for j in range(row_len, i):
						# allow insert star:
						if letter == '*':
							cur_row = prev_row[:]
							#print "cur_row: ", cur_row
							#print "cur_path: ", cur_path
							#insertCost = prev_row[j]
							#deleteCost = cur_row[j-1] + 1 
							#replaceCost = prev_row[j-1] + 1 

							#cur_row.append(min(insertCost, deleteCost, replaceCost) + wordPunish)
						else:
							insertCost = prev_row[j] + 1
	                                                deleteCost = cur_row[j-1] + 1
							if letter.startswith(word[i]) is False:
                                                        	replaceCost = prev_row[j-1] + 1
	                                                else:
                                                        	replaceCost = prev_row[j-1]

							cur_row.append(min(insertCost, deleteCost, replaceCost))
						'''
						insertCost = prev_row[j] + 1
	                                        deleteCost = cur_row[j-1] + 1
        	                                if letter.startswith(word[i]) is False:
                	                                replaceCost = prev_row[j-1] + 1
                        	                else:
                                	                replaceCost = prev_row[j-1]
						if letter != '*':
                                        		cur_row.append(min(insertCost, deleteCost, replaceCost))
						else:
                                        		cur_row.append(min(insertCost, deleteCost, replaceCost) + wordPunish)
						'''
				# if not there:
				else:
					out_count += 1
					#cur_row = [prev_row[0] + 1]

					m = max(i+1-remember_factor,1)
					cur_row = prev_row[0:m]
					#for j in range(1, i+1):
					for j in range(m,i+1):
						# for all the letters so far
						if letter == '*':

							cur_row = prev_row[:]
							#print "cur_row: ", cur_row
							#print "cur_path: ", cur_path
							#insertCost = prev_row[j]
							#deleteCost = cur_row[j-1] + 1
							#replaceCost = prev_row[j-1] + 1

							#cur_row.append(min(insertCost, deleteCost, replaceCost) + wordPunish)
						else:
							insertCost = prev_row[j] + 1
	                                                deleteCost = cur_row[j-1] + 1
							if letter.startswith(word[i]) is False:
                                                        	replaceCost = prev_row[j-1] + 1
	                                                else:
                                                        	replaceCost = prev_row[j-1]

							cur_row.append(min(insertCost, deleteCost, replaceCost))
						'''
						insertCost = prev_row[j] + 1
						deleteCost = cur_row[j-1] + 1
						if letter.startswith(word[i]) is False:
							replaceCost = prev_row[j-1] + 1
						else:
							replaceCost = prev_row[j-1]
						if letter != '*':
							cur_row.append(min(insertCost, deleteCost, replaceCost))
						else:	
                                        		cur_row.append(min(insertCost, deleteCost, replaceCost) + wordPunish)
						'''
				#print cur_row
				path_row[cur_path] = cur_row
				cur_row_matrix_tmp[cur_path] = cur_row
				path_cost[cur_path] = cur_row[-1]
				cost.append(cur_row[-1])
		# select and enqueue good children
		
		#print "cost:", min(cost)	
		upper = min(cost) + width
		path_row = find_good_children(path_cost, cur_row_matrix_tmp, upper, queue_len)
		for item in path_row:
			Q.enqueue(item)
		letter_end_t = time()
		print "time interval: %f" %(letter_end_t - letter_start_t)

	print_path(cur_row_matrix_tmp,20)
	print "in_count = ", in_count
	print "out_count = ", out_count

def print_path(path_r,n):
	# key: path, value: row
	
	# create a temp dictionary: key: path, value: currentRow[-1]
	dict_tmp = {}
#	for k in path_r:
#		dict_tmp[k] = path_r[k][-1]
#	path_r = sorted(dict_tmp.items(),key=itemgetter(1))

	for keys in path_r:
		tmp_str = ''
		for k in keys:
			#tmp_str += ' '
			tmp_str += k.letter
#		tmp_str += ': ' + str(path_r[keys][-1])	
#		print tmp_str
		#tmp_str = tmp_str.replace('*',' ').replace('$','').strip()
		dict_tmp[tmp_str] = path_r[keys][-1]

	#return results
	best_results = sorted(dict_tmp.items(), key=itemgetter(1))[:n]
	for item in best_results:
		segmentation, cost = item
		segmentation = segmentation.replace('*',' ').replace('$','').strip()
		print "%d\t%s" %(cost,segmentation)
def Nbest(Results, N):
	results = sorted(Results.items(), key=itemgetter(1))
	return results[:min(N, len(results))]
def load_story(fn):
	story = []
	lines = open(fn).readlines()
	for l in lines:
		words = l.split()
		for w in words:
			story.append(w)
	return story
def main():	
	parse = argparse.ArgumentParser()
	#parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
	#parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
	parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
	parse.add_argument('-p','--word-penalty', action="store", dest="word_penalty", default=2.0, help='set word penalty default: -p 2')
	parse.add_argument('-r','--remember-factor', action="store", dest="remember_factor", default=50, help='set remember factor default: -r 50')
	parse.add_argument('-q','--queue-length', action="store", dest="queue_len", default=5000, help='set queue length: -q 5000')
	parse.add_argument('-f','--file', action="store", dest="story_fn", default='story', help='dict file')
	res = parse.parse_args()
	#q = MyQUEUE()
	dictionary = []
	dictionary = open('dict').readlines()
	dictionary = map(strip,dictionary)
	trie = load_dictionary(dictionary)
	'''
	story = load_story('story') 
	#story = map(strip, story)

	beamwidth = 2
	
	for w in story:
		q = MyQUEUE()
		target_word = '*' + w
		q.enqueue(trie.children['*'])
		all_results = BFS(q,target_word,beamwidth)
		N_best_results = Nbest(all_results,1)
		print target_word, N_best_results
	'''
	#traverse(trie, "", 1)
	#letter = "*"
	#print trie.children
	#print trie.children[letter].path
#	print trie.children[letter].children['a$'].path
	#print "........"
	start_t = time()
	fn = res.story_fn
	beamwidth = int(res.beam_width)
	remember_factor = int(res.remember_factor)
	word_penalty = float(res.word_penalty)
	queue_len = int(res.queue_len)
	q = MyQUEUE()
	q.enqueue(tuple([trie.children['*']]))
	target_word = '*' + open(fn).readline().strip('\n')
	#target_word = '*appleanandabbad'
	result_all = BFS(trie,q,target_word,beamwidth,word_penalty,remember_factor,queue_len)
	end_t = time()
	print "total time elapsed: %f" %(end_t - start_t) 
	#N_best_results = Nbest(result_all, 10)
	#print N_best_results
	
	#matrix = {}
	#matrix[trie.children['*']] = [2,3,4,5]
	#print matrix
	#node = q.dequeue()
	#node = node.children['*']
	#traverse(node,"",1)
	#print node
	#for c in node.children:
	#	print "==\n", c, "\n=="
	#	traverse(node.children[c],c,1)
	#target_word = '*abcd'
	#target_word = list(target_word)
	#target_word[len(target_word)-1] += '$'
	#print target_word
	#results = search(target_word,  trie, 2)
		
	#print results
	#trie = trie.children['*'].children['a']

	#traverse(trie, "", 1)

	#target_word = '*aaronson'
	#print search(target_word,trie,10)
def main1():
	parse = argparse.ArgumentParser()
	parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
	parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
	parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
	parse.add_argument('-f','--dict-file', action="store", dest="dict_file", default='dict.txt', help='dict file')
	

	res = parse.parse_args()
	dictionary = []
	word = res.target_word
	if res.usr_dict:
		print "using user specific dictionary: ideal for trellis"	
		dictionary = res.usr_dict
	
	else:
		dictionary = open(res.dict_file).readlines()
		dictionary = map(strip, dictionary)
		
	#print len(dictionary)
		
	trie = load_dictionary(dictionary)
	traverse(trie, "*", 1)
	
	target_word = res.target_word 
	target_word = '*' + target_word
	target_word = list(target_word)
	
	target_word[len(target_word)-1] += '$'
	print target_word
	results = search(target_word,  trie, int(res.beam_width))
	
	print results
	
if __name__ == "__main__":
	main()

	
