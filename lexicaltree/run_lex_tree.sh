#!/bin/bash 

setting=("story_no_error_short" "story_with_error_short")
for f in "${setting[@]}"
do
  beam=(5 10 15)
  for b in "${beam[@]}"
  do 
      echo  "./LexicalTreeContinousPartial.py -b $b -p 2 -r 10 -f $f > $f-$b.log"
      ./LexicalTreeContinousPartial.py -b $b -p 2 -r 10 -f $f > $f-$b.log
# ./LexicalTreeContinousPartial.py  -b 10 -p 2 -r 10 -f story_no_error
# ./LexicalTreeContinousPartial.py  -b 15 -p 2 -r 10 -f story_no_error
# ./LexicalTreeContinousPartial.py  -b 5 -p 2 -r 10 -f story_with_error
# ./LexicalTreeContinousPartial.py  -b 10 -p 2 -r 10 -f story_with_error
# ./LexicalTreeContinousPartial.py  -b 15 -p 2 -r 10 -f story_with_error
  done 
done
