#!/usr/bin/env python
import time
import argparse
from string import strip
from clint.textui import indent, puts
from operator import itemgetter
#NodeCount = 0
#WordCount = 0

# path_row: dictionary. Key: a tuple of path so far. Value: a list: currentRow
path_row = {}
class MyQUEUE:
	def __init__(self):
		self.holder = []
	def enqueue(self,val):
		self.holder.append(val)
	def dequeue(self):
		val = None
		try:
			val = self.holder[0]
			if len(self.holder) == 1:
				self.holder = []
			else:
				self.holder = self.holder[1:]
		except:
			pass
		return val
	def IsEmpty(self):
		result = False
		if len(self.holder) == 0:
			result = True
		return result

class LexicalTree:
#	def __init__(self):
#		self.word = None
#		self.children = {}
#		self.path = []
	def __init__(self,l,p):
		self.letter = l
		self.word = None
		self.children = {}
		self.path = p
	def insert (self, word):
		# insert a word to the tree
		node = self
		# record the full word
		actual_word = word
		# split the word into letters, the last letter combined with a $
		word = list(word)
		word_len = len(word)
		word[word_len - 1] += '$'
		
		# loop over the length of the splitted word
		p = []
		for i in range(word_len):
			# print "i = %d" %(i)
			letter = word[i]
			# if the letter cannot be absorbed by the current node (not in the children set of current node)
			# add the letter to the children set of the current node
			if letter not in node.children: 
	#			print "letter %s is not in node's children" %(letter)
				#if letter != '*':
				node.children[letter] = LexicalTree(letter,tuple(p[:]))
				#else:
				#	node.children[letter] = LexicalTree([])
	#			print node.children[letter].path
	#		else:
	#			print "letter %s is in children" %(letter)
	#			print node.children[letter].path
				#node.children[letter].path = p_tmp
				#print "path till this letter is ",node.children[letter].path
			#if node.children[letter].word:
				#print "warning there is a word here %s" %(letter)

			# absorb one letter
			# print "absorbed one letter %s, going to this letter branch" %(letter)
			node = node.children[letter]
			#if node.path == []:
			#	node.path = p
			p.append(letter)
			#for c in node.children:
			#	print c,
		# at the end of the branch, associate the actual full word with the leaf		
		head = self.children['*']
		node.children['*'] = head
		node.word = actual_word	
# read dictionary file into a trie
def load_dictionary(words):
	# init a lexical tree
	trie = LexicalTree('',tuple([]))
	# for each word in the dictionary, insert them to the current lexical tree
	for word in words:
		word = '*' + word
		#WordCount += 1
	#	print "============\nword is %s" %word
		trie.insert( word )

	# return the head of the lexical tree	
	return trie

def traverse(trie, char, ind):
	# given a lexical tree ???
	print_str = '%s:%s' %(char,trie.path)
	#print_str = '%s %s' %(char, trie.children.keys())
	with indent(ind):  puts(print_str)
	for letter in trie.children:
		#if trie.children[letter].word:
		#	print "don't add here"
		#print "letter = %s" %(letter)
		c = trie.children[letter]
		#print letter,c.path
		#traverse(trie.children[letter], letter,trie.children[letter].path, ind + 1) 
		traverse(c, letter, ind+1)
	#print "\n"
	
def find_good_children(cur_row_matrix_tmp, upper):
	#results = [] # a list of paths which have low cost
	row_matrix = {}
	#print "#########"
	#print cur_row_matrix_tmp
	# for each TrieNode in cur_row_matrix_tmp, find out the ones with a cost not exceed upper
	# cur_row_matrix_tmp is a dictionary: key is a tuple of path, value is a row of cost: currentRow
	for item in cur_row_matrix_tmp: 
		r = cur_row_matrix_tmp[item]
		if r[-1] <= upper:
			#results.append(item)
			#results.append(item)
			#p = list(item.path)
			#p.append(item.letter)
			row_matrix[item] = r
	print len(row_matrix)
	return row_matrix

def BFS(trie, Q, word, width):
	# given a queue Q (a list of TrieNode), word (target word, e.g. *apple), and width(BeamWidth)
	# return a dictionary (key: candidate words; value: cost)
	results = {}
	replacePunish = 1
	wordPunish = 1
	# path_row: dictionary. Key: a tuple of path so far. Value: a list: currentRow
	global path_row
	# initialize prev_row_matrix structure
	# for the dummy star '*', its path is (), add a dummy row [0,1,2,3] to prev_row_matrix
	#path = tuple([])
	#path = list(path)
	#path.append('*')
	#print path
	#path = tuple(path)
	word_len = len(word)
	word = list(word)
	#word[-1] += '$'
	currentRow = range( word_len)
	#prev_row_matrix = {}
	#prev_row_matrix[path] = currentRow
	path_row[tuple([trie.children['*']])] = currentRow
	columns = word_len
	min_prev_cost = [9999]
	#print prev_row_matrix
	while(1):
		flag = 0
		cost = []
		cur_row_matrix_tmp = {}
		while(Q.IsEmpty()==False):
			path = Q.dequeue()
			node = path[-1]
			previousRow = path_row[path]
			#p = node.path
			#p = list(p)
			#p.append(node.letter)
			#p = tuple(p)
		#	print node.letter, p
			#print "-------"
			#print node.letter, p
		#	print "prev_row_matrix: ", prev_row_matrix
		#	print "previousRow: ",previousRow	
		#	cur_row_matrix_tmp = {}
		#	cost = []
			for letter in node.children:
		#		print "-------"
		#		print letter
				# calculate the current row using edit distance
				cur_node = node.children[letter]
				currentRow = [ previousRow[0] + 1 ]
		#		print currentRow
			        # Build one row for the letter, with a column for each letter in the target
			        # word, plus one for the empty string at column 0
        			for column in xrange( 1, columns ):
				
					if letter == '*':
						insertCost = currentRow[column - 1] + 1
						deleteCost = previousRow[column] # allowing inserting '*'
						replaceCost = previousRow[ column - 1 ] + 1 + replacePunish
						currentRow.append( min( insertCost, deleteCost, replaceCost ) + wordPunish)
					else:
						insertCost = currentRow[column - 1] + 1
                                                deleteCost = previousRow[column] + 1
						if  letter.startswith(word[column]) is False:
	                                                replaceCost = previousRow[ column - 1 ] + 1
        	                                else:
                	                                replaceCost = previousRow[ column - 1 ]
						currentRow.append( min( insertCost, deleteCost, replaceCost ) )


					'''	
                			insertCost = currentRow[column - 1] + 1
                			
					if letter != '*':
						deleteCost = previousRow[column] + 1
					else:
						deleteCost = previousRow[column]

                			if  letter.startswith(word[column]) is False:
                        			replaceCost = previousRow[ column - 1 ] + 1
                			else:
                        			replaceCost = previousRow[ column - 1 ]
					'''
                			#currentRow.append( min( insertCost, deleteCost, replaceCost ) )
		#			print column, min( insertCost, deleteCost, replaceCost )
		#		print currentRow
				# construct a new path containing the current node
				# add this new path and currentRow to path_row
				currentPath = path + tuple([node.children[letter]])
				path_row[currentPath] = currentRow
				'''
				if cur_node.word != None:
					results[cur_node.word] = currentRow[-1]
				'''
				
				#flag = 1 # the program can still go one level deeper
				# record cur_node & currentRow. Some of them will be to create a new prev_row_matrix later
				cur_row_matrix_tmp[currentPath] = currentRow
				cost.append(currentRow[-1])
				
		# stop condition:
		# cur_row_matrix_tmp's minimum cost (min(cost)) is worse than prev_row_matrix_tmp's minimum (min_prev_cost)
		#print_path(cur_row_matrix_tmp,1)
		if min(cost) > min(min_prev_cost):
			break

		print min(cost), min(min_prev_cost)
		min_prev_cost = cost
		#if (flag == 0):
		#	break
		#print "updating ... "
		#print cost		
		upper = min(cost) + width
		#print cur_row_matrix_tmp
		# find out a list of good children for 'node', create a new prev_row_matrix
		#good_children, prev_row_matrix = find_good_children(cur_row_matrix_tmp, upper)
		path_row= find_good_children(cur_row_matrix_tmp, upper)
		#print "good children: ", good_children
		#print "prev_row_matrix: ",prev_row_matrix
		#print "***************************"
		for item in path_row:
			Q.enqueue(item)
		prev_row_matrix_tmp = cur_row_matrix_tmp.copy()


		#if (flag == 0):
		#	break
		#else:
		#print "go down one level"	

	print_path(prev_row_matrix_tmp,5)

def print_path(path_r,n):
	# key: path, value: row
	
	# create a temp dictionary: key: path, value: currentRow[-1]
	dict_tmp = {}
#	for k in path_r:
#		dict_tmp[k] = path_r[k][-1]
#	path_r = sorted(dict_tmp.items(),key=itemgetter(1))

	for keys in path_r:
		tmp_str = ''
		for k in keys:
			tmp_str += ' '
			tmp_str += k.letter
#		tmp_str += ': ' + str(path_r[keys][-1])	
#		print tmp_str
		dict_tmp[tmp_str] = path_r[keys][-1]

	#return results
	print sorted(dict_tmp.items(), key=itemgetter(1))[:n]
def Nbest(Results, N):
	results = sorted(Results.items(), key=itemgetter(1))
	return results[:min(N, len(results))]
def load_story(fn):
	story = []
	lines = open(fn).readlines()
	for l in lines:
		words = l.split()
		for w in words:
			story.append(w)
	return story
def main():	
	parse = argparse.ArgumentParser()
	#parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
	#parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
	parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
	parse.add_argument('-f','--file', action="store", dest="story_fn", default='story', help='dict file')
	res = parse.parse_args()
	#q = MyQUEUE()
	dictionary = []
	dictionary = open('dict').readlines()
	dictionary = map(strip,dictionary)
	trie = load_dictionary(dictionary)
	'''
	story = load_story('story') 
	#story = map(strip, story)

	beamwidth = 2
	
	for w in story:
		q = MyQUEUE()
		target_word = '*' + w
		q.enqueue(trie.children['*'])
		all_results = BFS(q,target_word,beamwidth)
		N_best_results = Nbest(all_results,1)
		print target_word, N_best_results
	'''
	#traverse(trie, "", 1)
	#letter = "*"
	#print trie.children
	#print trie.children[letter].path
#	print trie.children[letter].children['a$'].path
	#print "........"

	fn = res.story_fn
	q = MyQUEUE()
	q.enqueue(tuple([trie.children['*']]))
	target_word = '*' + open(fn).readline().strip('\n')
	#target_word = '*appleanandabbad'
	beamwidth = int(res.beam_width)
	result_all = BFS(trie,q,target_word,beamwidth)
	#N_best_results = Nbest(result_all, 10)
	#print N_best_results
	
	#matrix = {}
	#matrix[trie.children['*']] = [2,3,4,5]
	#print matrix
	#node = q.dequeue()
	#node = node.children['*']
	#traverse(node,"",1)
	#print node
	#for c in node.children:
	#	print "==\n", c, "\n=="
	#	traverse(node.children[c],c,1)
	#target_word = '*abcd'
	#target_word = list(target_word)
	#target_word[len(target_word)-1] += '$'
	#print target_word
	#results = search(target_word,  trie, 2)
		
	#print results
	#trie = trie.children['*'].children['a']

	#traverse(trie, "", 1)

	#target_word = '*aaronson'
	#print search(target_word,trie,10)
def main1():
	parse = argparse.ArgumentParser()
	parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
	parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
	parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
	parse.add_argument('-f','--dict-file', action="store", dest="dict_file", default='dict.txt', help='dict file')
	

	res = parse.parse_args()
	dictionary = []
	word = res.target_word
	if res.usr_dict:
		print "using user specific dictionary: ideal for trellis"	
		dictionary = res.usr_dict
	
	else:
		dictionary = open(res.dict_file).readlines()
		dictionary = map(strip, dictionary)
		
	#print len(dictionary)
		
	trie = load_dictionary(dictionary)
	traverse(trie, "*", 1)
	
	target_word = res.target_word 
	target_word = '*' + target_word
	target_word = list(target_word)
	
	target_word[len(target_word)-1] += '$'
	print target_word
	results = search(target_word,  trie, int(res.beam_width))
	
	print results
	
if __name__ == "__main__":
	main()

	
