#!/usr/bin/env python
from time import time
import argparse
from string import strip
from clint.textui import indent, puts
from operator import itemgetter
import sys
#NodeCount = 0
#WordCount = 0

class MyQUEUE:
	def __init__(self):
		self.holder = []
	def enqueue(self,val):
		self.holder.append(val)
	def dequeue(self):
		val = None
		try:
			val = self.holder[0]
			if len(self.holder) == 1:
				self.holder = []
			else:
				self.holder = self.holder[1:]
		except:
			pass
		return val
	def IsEmpty(self):
		result = False
		if len(self.holder) == 0:
			result = True
		return result


'''
class TrieNode:
	def __init__(self):
		self.word = None
		self.children = {}

		#global NodeCount
		#NodeCount += 1

	def insert( self, word ):
		node = self
		actual_word = word[1:]
		word = list(word)
		word[len(word) - 1] += '$'
		for letter in word:
			if letter not in node.children: 
				
				node.children[letter] = TrieNode()
			
			#if node.children[letter].word:
				#print "warning there is a word here %s" %(letter)
			node = node.children[letter]
		
		node.word = actual_word
'''
class LexicalTree:
#	def __init__(self):
#		self.word = None
#		self.children = {}
#		self.path = []
	def __init__(self,l,p):
		self.letter = l
		self.word = None
		self.children = {}
		self.path = p
	def insert (self, word):
		# insert a word to the tree
		node = self
		# record the full word
		actual_word = word
		# split the word into letters, the last letter combined with a $
		word = list(word)
		word_len = len(word)
		word[word_len - 1] += '$'
		
		# loop over the length of the splitted word
		p = []
		for i in range(word_len):
			# print "i = %d" %(i)
			letter = word[i]
			# if the letter cannot be absorbed by the current node (not in the children set of current node)
			# add the letter to the children set of the current node
			if letter not in node.children: 
	#			print "letter %s is not in node's children" %(letter)
				#if letter != '*':
				node.children[letter] = LexicalTree(letter,tuple(p[:]))
				#else:
				#	node.children[letter] = LexicalTree([])
	#			print node.children[letter].path
	#		else:
	#			print "letter %s is in children" %(letter)
	#			print node.children[letter].path
				#node.children[letter].path = p_tmp
				#print "path till this letter is ",node.children[letter].path
			#if node.children[letter].word:
				#print "warning there is a word here %s" %(letter)

			# absorb one letter
			# print "absorbed one letter %s, going to this letter branch" %(letter)
			node = node.children[letter]
			#if node.path == []:
			#	node.path = p
			p.append(letter)
			#for c in node.children:
			#	print c,
		# at the end of the branch, associate the actual full word with the leaf		
		node.word = actual_word	
# read dictionary file into a trie
def load_dictionary(words):
	# init a lexical tree
	trie = LexicalTree('',tuple([]))
	# for each word in the dictionary, insert them to the current lexical tree
	for word in words:
		word = '*' + word
		#WordCount += 1
	#	print "============\nword is %s" %word
		trie.insert( word )

	# return the head of the lexical tree	
	return trie

def traverse(trie, char, ind):
	# given a lexical tree ???
	print_str = '%s:%s' %(char,trie.path)
	#print_str = '%s %s' %(char, trie.children.keys())
	with indent(ind):  puts(print_str)
	for letter in trie.children:
		#if trie.children[letter].word:
		#	print "don't add here"
		#print "letter = %s" %(letter)
		c = trie.children[letter]
		#print letter,c.path
		#traverse(trie.children[letter], letter,trie.children[letter].path, ind + 1) 
		traverse(c, letter, ind+1)
	#print "\n"
	

def find_good_children(cur_row_matrix_tmp, upper):
	results = [] # a list of TrieNodes which have low cost
	row_matrix = {}
	#print "#########"
	#print cur_row_matrix_tmp
	# for each TrieNode in cur_row_matrix_tmp, find out the ones with a cost not exceed upper
	# cur_row_matrix_tmp is a dictionary: key is a TrieNode, value is a row of cost
	for item in cur_row_matrix_tmp:
		r = cur_row_matrix_tmp[item]
		if r[-1] <= upper:
			results.append(item)
			p = list(item.path)
			p.append(item.letter)
			row_matrix[tuple(p)] = r
	return results, row_matrix

def BFS(Q, word, width):
	# given a queue Q (a list of TrieNode), word (target word, e.g. *apple), and width(BeamWidth)
	# return a dictionary (key: candidate words; value: cost)
	results = {}

	# initialize prev_row_matrix structure
	# for the dummy star '*', its path is (), add a dummy row [0,1,2,3] to prev_row_matrix
	path = tuple([])
	path = list(path)
	path.append('*')
	#print path
	path = tuple(path)
	word_len = len(word)
	word = list(word)
	#word[-1] += '$'
	currentRow = range( word_len)
	prev_row_matrix = {}
	prev_row_matrix[path] = currentRow
	columns = word_len

	#print prev_row_matrix
	while(1):
		flag = 0
		cost = []
		cur_row_matrix_tmp = {}
		while(Q.IsEmpty()==False):
			node = Q.dequeue()
			p = node.path
			p = list(p)
			p.append(node.letter)
			p = tuple(p)
		#	print node.letter, p
			#print "-------"
			#print node.letter, p
		#	print "prev_row_matrix: ", prev_row_matrix
			previousRow = prev_row_matrix[p]
		#	print "previousRow: ",previousRow	
		#	cur_row_matrix_tmp = {}
		#	cost = []
			for letter in node.children:
		#		print "-------"
		#		print letter
				# calculate the current row using edit distance
				cur_node = node.children[letter]
				currentRow = [ previousRow[0] + 1 ]
		#		print currentRow
			        # Build one row for the letter, with a column for each letter in the target
			        # word, plus one for the empty string at column 0
        			for column in xrange( 1, columns ):
					
                			insertCost = currentRow[column - 1] + 1
                			deleteCost = previousRow[column] + 1

                			if  letter.startswith(word[column]) is False:
                        			replaceCost = previousRow[ column - 1 ] + 1
                			else:
                        			replaceCost = previousRow[ column - 1 ]

                			currentRow.append( min( insertCost, deleteCost, replaceCost ) )
		#			print column, min( insertCost, deleteCost, replaceCost )
		#		print currentRow
				if cur_node.word != None:
					results[cur_node.word] = currentRow[-1]
				else:
					flag = 1 # the program can still go one level deeper
					# record cur_node & currentRow. Some of them will be to create a new prev_row_matrix later
					cur_row_matrix_tmp[cur_node] = currentRow
					cost.append(currentRow[-1])
		if (flag == 0):
			break
		#print "updating ... "
		#print cost		
		upper = min(cost) + width
		#print cur_row_matrix_tmp
		# find out a list of good children for 'node', create a new prev_row_matrix
		good_children, prev_row_matrix = find_good_children(cur_row_matrix_tmp, upper)
		#print "good children: ", good_children
		#print "prev_row_matrix: ",prev_row_matrix
		#print "***************************"
		for item in good_children:
			Q.enqueue(item)
		#if (flag == 0):
		#	break
		#else:
		#print "go down one level"	
	return results
	#print sorted(results.items(), key=itemgetter(1))
def Nbest(Results, N):
	results = sorted(Results.items(), key=itemgetter(1))
	return results[:min(N, len(results))]
def load_story(fn):
	story = []
	lines = open(fn).readlines()
	for l in lines:
		words = l.split()
		for w in words:
			story.append(w)
	return story
def main():	
	parse = argparse.ArgumentParser()
	#parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
	parse.add_argument('-d', '--dict', action="store", dest="dict_file", default="dict", help="user_defined dictionary")
	parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
	parse.add_argument('-s','--story-file', action="store", dest="story_file", default='story', help='set story file(segmented)')
	parse.add_argument('-l','--log-file', action="store", dest="log_file", default='singal_word_log', help='set log file')	
	parse.add_argument('-i','--interactive_mode', action="store", dest="mode", default='no', help='set mode')
		
	res = parse.parse_args()
	dict_fn = res.dict_file
	beamwidth = int(res.beam_width)
	story_fn = res.story_file
	log_fn = res.log_file
	mode = res.mode
	dictionary = []
	#dictionary = open('dict').readlines()
	dictionary = open(dict_fn).readlines()
	dictionary = map(strip,dictionary)
	trie = load_dictionary(dictionary)

	if (mode == 'yes'):
		w = sys.stdin.readline().strip('\n')
		#print word
		target_word = '*' + w
                q = MyQUEUE()
                #target_word = '*' + w
                q.enqueue(trie.children['*'])
                all_results = BFS(q,target_word,beamwidth)
                N_best_results = Nbest(all_results,10)
                print target_word, N_best_results
		return;
	# construct a dict{} for dictionary
	my_dict = {}
	for d in dictionary:
		d = '*' + d
		my_dict[d] = d

	#story = load_story('story_short') 
	story = load_story(story_fn) 
	#log = open('singal_word_log','w')
	log = open(log_fn,'w')
	#story = map(strip, story)

	start_t = time()	
	str_tmp = ''
	for w in story:
		target_word = '*' + w
		if target_word in my_dict:
			str_tmp += my_dict[target_word]
			continue
		q = MyQUEUE()
		#target_word = '*' + w
		q.enqueue(trie.children['*'])
		all_results = BFS(q,target_word,beamwidth)
		N_best_results = Nbest(all_results,1)
		print target_word, N_best_results
		# we take the first result of the N-best list. Might not be the best one. Randomness
		matched_word, cost = N_best_results[0]
		my_dict[target_word] = matched_word
		str_tmp += str(matched_word)
	str_tmp = str_tmp.replace('*',' ').replace('\n','')
	log.write(str_tmp)
	end_t = time()	
	print "total time elapsed: %f" %(end_t - start_t)
	total_time = end_t - start_t
	log.write("\ntotal time: %s\n" %(str(total_time)))
	#log.write(total_time)
	log.close()
	#traverse(trie, "", 1)
	#letter = "*"
	#print trie.children
	#print trie.children[letter].path
#	print trie.children[letter].children['a$'].path
	#print "........"

	'''
	q = MyQUEUE()
	q.enqueue(trie.children['*'])
	target_word = '*apple'
	beamwidth = 10
	result_all = BFS(q,target_word,beamwidth)
	N_best_results = Nbest(result_all, 10)
	print N_best_results
	'''
if __name__ == "__main__":
	main()

	
