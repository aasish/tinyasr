$log_fn = shift;
$truth_fn = shift; # truth file with uppercase
$tmp = "out.tmp";
$word_align_cmd = "perl ../hmm/word_align.pl -i ";

open LOG, "<$log_fn" or die "cannot open log file"; 


@lines = <LOG>;
close(LOG);

$count = 0;
$accu_accuracy = 0;
foreach my $l (@lines)
{
	next unless ($l =~ /^\d+\t/);
	#print $l;
	$l =~ m/^\d+\t(.+$)/;
	$sentence = $1;
	print "=============\n";
	$count += 1;
	#print $sentence;

	open TMP, ">$tmp" or die "cannot open tmp file"; # put the one log string with uppercase there
	$sen_upper = uc($sentence)." (SENTENCE1)";
	print TMP $sen_upper;
	my $cmd = "$word_align_cmd $truth_fn $tmp";
	#print $cmd."\n";
	my $result = `$cmd`;
	#print $result;
	$result =~ m/Accuracy = (\d\d\.\d\d)%/;
	print "$1\n";
	$accu_accuracy += $1;	
	close(TMP);
}
print "##########\n";
print "Average accuracy on $count results:\n".($accu_accuracy / $count);
print "\n";



