#!/usr/bin/env python
import time
import argparse
from string import strip
from clint.textui import indent, puts
from operator import itemgetter
#NodeCount = 0
#WordCount = 0

node_p ={}

class MyQUEUE:
	def __init__(self):
		self.holder = []
	def enqueue(self,val):
		self.holder.append(val)
	def dequeue(self):
		val = None
		try:
			val = self.holder[0]
			if len(self.holder) == 1:
				self.holder = []
			else:
				self.holder = self.holder[1:]
		except:
			pass
		return val
	def IsEmpty(self):
		result = False
		if len(self.holder) == 0:
			result = True
		return result


'''
class TrieNode:
	def __init__(self):
		self.word = None
		self.children = {}

		#global NodeCount
		#NodeCount += 1

	def insert( self, word ):
		node = self
		actual_word = word[1:]
		word = list(word)
		word[len(word) - 1] += '$'
		for letter in word:
			if letter not in node.children: 
				
				node.children[letter] = TrieNode()
			
			#if node.children[letter].word:
				#print "warning there is a word here %s" %(letter)
			node = node.children[letter]
		
		node.word = actual_word
'''
class LexicalTree:
#	def __init__(self):
#		self.word = None
#		self.children = {}
#		self.path = []
	def __init__(self,l,p):
		self.letter = l
		self.word = None
		self.children = {}
		self.path = p
	def insert (self, word):
		# insert a word to the tree
		node = self
		# record the full word
		actual_word = word
		# split the word into letters, the last letter combined with a $
		word = list(word)
		word_len = len(word)
		word[word_len - 1] += '$'
		
		# loop over the length of the splitted word
		p = []
		for i in range(word_len):
			# print "i = %d" %(i)
			letter = word[i]
			# if the letter cannot be absorbed by the current node (not in the children set of current node)
			# add the letter to the children set of the current node
			if letter not in node.children: 
	#			print "letter %s is not in node's children" %(letter)
				#if letter != '*':
				node.children[letter] = LexicalTree(letter,tuple(p[:]))
				#else:
				#	node.children[letter] = LexicalTree([])
	#			print node.children[letter].path
	#		else:
	#			print "letter %s is in children" %(letter)
	#			print node.children[letter].path
				#node.children[letter].path = p_tmp
				#print "path till this letter is ",node.children[letter].path
			#if node.children[letter].word:
				#print "warning there is a word here %s" %(letter)

			# absorb one letter
			# print "absorbed one letter %s, going to this letter branch" %(letter)
			node = node.children[letter]
			#if node.path == []:
			#	node.path = p
			p.append(letter)
			#for c in node.children:
			#	print c,
		# at the end of the branch, associate the actual full word with the leaf		
		head = self.children['*']
		node.children['*'] = head
		node.word = actual_word	
# read dictionary file into a trie
def load_dictionary(words):
	# init a lexical tree
	trie = LexicalTree('',tuple([]))
	# node_p: dictionary, key: trieNode; value: a tuple of 1) list of characters, 2) cost
	global node_p
	#node_p[trie.children['*']] = ([],99999) 
	# for each word in the dictionary, insert them to the current lexical tree
	for word in words:
		word = '*' + word
		#WordCount += 1
	#	print "============\nword is %s" %word
		trie.insert( word )

	node_p[trie.children['*']] = ([],99999) 
	# return the head of the lexical tree	
	return trie

def traverse(trie, char, ind):
	# given a lexical tree ???
	print_str = '%s:%s' %(char,trie.path)
	#print_str = '%s %s' %(char, trie.children.keys())
	with indent(ind):  puts(print_str)
	for letter in trie.children:
		#if trie.children[letter].word:
		#	print "don't add here"
		#print "letter = %s" %(letter)
		c = trie.children[letter]
		#print letter,c.path
		#traverse(trie.children[letter], letter,trie.children[letter].path, ind + 1) 
		traverse(c, letter, ind+1)
	#print "\n"
'''	
def find_good_children(cur_row_matrix_tmp, upper):
	results = [] # a list of TrieNodes which have low cost
	row_matrix = {}
	#print "#########"
	#print cur_row_matrix_tmp
	# for each TrieNode in cur_row_matrix_tmp, find out the ones with a cost not exceed upper
	# cur_row_matrix_tmp is a dictionary: key is a TrieNode, value is a row of cost
	for item in cur_row_matrix_tmp:
		r = cur_row_matrix_tmp[item]
		if r[-1] <= upper:
			results.append(item)
			p = list(item.path)
			p.append(item.letter)
			row_matrix[tuple(p)] = r
	return results, row_matrix
'''

def find_good_children(cur_row_matrix_tmp, upper):
	results = [] # a list of TrieNodes which have low cost
	row_matrix = {}
	#print "#########"
	#print cur_row_matrix_tmp
	# for each TrieNode in cur_row_matrix_tmp, find out the ones with a cost not exceed upper
	# cur_row_matrix_tmp is a dictionary: key is a TrieNode, value is a row of cost
	for item in cur_row_matrix_tmp:
		r = cur_row_matrix_tmp[item][:]
		if r[-1] <= upper:
			results.append(item)
			p = list(item.path)
			p.append(item.letter)
			row_matrix[item] = r[:]
			#row_matrix[tuple(p)] = r
	return results, row_matrix


def BFS(trie, Q, word, width):
	# given a queue Q (a list of TrieNode), word (target word, e.g. *apple), and width(BeamWidth)
	# return a dictionary (key: candidate list of characters; value: cost)
	results = {}
	# initialize prev_row_matrix structure
	# for the dummy star '*', its path is (), add a dummy row [0,1,2,3] to prev_row_matrix
	#Node_p[] = tuple([])
	#path = tuple([])
	#path = list(path)
	#path.append('*')
	#print path
	#path = tuple(path)

	# node_p: dictionary. key: trieNode; value: a tuple of 1) list of characters, 2) cost
	global node_p

	# segmentation_cost: dictionary. It records the cost of the best valid segmentation(s) ends with '*'.
	# key: one valid path so far. value: cost
	segmentation_cost = {}
	prev_segmentation_cost = {}

	word_len = len(word)
	word = list(word)
	#word[-1] += '$'
	currentRow = range( word_len)
	prev_row_matrix = {}
	prev_row_matrix[trie.children['*']] = currentRow
	columns = word_len
	p = node_p[trie.children['*']]
	p = list(p)[0]
	p.append('*')
	#print prev_row_matrix
	while(1):
		flag = 0
		cost = []
		cur_row_matrix_tmp = {}
		while(Q.IsEmpty()==False):
			node = Q.dequeue()
			print "==========="
			print node.letter
			p = node_p[node][0]
			#p = node.path
			#p = list(p)
			#p.append(node.letter)
			#p = tuple(p)

		#	print node.letter, p
			#print "========="
			#print node.letter, p
		#	print "prev_row_matrix: ", prev_row_matrix
			#print node.letter
			#for k in prev_row_matrix:
			#	print k.letter
			previousRow = prev_row_matrix[node][:]

			print "previousRow: ",previousRow	
		#	cur_row_matrix_tmp = {}
		#	cost = []
			for letter in node.children:
				print "-------"
				print letter
				# calculate the current row using edit distance
				cur_path = p + [letter]
				print "cur_path: ",cur_path	
				cur_node = node.children[letter]

				currentRow = [ previousRow[0] + 1 ]
				if 1:
				#if letter != '*':
					currentRow = [ previousRow[0] + 1 ]
					for column in xrange( 1, columns ):
						insertCost = currentRow[column - 1] + 1
						deleteCost = previousRow[column] + 1

						if  letter.startswith(word[column]) is False:
							replaceCost = previousRow[ column - 1 ] + 1
						else:
                                       	        	replaceCost = previousRow[ column - 1 ]

                                       		currentRow.append( min( insertCost, deleteCost, replaceCost ) )
				#else:
					#currentRow = range( previousRow[-1], columns + previousRow[-1])
				'''
				for column in xrange( 1, columns ):
                                        insertCost = currentRow[column - 1] + 1
					deleteCost = previousRow[column] + 1

                                        if  letter.startswith(word[column]) is False:
                       	                        replaceCost = previousRow[ column - 1 ] + 1
                              	        else:
                                       	        replaceCost = previousRow[ column - 1 ]

                                       	currentRow.append( min( insertCost, deleteCost, replaceCost ) )

				'''
#				else:
#					currentRow = previousRow[:]
				'''
				if letter != '*':
					currentRow = [ previousRow[0] + 1 ]
				else:
					currentRow = [1]
		#		print currentRow
			        # Build one row for the letter, with a column for each letter in the target
			        # word, plus one for the empty string at column 0
        			for column in xrange( 1, columns ):
					
                			insertCost = currentRow[column - 1] + 1
                			deleteCost = previousRow[column] + 1

                			if  letter.startswith(word[column]) is False:
                        			replaceCost = previousRow[ column - 1 ] + 1
                			else:
                        			replaceCost = previousRow[ column - 1 ]

                			currentRow.append( min( insertCost, deleteCost, replaceCost ) )
		#			print column, min( insertCost, deleteCost, replaceCost )
				#print currentRow
				'''
				# update node_p structure
				if letter != '*':
					print "not *"	
				#	print cur_path
				#	print currentRow[-1]

					if node.children[letter] not in node_p or currentRow[-1] <= node_p[node.children[letter]][1]:
						node_p[node.children[letter]] = (cur_path,currentRow[-1])
					#elif node.children[letter] not in node_p:
						 
					
				else:
					print "is *"
					flag = 1 # reaches a '*' in this level
					# compare the current cost with the existing cost of '*'. select the lower cost
					tuple_cur_path = tuple(cur_path)

					exist_cost = node_p[trie.children['*']][1]
					if currentRow[-1] < exist_cost:
						node_p[trie.children['*']] = (cur_path,currentRow[-1])

					if not segmentation_cost:
						segmentation_cost[tuple_cur_path] = currentRow[-1]
					else:
						if currentRow[-1] < min(segmentation_cost.values()):
							segmentation_cost.clear()
							segmentation_cost[tuple_cur_path] = currentRow[-1]
						elif currentRow[-1] == min(segmentation_cost.values()):	
							segmentation_cost[tuple_cur_path] = currentRow[-1]
				#print node_p
				#if cur_node.word != None:
				#	results[tuple(cur_path)] = currentRow[-1]
					#results[cur_node.word] = currentRow[-1]
				#else:
				#	flag = 1 # the program can still go one level deeper
					# record cur_node & currentRow. Some of them will be to create a new prev_row_matrix later
				print "currentRow: ", currentRow
				cur_row_matrix_tmp[cur_node] = currentRow[:]
				cost.append(currentRow[-1])
		#if (flag == 0):
		#	break

		# stop condition:
		# if it reaches '*', and the cost
		#min_cost =  min(cost)
		#min_seg_cost = min(segmentation_cost.values())
		#print min_cost
		#print min_seg_cost
		print "prev: ", prev_segmentation_cost
		print "cur: ", segmentation_cost


		if flag and segmentation_cost and prev_segmentation_cost:
			if min(segmentation_cost.values())>=min(prev_segmentation_cost.values()):
				break
				#pass
	
		prev_segmentation_cost = segmentation_cost.copy()
		print "segmentation_cost: ", prev_segmentation_cost

		#print "updating ... "
		#print cost		
		upper = min(cost) + width
		#print cur_row_matrix_tmp
		# find out a list of good children for 'node', create a new prev_row_matrix
		good_children, prev_row_matrix = find_good_children(cur_row_matrix_tmp, upper)
		#print "good children: ", good_children
		#print "prev_row_matrix: ",prev_row_matrix
		#print "***************************"
		for item in good_children:
			Q.enqueue(item)
		#if (flag == 0):
		#	break
		#else:
		#print "go down one level"	
	#return results
	print prev_segmentation_cost
	#print sorted(results.items(), key=itemgetter(1))
def Nbest(Results, N):
	results = sorted(Results.items(), key=itemgetter(1))
	return results[:min(N, len(results))]
def load_story(fn):
	story = []
	lines = open(fn).readlines()
	for l in lines:
		words = l.split()
		for w in words:
			story.append(w)
	return story
def main():	
	#parse = argparse.ArgumentParser()
	#parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
	#parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
	#parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
	#parse.add_argument('-f','--dict-file', action="store", dest="dict_file", default='dict.txt', help='dict file')
	#res = parse.parse_args()
	#q = MyQUEUE()
	dictionary = []
	dictionary = open('temp.dict2').readlines()
	dictionary = map(strip,dictionary)
	trie = load_dictionary(dictionary)

	story = load_story('story') 
	#story = map(strip, story)

	beamwidth = 2
	'''	
	for w in story:
		q = MyQUEUE()
		target_word = '*' + w
		q.enqueue(trie.children['*'])
		all_results = BFS(q,target_word,beamwidth)
		N_best_results = Nbest(all_results,1)
		print target_word, N_best_results
	'''
	#traverse(trie, "", 1)
	#letter = "*"
	#print trie.children
	#print trie.children[letter].path
#	print trie.children[letter].children['a$'].path
	#print "........"

	
	q = MyQUEUE()
	q.enqueue(trie.children['*'])
	target_word = '*andandand'
	beamwidth = 3
	result_all = BFS(trie,q,target_word,beamwidth)
	#N_best_results = Nbest(result_all, 10)
	#print N_best_results
	
	#matrix = {}
	#matrix[trie.children['*']] = [2,3,4,5]
	#print matrix
	#node = q.dequeue()
	#node = node.children['*']
	#traverse(node,"",1)
	#print node
	#for c in node.children:
	#	print "==\n", c, "\n=="
	#	traverse(node.children[c],c,1)
	#target_word = '*abcd'
	#target_word = list(target_word)
	#target_word[len(target_word)-1] += '$'
	#print target_word
	#results = search(target_word,  trie, 2)
		
	#print results
	#trie = trie.children['*'].children['a']

	#traverse(trie, "", 1)

	#target_word = '*aaronson'
	#print search(target_word,trie,10)
def main1():
	parse = argparse.ArgumentParser()
	parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
	parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
	parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
	parse.add_argument('-f','--dict-file', action="store", dest="dict_file", default='dict.txt', help='dict file')
	

	res = parse.parse_args()
	dictionary = []
	word = res.target_word
	if res.usr_dict:
		print "using user specific dictionary: ideal for trellis"	
		dictionary = res.usr_dict
	
	else:
		dictionary = open(res.dict_file).readlines()
		dictionary = map(strip, dictionary)
		
	#print len(dictionary)
		
	trie = load_dictionary(dictionary)
	traverse(trie, "*", 1)
	
	target_word = res.target_word 
	target_word = '*' + target_word
	target_word = list(target_word)
	
	target_word[len(target_word)-1] += '$'
	print target_word
	results = search(target_word,  trie, int(res.beam_width))
	
	print results
	
if __name__ == "__main__":
	main()

	
