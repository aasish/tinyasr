#!/usr/bin/python
import pyaudio
import wave
import sys,os.path
import select
import termios
import argparse
import math
from array import array
from struct import unpack, pack
from featext import FeatureExt
import numpy as np
import os
import signal
from hmm import hmm as discretehmm
#from continuoushmm import hmm as conthmm
from continuoushmm import opthmm as conthmm
from hmm.hmm import HMM
#from continuoushmm.hmm import HMM, BPNode
from continuoushmm.opthmm import HMM, BPNode
import subprocess
"""
General variables
"""
CHUNK_SIZE = 400
RATE = 16000
FORMAT = pyaudio.paInt16

"""
Threshold variables
"""
ENERGY_THRESHOLD = 500
DB_ENERGY_THRESHOLD = 90
ONSET_THRESHOLD = 10
TIME_THRESHOLD = 20
FORGET_FACTOR = 2
ADJUSTMENT = 0.05

background = 0
level = 0

"""
Special purpose variable
"""
JUST_SILENCE = False
SILENCE_SPAN = 50

def play_wave_file(p, wav_path):
    command = ' '.join(['aplay', wav_path])
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()

    
def is_silent(L, endpt):
    if endpt == "vanilla":
        return is_silent_vanilla(L)
    elif endpt == "energy":
        return is_silent_energy(L)
    else:
        return is_silent_adaptive(L)
    #return is_silent_vanilla(L)

def is_silent_vanilla(L):
    "Returns `True` if below the 'silent' threshold"
    return max(L) < ENERGY_THRESHOLD

def energy_per_sample_in_db(L):
    energy = 0
    for x in L:
        energy += x*x
    if energy is 0:
        energy = 1
    energy = 10 * math.log10(energy)    

    return energy

def is_silent_energy(L):
    energy = energy_per_sample_in_db(L)
    #print energy
    return energy < DB_ENERGY_THRESHOLD


def is_silent_adaptive(L):
    global level, background
    #print level, background
    current = energy_per_sample_in_db(L)
    isSilence = True
    level  = (level * FORGET_FACTOR + current) / (FORGET_FACTOR + 1)
    if current < background:
        background = current
    else:
        background += (current - background) * ADJUSTMENT
    
    if level < background: 
        level = background
        
    #print "level - background is %d " %(level - background)
    if (level - background) > ONSET_THRESHOLD:
        isSilence = False
        
    return isSilence

def normalize(L):
    "Average the volume out"
    MAXIMUM = 16384
    times = float(MAXIMUM)/max(abs(i) for i in L)

    LRtn = array('h')
    for i in L:
        LRtn.append(int(i*times))
    return LRtn

def trim(L):
    "Trim the blank spots at the start and end"
    def _trim(L):
        snd_started = False
        LRtn = array('h')

        for i in L:
            if not snd_started and abs(i)>ENERGY_THRESHOLD:
                snd_started = True
                LRtn.append(i)

            elif snd_started:
                LRtn.append(i)
        return LRtn

    # Trim to the left
    L = _trim(L)

    # Trim to the right
    L.reverse()
    L = _trim(L)
    L.reverse()
    return L

def add_silence(L, seconds):
    "Add silence to the start and end of `L` of length `seconds` (float)"
    LRtn = array('h', [0 for i in xrange(int(seconds*RATE))])
    LRtn.extend(L)
    LRtn.extend([0 for i in xrange(int(seconds*RATE))])
    return LRtn

def isData():
	return select.select([sys.stdin],[],[],0) == ([sys.stdin],[],[])

def record(p, endpt):
    """
    Record a word or words from the microphone and 
    return the data as an array of signed shorts.

    Normalizes the audio, trims silence from the 
    start and end, and pads with 0.5 seconds of 
    blank sound to make sure VLC et al can play 
    it without getting chopped off.
    """
    #p = pyaudio.PyAudio()
    stream = p.open(format=FORMAT, channels=1, rate=RATE, 
                    input=True, output=True,
                    frames_per_buffer=CHUNK_SIZE)

    num_silent = 0
    snd_started = False

    LRtn = array('h')
    
    isLevelSet = False
    isBgndSet = False
    frameCounter = 0
    background = 0
    level = 0
    done = False
    prevIsSilence = True
   
    while not done:
        data = stream.read(CHUNK_SIZE)
        L = unpack('<' + ('h'*(len(data)/2)), data) # little endian, signed short
        L = array('h', L)

        
        if not isLevelSet:
            level = energy_per_sample_in_db(L)
            isLevelSet = True
            
        if not isBgndSet:
            if frameCounter < 10:
                background += energy_per_sample_in_db(L)
            else:
                background += energy_per_sample_in_db(L)
                background /= 10
                isBgndSet = True
                
        frameCounter += 1
        if JUST_SILENCE:
            if frameCounter < SILENCE_SPAN:
                LRtn.extend(L)
            else:
                break
        
        #print "frame number is %d\n" % (frameCounter)            
        if (endpt == "adaptive" and isBgndSet and isLevelSet) or (endpt == "energy") or endpt == "vanilla":
            silent = is_silent(L, endpt)
            #print silent, num_silent, L[:10]
            
            if not prevIsSilence and silent:
                num_silent = 0
            
            if silent and snd_started:
                num_silent += 1
           

            elif not silent and not snd_started:
                snd_started = True
                
                

            if snd_started and num_silent >= TIME_THRESHOLD:
	    	done = True
            	break

            prevIsSilence = silent
            #if snd_started and JUST_SILENCE is False:
            LRtn.extend(L)
                
                
    sample_width = p.get_sample_size(FORMAT)
    stream.stop_stream()
    stream.close()
    #p.terminate()

    LRtn = normalize(LRtn)
    #LRtn = trim(LRtn)
    #LRtn = add_silence(LRtn, 0.5)
    return sample_width, LRtn

def record_to_file(pyaudioObj, path, endpt):
    "Records from the microphone and outputs the resulting data to `path`"
    sample_width, data = record(pyaudioObj, endpt)

    data = pack('<' + ('h'*len(data)), *data)

    wf = wave.open(path, 'wb')
    wf.setnchannels(1)
    wf.setsampwidth(sample_width)
    wf.setframerate(RATE)
    wf.writeframes(data)
    wf.close()
    
    isLogSpec = True
    #FeatureExt.PlotAGrid(path, isLogSpec)
    isLogSpec = False
    #FeatureExt.PlotAGrid(path, isLogSpec)
    output_file, C_out = FeatureExt.GetEverything(path, 40)

    np.savetxt(open(output_file, 'w'), C_out)
    
    
    # FeatureExt.PlotTheFeatures(path, isLogSpec, 40)
    # FeatureExt.PlotTheFeatures(path, isLogSpec, 25)
    # FeatureExt.PlotTheFeatures(path, isLogSpec, 16)
    # isLogSpec = False
    # FeatureExt.PlotTheFeatures(path, isLogSpec, 40)
    # FeatureExt.PlotTheFeatures(path, isLogSpec, 25)
    # FeatureExt.PlotTheFeatures(path, isLogSpec, 16)
    #print FeatureExt.GetReadableMFCC(path)
    #print FeatureExt.GetReadableLogMFCC(path)
    return output_file


    
def signal_handler(signal, frame):
        print 'You pressed Ctrl+C!'
        sys.exit(0)



if __name__ == '__main__':
	#old_settings = termios.tcgetattr(sys.stdin)	
        #filename = sys.argv[1]
        parse = argparse.ArgumentParser()
        parse.add_argument("-a", action="store_true", dest="autostart", default=False, help="Detect SpeechOnset")
        parse.add_argument("-b", action="store", dest="beamwidth", default=2)
        parse.add_argument("-c", action="store_true", dest="decode_cont", default=False)
        parse.add_argument("-d", action="store", dest="dirname", help="store directory")
        parse.add_argument("-g", action="store", dest="fsgfile")
        parse.add_argument("-i", action="store", dest="insert_factor", default=45)
        parse.add_argument("-k", action="store", dest="modeldir", help="model directory")
        parse.add_argument("-l", action="store_true", dest="backpointer_flag", default=False, help="use backpointer or not")
        parse.add_argument("-p", action="store", dest="prefix", help="prefix to the file")
        parse.add_argument("-q", action="store", dest="q_length", help="queue length", default=8000)
        parse.add_argument("-r", action="store_true", dest="repeat", default=False)
        parse.add_argument("-s", action="store", dest="silence_span", default=0)
        parse.add_argument("-t", action="store_true", dest="decode_discrete", default=False)
        


   
        results = parse.parse_args()
        
        dirname = results.dirname
        prefix =  results.prefix
        repeat = results.repeat
        auto_start = results.autostart
        decode_discrete = results.decode_discrete
        decode_cont = results.decode_cont
        beam_width = int(results.beamwidth)
        q_length = int(results.q_length)
        insert_factor = float(results.insert_factor)

        backpointer_flag = results.backpointer_flag
        silence_span = int(results.silence_span)
        
        if silence_span != 0:
            #global JUST_SILENCE, SILENCE_SPAN
            JUST_SILENCE = True
            SILENCE_SPAN = silence_span
            
        

        discrete_hmm = {}
        big_hmm_model = None

        if decode_discrete:
            model_dir = results.modeldir
            discrete_hmm = discretehmm.init_models(model_dir)
        elif decode_cont:
            model_dir = results.modeldir
            fsg_file = results.fsgfile
            big_hmm_model = conthmm.init_models(fsg_file, model_dir, insert_factor)

        signal.signal(signal.SIGINT, signal_handler)

        if not os.path.exists(dirname):
            os.makedirs(dirname)

        endpt = "adaptive"
	# for i in range(10):
	# 	name = ''.join([filename, str(i),'.wav'])
	# 	print name
	# 	record(name)
        filecounter = 0
        pyaudioObj = pyaudio.PyAudio()
        while True:
            
            if auto_start is False:
                print "Hit enter to start recording the next utterance or Ctrl+C to break"
            while auto_start is False:
                if isData():
                    c = sys.stdin.read(1)
                    break
            filename = os.path.join(dirname, ''.join([prefix, '-', str(filecounter), '.wav']))    
            output_file = record_to_file(pyaudioObj, filename, endpt)
            #if repeat:
            #    play_wave_file(pyaudioObj, filename)

            print "recorded the %d utterance to %s\n" %(filecounter, filename)

            filecounter += 1

            if decode_discrete:
                model_file = hmm.live_decode(output_file, 
                                             discrete_hmm)
                print "%s" %(model_file) 

            elif decode_cont:
                result = conthmm.live_decode(output_file, 
                                             backpointer_flag, 
                                             big_hmm_model, 
                                             beam_width,
                                             q_length)
                print result
