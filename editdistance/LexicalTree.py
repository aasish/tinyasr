#!/usr/bin/env python
import time
import argparse
from string import strip
from clint.textui import indent, puts
#NodeCount = 0
#WordCount = 0
class TrieNode:
    def __init__(self):
        self.word = None
        self.children = {}

        #global NodeCount
        #NodeCount += 1

    def insert( self, word ):
        node = self
        actual_word = word[1:]
        word = list(word)
        word[len(word) - 1] += '$'
        for letter in word:
            if letter not in node.children: 
                
                node.children[letter] = TrieNode()
            
            #if node.children[letter].word:
                #print "warning there is a word here %s" %(letter)
            node = node.children[letter]
        
        node.word = actual_word

class LexicalTree:
    def __init__(self):
        self.word = None
        self.children = {}
        
    def insert (self, word):
        node = self
        actual_word = word
        word = list(word)
        word_len = len(word)
        word[word_len - 1] += '$'
        
        for i in range(word_len):
            node = self
            letter = word[i]
            if letter not in node.children: 
                node.children[letter] = LexicalTree()
            
            #if node.children[letter].word:
                #print "warning there is a word here %s" %(letter)
            node = node.children[letter]
        
        node.word = actual_word
        
# read dictionary file into a trie
def load_dictionary(words):
    trie = TrieNode()
    for word in words:
        word = '*' + word
        #WordCount += 1
        trie.insert( word )

    
    return trie

def traverse(trie, char, ind):
    print_str = '%s' %(char)
    #print_str = '%s %s' %(char, trie.children.keys())
    with indent(ind):  puts(print_str)
    for letter in trie.children:
        #if trie.children[letter].word:
        #    print "don't add here"
        traverse(trie.children[letter], letter, ind + 1) 
    #print "\n"
    

        
def search( word, trie, maxCost ):

    # build first row
    currentRow = range( len(word) + 1 )

    results = []

    # recursively search each branch of the trie
    for letter in trie.children:
        searchRecursive( trie.children[letter], letter, word, currentRow, 
            results, maxCost )

    return results

def searchRecursive( node, letter, word, previousRow, results, maxCost ):

    columns = len( word ) + 1
    currentRow = [ previousRow[0] + 1 ]

    # Build one row for the letter, with a column for each letter in the target
    # word, plus one for the empty string at column 0
    for column in xrange( 1, columns ):

        insertCost = currentRow[column - 1] + 1
        deleteCost = previousRow[column] + 1

        if word[column - 1] != letter:
            replaceCost = previousRow[ column - 1 ] + 1
        else:                
            replaceCost = previousRow[ column - 1 ]

        currentRow.append( min( insertCost, deleteCost, replaceCost ) )

    # if the last entry in the row indicates the optimal cost is less than the
    # maximum cost, and there is a word in this trie node, then add it.
    if currentRow[-1] <= maxCost and node.word != None:
        results.append( (node.word, currentRow[-1] ) )

    # if any entries in the row are less than the maximum cost, then 
    # recursively search each branch of the trie
    if min( currentRow ) <= maxCost:
        for letter in node.children:
            searchRecursive( node.children[letter], letter, word, currentRow, 
                results, maxCost )


def main():	
    parse = argparse.ArgumentParser()
    #parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
    parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
    #parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
    #parse.add_argument('-f','--dict-file', action="store", dest="dict_file", default='dict.txt', help='dict file')
	res = parse.parse_args()
	dictionary = open(res.user_dict).readlines()
	dictionary = map(strip,dictionary)
	print len(dictionary)	
def main1():
    parse = argparse.ArgumentParser()
    parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
    parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
    parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
    parse.add_argument('-f','--dict-file', action="store", dest="dict_file", default='dict.txt', help='dict file')
    

    res = parse.parse_args()
    dictionary = []
    word = res.target_word
    if res.usr_dict:
                print "using user specific dictionary: ideal for trellis"	
		dictionary = res.usr_dict
    
    else:
        dictionary = open(res.dict_file).readlines()
        dictionary = map(strip, dictionary)
        
    #print len(dictionary)
        
    trie = load_dictionary(dictionary)
    traverse(trie, "*", 1)
    
    target_word = res.target_word 
    target_word = '*' + target_word
    target_word = list(target_word)
    
    target_word[len(target_word)-1] += '$'
    print target_word
    results = search(target_word,  trie, int(res.beam_width))
    
    print results
    
if __name__ == "__main__":
    main()

    
