#!/usr/bin/python
import math
from string import strip, lower
import time
import operator
from clint.textui import colored
import argparse
import sys

def load_words(filename):
	lines = open(filename).readlines()
	lines = map(strip, lines)
	lines = map(lower, lines)
	words = {}
	for line in lines:
		if ' ' in line:
			sentence = line.split()
			for word in sentence:
				if word not in words:
					words[word] = 1
		else:
			if line not in words:
				words[line] = 1
	return sorted(words.iterkeys())

def filter_by_start(start_phrase, dictionary):
	dictionary = [word for word in dictionary if word.startswith(start_phrase)]
	return dictionary

def filter_by_length(threshold_len, dictionary):
	dictionary = [word for word in dictionary if len(word) < threshold_len]
	return dictionary

def filter_by_all(start_phrase, threshold_len, dictionary):
	dictionary = [word for word in dictionary if (len(word) < threshold_len and word.startswith(start_phrase))]
	return dictionary

def concatenate_words (dictionary):
	ref = '';
	for i in range(0,len(dictionary)):
		ref += '*' + dictionary[i] # + sign is the word boundary, * sign is the dummy letter
	#print ref
	return ref

def beamSearchResult(s1,s2,cost,N):
	# return the beamSearchResult from cost matrix
	end_point = []
	words = s2.split('*')
	word_costs = {}
	for j in range(1,len(s2)):
		if s2[j] == '*':
			end_point.append(j-1)
	end_point.append(len(s2)-1)
#	print end_point
	least_cost = 9999
	best_word_index = -1
	i = len(s1) - 1
	for e in range(len(end_point)):
		j = end_point[e]
		#if cost[i][j] < least_cost:
		#	least_cost = cost[i][j]
		#	best_word_index = e
	#	print 'cost[%d][%d] = %d' %(i,j,cost[i][j])
		if cost[i][j] < least_cost:
			word_costs[words[e + 1]] = cost[i][j]
	#print 'least_cost is %d at %d' %(least_cost,best_word_index)
#	print word_costs	
	#print words[best_word_index + 1]	
	sorted_word_costs = sorted(word_costs.iteritems(), key=operator.itemgetter(1))
#	print sorted_word_costs
	if N <= len(word_costs):
		return sorted_word_costs[:N]
	else:
		return sorted_word_costs

#def PrintPruneTrellisCost(path, s1, s2, blacklist, cost):
#	#print path 
#	end_point = []
#	for j in range(1,len(s2)):
#		if s2[j] == '*':
#			end_point.append(j-1)
#	end_point.append(len(s2)-1)
#	#print end_point
#	my_path = []
#	for e in range(len(end_point)):
#		print '-----------'
#		print end_point[e]
#		print path[len(s1)-1,end_point[e]]
#		my_path_t = path[len(s1)-1,end_point[e]]
#		#print 'my_path_t -> ', my_path_t
#		#print 'cost -> ', cost[len(s1)-1][end_point[e]]
#		for t in range(len(my_path_t)):
#			my_path.append(my_path_t[t])
#	print 'my_path -> ', my_path
#	
#	for i in range(len(s1)):
#		tmp_str = []
#		for j in range(len(s2)):
#			cost_t = cost[i][j]
#			if cost_t >= 9999:
#				cost_t = -1
#			if (i, j) in my_path:
#				tmp_str.append("%s,%s:%d" %(i, j, cost_t))
#			elif (i,j) in blacklist:
#				tmp_str.append("+,+:%d" %(cost_t))
#			else:
#				tmp_str.append("X,X:%d" %(cost_t))
#		print ' '.join(tmp_str)

def PrintPruneTrellisCost(path, s1, s2, blacklist, cost):
        #print path 
        end_point = []
        for j in range(1,len(s2)):
                if s2[j] == '*':
                        end_point.append(j-1)
        end_point.append(len(s2)-1)
        #print end_point
        my_path = []
        for e in range(len(end_point)):
#               print '-----------'
#               print end_point[e]
#               print path[len(s1)-1,end_point[e]]
                my_path_t = path[len(s1)-1,end_point[e]]
                #print 'my_path_t -> ', my_path_t
                #print 'cost -> ', cost[len(s1)-1][end_point[e]]
                for t in range(len(my_path_t)):
                        my_path.append(my_path_t[t])
        #print 'my_path -> ', my_path

        for i in range(len(s1)):
                tmp_str = ""
                for j in range(len(s2)):
                        cost_t = cost[i][j]
                        if (i,j) in blacklist:
                                #tmp_str.append("+,+:%d" %(cost_t))
                                tmp_str += " "+colored.black("%02d,%02d:%02d" %(i,j,cost_t))
                        elif (i, j) in my_path:
                                #tmp_str.append("%s,%s:%d" %(i, j, cost_t))
                                tmp_str += " "+colored.red("%02d,%02d:%02d" %(i, j, cost_t))
                        else:
                                #tmp_str.append("X,X:%d" %(cost_t))
                                tmp_str += " "+colored.blue("%02d,%02d:%02d" %(i,j,cost_t))
                #print ' '.join(tmp_str)
                print tmp_str
"""
def beamSearch(s1, dictionary, threshold, beam):
	m = []
	black_list = {}
	path = {}
	s1 = '*' + s1
	s2 = concatenate_words(dictionary)
	if s2 == '':
		# templates are deleted by the preprocessing
		return ''
	print 'input is %s' %(s1)
	print 'templates are %s' %(s2)
	boundary_mark = 9999; # after searching, the score of boundary nodes will be higher than boundary_mark
	label = "without beam"
	if beam:
		label = "with beam"

	
	for i in range(0,len(s1)):
		m.append([])
		for j in range(0,len(s2)):
			m[i].append([])
			m[i][j] = 0
			if s2[j] == '+':
				m[i][j] = boundary_mark
			elif s2[i] == '*':
				m[i][j] = i
			path[(i,j)] = [(i,j)]
		
	for i in range(1,len(s1)):
		m[i][0] = boundary_mark
		m[i][1] = i
	count = 0;
	for j in range(0,len(s2)):	
		if s2[j] == '+':
			m[0][j] = boundary_mark
			count = 0
		else:
	                m[0][j] = count 
        	        count += 1

#	print m[0]
	#print m
	# search starts here
	for i in range(1,len(s1)):
		for j in range(2, len(s2)):
			if s2[j] == '*':
				continue
			cost = 1;
			# two units match
			if s1[i] == s2[j]:
				cost = 0;
			tmp_cost = []
			tmp_path = []

			# calculate the score after transition from previous three directions
			if s2[j] == '+':
				#print 'hit the boundary'
				path[(i,j)] = []
				m[i][j] = boundary_mark
				continue
			#if (i-1,j-1) in black_list and (i-1,j) in black_list and (i,j-1) in black_list:
			#	tmp_cost.append(9999)
			#else:
			if (i-1,j-1) not in black_list:
				tmp_cost.append(m[i-1][j-1] + cost + m[i][j])
				tmp_path.append((i-1,j-1))
			if (i-1,j) not in black_list:
				tmp_cost.append(m[i-1][j] + 1 + m[i][j])
				tmp_path.append((i-1,j))
			if (i,j-1) not in black_list:
				tmp_cost.append(m[i][j-1] + 1 + m[i][j])
				tmp_path.append((i,j-1))
			
			m[i][j],prev_node,subpath = min([(tmp_cost[x], x, tmp_path[x]) for x in range(len(tmp_cost))])
			
			if tmp_path:
				path[(i,j)] = path[subpath] + [(i,j)]
			else:
				path[(i,j)] = []
#			if i is 0 and j is 7:
#				print 'hi there!'
#			 	print s1[i],s2[j]
#			 	print tmp_cost
#			 	print tmp_path
#			 	print 'path -> ', path[(i,j)]
		(best_score, best_row, best_col) = min([(m[i][j], i, j) for j in range(1, len(s2))])
		# setting the tmp_threshold
		tmp_threshold = threshold
		if beam:
			tmp_threshold += best_score
		# updating black_list
		for j in range(0,len(s2)):
			if m[i][j] >= tmp_threshold:
				black_list[(i,j)]=1

	#print "EDIT DISTANCE USING PRUNED VERSION ON MULTIPLE TEMPLATES" ,label
	
	PrintPruneTrellisCost(path, s1, s2, black_list,m)
	#print "hello"
	#PrintPruneTrellisCost(path[5,10],s1,s2,black_list,m)
	best_match = beamSearchResult(s1,s2,m,4)
	return best_match
"""
def beamSearch(s1, dictionary, threshold, beam, Nbest, isTrellis):
	m = []
        black_list = {}
        path = {}
        s1 = '*' + s1
        s2 = concatenate_words(dictionary)
        if s2 == '':
                # templates are deleted by the preprocessing
                return ''
#	print 'input is %s' %(s1)
#	print 'templates are %s' %(s2)
        boundary_mark = 9999; # after searching, the score of boundary nodes will be higher than boundary_mark
        label = "without beam"
        if beam:
                label = "with beam"
	
	for i in range(0,len(s1)):
		m.append([])
		for j in range(0,len(s2)):
			m[i].append([])
			m[i][j] = 0
			if s2[j] == '*':
				m[i][j] = i
			path[(i,j)] = [(i,j)]
			
	count = 0
	for j in range(0,len(s2)):
		if s2[j] == '*':
			count = 0
		m[0][j] = count
		count += 1
	for i in range(1, len(s1)):
		for j in range(1,len(s2)):
			if s2[j] == '*':
				continue
			cost = 1
			if (s1[i] == s2[j]):
				cost = 0
			tmp_cost = []
			tmp_path = []
			
			if (i-1,j-1) not in black_list:
                                tmp_cost.append(m[i-1][j-1] + cost + m[i][j])
                                tmp_path.append((i-1,j-1))
                        if (i-1,j) not in black_list:
                                tmp_cost.append(m[i-1][j] + 1 + m[i][j])
                                tmp_path.append((i-1,j))
                        if (i,j-1) not in black_list:
                                tmp_cost.append(m[i][j-1] + 1 + m[i][j])
                                tmp_path.append((i,j-1))
                        
                        m[i][j],prev_node,subpath = min([(tmp_cost[x], x, tmp_path[x]) for x in range(len(tmp_cost))])
			path[(i,j)] = path[subpath] + [(i,j)]

		(best_score, best_row, best_col) = min([(m[i][j], i, j) for j in range(0, len(s2))])
                # setting the tmp_threshold
                tmp_threshold = threshold
                if beam:
                        tmp_threshold += best_score
                # updating black_list
                for j in range(0,len(s2)):
                        if m[i][j] >= tmp_threshold:
                                black_list[(i,j)]=1

	if isTrellis:
		PrintPruneTrellisCost(path, s1, s2, black_list, m)

        best_match = beamSearchResult(s1,s2,m, Nbest)
	#print best_match
        return best_match
		

#dictionary = ['disappeared','xinhua']
#beamSearch('xdiseaappeaed',dictionary,4,True)


def batch_correct(words, dictionary, output_fn, beam_width, isBeam, filterType, NBest, isTrellis):
	f = open(output_fn, 'w')
	begin_time = time.time()
	#print "filter type is %s " %(filterType)
	print "total words in the list %d" %(len(words))

	for word in words:

		if word not in dictionary and len(word) > 1:
				
			tmp_dictionary = []
			if filterType == "fc":
				tmp_dictionary = filter_by_start(word[0], dictionary)
			elif filterType == "len":								
				tmp_dictionary = filter_by_length(len(word) + 4, dictionary)
			elif filterType == "all":
				tmp_dictionary = filter_by_all(word[0], len(word) + 4, dictionary)
			start_time = time.time()
			#print "looking at %d out of %d words after filtering" %(len(tmp_dictionary), len(dictionary))
			best_words = beamSearch(word, tmp_dictionary, beam_width, isBeam, NBest, isTrellis)
			word_list = ""
			for w,cost in best_words:
				word_list += w + ":" +str(cost) + "\t"
				
				
				output_line = word + '\t' + word_list + "time_taken:"+str(time.time()-start_time) + '\n'
				print output_line.strip('\n')
				f.write(output_line)
			
	f.close()
	print time.time() - begin_time, "seconds lapsed in the search"


def main():

	dictionary = load_words('dict.txt')


	parse = argparse.ArgumentParser()
	parse.add_argument('-i',  '--interactive', action="store_true", dest="interactive", default=False, help='interactive input default=False')
	parse.add_argument('-f', '--input', action="store", dest="filename", help='input_type eg. -f input_filename')
	parse.add_argument('-o', '--output', action="store", dest="output_file", default='out.txt', help='output_file eg. -o output.txt')
	parse.add_argument('-b','--beam-width', action="store", dest="beam_width", default=2, help='set beam width default: -b 2')
	parse.add_argument('-x', '--fixed', action="store_false", dest="rel_beam", default=True, help="turns off the relative_beam, default=True")
	parse.add_argument('-p', '--preprocess', action="store", dest="preprocess", default='all', help="-p [len|all|fc], default=all")
	parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
	parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
	parse.add_argument('-t', '--trellis', action="store_true", dest="istrellis", default=False, help="print trellis or not: default=False")
	parse.add_argument('-n', '--nbest', action="store", dest="nbest", default=4, help="nbest list, default=4")
	res = parse.parse_args()

	beam_width = int(res.beam_width)
	NBest = int(res.nbest)
	isBeam = res.rel_beam
	filterType = res.preprocess
	isTrellis = res.istrellis
	print "using beam_width %d, filter_type:%s and isBeam = %s" %(beam_width, filterType, isBeam)
	if res.interactive:
		while True:
			try:
				
				sentence = raw_input(">enter your sentence\n>")
				begin_time = time.time()
				words = sentence.split(' ')

				candidates = {} #print candidates at the end
				print ">",
				for word in words:
					if word not in dictionary and len(word) > 1:
						
						tmp_dictionary = []
						if filterType == "fc":
							tmp_dictionary = filter_by_start(word[0], dictionary)
						elif filterType == "len":
							tmp_dictionary = filter_by_length(len(word) + 4, dictionary)
						else:
							tmp_dictionary = filter_by_all(word[0], len(word) + 4, dictionary)
						start_time = time.time()
							
						best_words = beamSearch(word, tmp_dictionary, beam_width, isBeam, NBest, isTrellis)
						word_list = ""
						best_candidate,cost = best_words[0]
						candidates[word] = best_words[1:]
						print colored.red(best_candidate),
					else:
						print colored.green(word),

				print '.' #for new line after the sentence
				print time.time() - begin_time, "secs lapsed"
				print ">other candidates not considered %s" %(candidates)
								
			except KeyboardInterrupt:
				print "Bye"
				sys.exit()
    
	elif res.filename:
		print 'both input and output files are mentioned'
		words = load_words(res.filename)	
		output_fn = res.output_file

		batch_correct(words, dictionary, output_fn, beam_width, isBeam, filterType, NBest, isTrellis)

	elif res.usr_dict and res.target_word:
		print "using user specific dictionary: ideal for trellis"
		word = res.target_word
		dictionary = res.usr_dict

		best_words = beamSearch(word, dictionary, beam_width, isBeam, NBest, isTrellis)

		

#dictionary = filter_by_all(word[0], len(word) + 1, dictionary)

if __name__ == "__main__":
    main()
