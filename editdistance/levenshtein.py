#!/usr/bin/python
import math
from clint.textui import colored

def PrintTrellisCost(path, s1, s2, cost):
   #print path 
   for i in range(len(s1)):
        tmp_str = ""
        for j in range(len(s2)):
            if (i, j) in path:
                #tmp_str.append("%s,%s" %(i, j))
                #tmp_str.append(colored.red("\033[1m%s,%s\033[0m:%d" %(i,j,cost[i][j])))
               tmp_str += " " + colored.red("\033[1m%s,%s\033[0m:%d" %(i,j,cost[i][j]))
            else:
               tmp_str+= " " + colored.blue("X,X:%d" %(cost[i][j])) 
                #tmp_str.append(colored.blue("X,X:%d" %(cost[i][j])))
        print tmp_str
 

def PrintTrellis(path, s1, s2):
   #print path 
   for i in range(len(s1)):
        tmp_str = []
        for j in range(len(s2)):
            if (i, j) in path:
                tmp_str.append("%s,%s" %(i, j))
            else:
                tmp_str.append("X,X")
        print ' '.join(tmp_str)
 
def PrintPruneTrellisCost(path, s1, s2,blacklist,cost):
   #print path 
   for i in range(len(s1)):
        #tmp_str = []
        tmp_str = ""
        for j in range(len(s2)):
            if (i, j) in path:
                #tmp_str.append(colored.red("%s,%s:%d" %(i, j, cost[i][j])))
               tmp_str += " "+colored.red("%s,%s:%d" %(i, j, cost[i][j]))
            elif (i,j) in blacklist:
                #tmp_str.append(colored.black("+,+:%d" %(cost[i][j])))
               tmp_str += " "+colored.black("+,+:%d" %(cost[i][j]))
            else:
                #tmp_str.append(colored.blue("X,X:%d" %(cost[i][j])))
               tmp_str += " "+colored.blue("X,X:%d" %(cost[i][j]))
        print tmp_str
  
 
def PrintPruneTrellis(path, s1, s2,blacklist):
   #print path 
   for i in range(len(s1)):
        tmp_str = []
        
        for j in range(len(s2)):
            if (i, j) in path:
                tmp_str.append("%s,%s" %(i, j))
            elif (i,j) in blacklist:
                tmp_str.append("+,+")
            else:
                tmp_str.append("X,X")
        print ' '.join(tmp_str)
      
def VanillaEditDistance (s1, s2):
    m = []
    V = [{}]
    #path =[(0, 0)]
    path = {}
    s1 = "*" + s1
    s2 = "*" + s2
    #initialization
    for i in range(0,len(s1)):
        m.append([])
        for j in range(0,len(s2)):
            m[i].append([])
            m[i][j] = 0
            path[(i,j)] = [(i,j)]

    for i in range(1, len(s1)):
        m[i][0] = i

    for j in range(1, len(s2)):
        m[0][j] = j

    m[0][0] = 0;
    
    #DP Here                
    for i in range(1, len(s1)):
        
        for j in range(1, len(s2)):
                cost = 1

                if s1[i] == s2[j]:
                    cost = 0
                
                tmp_cost = []
                tmp_path = []
                

                tmp_cost.append(m[i-1][j-1] + cost)
                tmp_path.append((i-1, j-1))
                   
                tmp_cost.append(m[i-1][j] + 1)
                tmp_path.append((i-1, j))


                tmp_cost.append(m[i][j-1] + 1)
                tmp_path.append((i, j-1))
                

                m[i][j], subpath = min([(tmp_cost[x], tmp_path[x]) for x in range(len(tmp_cost))])

                #m[i][j] = min(m[i-1][j-1] + cost, m[i-1][j] + 1, m[i][j-1] + 1)
                path[(i,j)] = path[subpath] + [(i,j)]
        

        #(best_score, best_row, best_col) = min([(m[i][j], i, j) for j in range(1, len(s2))])        
        #path.append((best_row, best_col))
    
    print "VANILLA EDIT DISTANCE"
    PrintTrellisCost(path[len(s1)-1,len(s2)-1], s1, s2, m)
    #print m[len(s1)-1][len(s2)-1], path[(len(s1)-1,len(s2)-1)]
    return m[len(s1)-1][len(s2)-1], path[(len(s1)-1,len(s2)-1)]


def PruneEditDistance (s1, s2, threshold, beam):
    m = []
    black_list = []
    #path = [(0,0)]
    path = {}
    s1 = "*" + s1
    s2 = "*" + s2
    label = "without beam"
    if beam:
        label = "with beam"

    """
    
    """
    for i in range(0,len(s1)):
        m.append([])
        for j in range(0,len(s2)):
            m[i].append([])
            m[i][j] = 0
            path[(i,j)] = [(i,j)]

    for i in range(1, len(s1)):
        m[i][0] = i
    for j in range(1, len(s2)):
        m[0][j] = j
   
    
                    
    for i in range(1, len(s1)):
        for j in range(1, len(s2)):
                cost = 1
                if s1[i] == s2[j]:
                    cost = 0
                    
                tmp_cost = []
                tmp_path = []
                
                
 
                if (i-1, j-1) not in black_list:
                    tmp_cost.append(m[i-1][j-1] + cost)
                    tmp_path.append((i-1, j-1))
                    
                if (i-1, j) not in black_list:
                    tmp_cost.append(m[i-1][j] + 1)
                    tmp_path.append((i-1, j))

                if (i, j-1) not in black_list:
                    tmp_cost.append(m[i][j-1] + 1)
                    tmp_path.append((i, j-1))
                    
                m[i][j], prev_node, subpath = min([(tmp_cost[x], x, tmp_path[x]) for x in range(len(tmp_cost))])
                #path.append(tmp_path[x])                         
                path[(i,j)] = path[subpath] + [(i,j)]

        (best_score, best_row, best_col) = min([(m[i][j], i, j) for j in range(1, len(s2))])        
        
        #print best_score, best_row, best_col
        #path.append((best_row, best_col))
        
        tmp_threshold = threshold
        if beam:
            tmp_threshold += best_score

        for j in range(1, len(s2)):
            if m[i][j] >= tmp_threshold:
                black_list.append((i, j))
                #print "removing", i, j, " because of high cost", m[i][j], ">", tmp_threshold 
        #print "black list is ", black_list
    print "EDIT DISTANCE USING PRUNED VERSION",label
    #print path
    #print path[len(s1)-1,len(s2)-1]
    PrintPruneTrellisCost(path[len(s1)-1,len(s2)-1], s1, s2, black_list,m)
    return m[len(s1)-1][len(s2)-1], path


s1 = "fpoor"
s2 = "for"
#s1 = "hello"
#s2 = "helo"
#s1 = 'fpoxr'
#s2 = 'for'
isBeam = False


edit_distance, path = VanillaEditDistance(s1, s2)
#edit_distance, path = PruneEditDistance(s1, s2, 3, isBeam)
isBeam = True
edit_distance, path = PruneEditDistance(s1, s2, 2, isBeam)

