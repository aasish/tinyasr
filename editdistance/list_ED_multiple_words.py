#!/usr/bin/python
import math
from string import strip, lower
import time
import operator
from clint.textui import colored

def load_words(filename):
	lines = open(filename).readlines()
	lines = map(strip, lines)
	lines = map(lower, lines)
	return lines

def filter_by_start(start_phrase, dictionary):
	dictionary = [word for word in dictionary if word.startswith(start_phrase)]
	return dictionary

def filter_by_length(threshold_len, dictionary):
	dictionary = [word for word in dictionary if len(word) < threshold_len]
	return dictionary

def filter_by_all(start_phrase, threshold_len, dictionary):
	dictionary = [word for word in dictionary if (len(word) < threshold_len and word.startswith(start_phrase))]
	return dictionary

def concatenate_words (dictionary):
	ref = '';
	for i in range(0,len(dictionary)):
		ref += '*' + dictionary[i] # + sign is the word boundary, * sign is the dummy letter
	#print ref
	return ref

def concatenate_words_in_list (dictionary):
	ref = []
	for i in range(0,len(dictionary)):
		tmp_dict_word = dictionary[i];
		tmp_dict_word1 = normalizeWord(tmp_dict_word);
		tmp_dict_word_list = tmp_dict_word1.split(' ');
		dictionary[i] = tmp_dict_word_list;
		
		ref.append('*');
		ref.extend(dictionary[i]) # + sign is the word boundary, * sign is the dummy letter
		#print ref
	return ref

def normalizeWord (word):
	word = word.replace("'", "");
	return word

def changeStringToList (word):
	word = word.split(' ')
	return word

def beamSearchResult(s1,s2,cost,N):
	# return the beamSearchResult from cost matrix
	end_point = []
	words = s2.split('*')
	word_costs = {}
	for j in range(1,len(s2)):
		if s2[j] == '*':
			end_point.append(j-1)
	end_point.append(len(s2)-1)
#	print end_point
	least_cost = 9999
	best_word_index = -1
	i = len(s1) - 1
	for e in range(len(end_point)):
		j = end_point[e]
		#if cost[i][j] < least_cost:
		#	least_cost = cost[i][j]
		#	best_word_index = e
	#	print 'cost[%d][%d] = %d' %(i,j,cost[i][j])
		if cost[i][j] < least_cost:
			word_costs[words[e + 1]] = cost[i][j]
	#print 'least_cost is %d at %d' %(least_cost,best_word_index)
#	print word_costs	
	#print words[best_word_index + 1]	
	sorted_word_costs = sorted(word_costs.iteritems(), key=operator.itemgetter(1))
#	print sorted_word_costs
	if N < len(word_costs):
		return sorted_word_costs[:N]
	else:
		return sorted_word_costs

def beamSearchResultList(s1,s2,cost,N):
	# return the beamSearchResult from cost matrix
	end_point = []
	tmp_s2 = str(s2);
	words = tmp_s2.split('*')
	word_costs = {}
	for j in range(1,len(s2)):
		if s2[j] == '*':
			end_point.append(j-1)
	end_point.append(len(s2)-1)
#	print end_point
	least_cost = 9999
	best_word_index = -1
	i = len(s1) - 1
	for e in range(len(end_point)):
		j = end_point[e]
		#if cost[i][j] < least_cost:
		#	least_cost = cost[i][j]
		#	best_word_index = e
	#	print 'cost[%d][%d] = %d' %(i,j,cost[i][j])
		if cost[i][j] < least_cost:
			word_costs[words[e + 1]] = cost[i][j]
	#print 'least_cost is %d at %d' %(least_cost,best_word_index)
#	print word_costs	
	#print words[best_word_index + 1]	
	sorted_word_costs = sorted(word_costs.iteritems(), key=operator.itemgetter(1))
#	print sorted_word_costs
	if N < len(word_costs):
		return sorted_word_costs[:N]
	else:
		return sorted_word_costs



def PrintPruneTrellisCost(path, s1, s2, blacklist, cost):
        #print path 
        end_point = []
        for j in range(1,len(s2)):
                if s2[j] == '*':
                        end_point.append(j-1)
        end_point.append(len(s2)-1)
        #print end_point
        my_path = []
        for e in range(len(end_point)):
#               print '-----------'
#               print end_point[e]
                my_path_t = path[len(s1)-1,end_point[e]]
                #print 'my_path_t -> ', my_path_t
                for t in range(len(my_path_t)):
                        my_path.append(my_path_t[t])
        #print 'my_path -> ', my_path

        for i in range(len(s1)):
                tmp_str = ""
                for j in range(len(s2)):
                        cost_t = cost[i][j]
                        if (i,j) in blacklist:
                                #tmp_str.append("+,+:%d" %(cost_t))
                                tmp_str += " "+colored.black("%02d,%02d:%02d" %(i,j,cost_t))
                        elif (i, j) in my_path:
                                #tmp_str.append("%s,%s:%d" %(i, j, cost_t))
                                tmp_str += " "+colored.red("%02d,%02d:%02d" %(i, j, cost_t))
                        else:
                                #tmp_str.append("X,X:%d" %(cost_t))
                                tmp_str += " "+colored.blue("%02d,%02d:%02d" %(i,j,cost_t))
                #print ' '.join(tmp_str)
                print tmp_str

def beamSearch(s1, dictionary, threshold, beam):
	m = []
        black_list = {}
        path = {}
        s1.insert(0, '*');
        s2 = concatenate_words_in_list(dictionary)
	print ' '.join(s1)
	print ' '.join(s2)
	# TODO: Check if list is empty
        if not s2:
                # templates are deleted by the preprocessing
                return s2
#	print 'input is %s' %(s1)
#	print 'templates are %s' %(s2)
        boundary_mark = 9999; # after searching, the score of boundary nodes will be higher than boundary_mark
        label = "without beam"
        if beam:
                label = "with beam"
	
	for i in range(0,len(s1)):
		m.append([])
		for j in range(0,len(s2)):
			m[i].append([])
			m[i][j] = 0
			if s2[j] == '*':
				m[i][j] = i
			path[(i,j)] = [(i,j)]
			
	count = 0
	for j in range(0,len(s2)):
		if s2[j] == '*':
			count = 0
		m[0][j] = count
		count += 1
	for i in range(1, len(s1)):
		for j in range(1,len(s2)):
			if s2[j] == '*':
				continue
			cost = 1
			if (s1[i] == s2[j]):
				cost = 0
			tmp_cost = []
			tmp_path = []
			
			if (i-1,j-1) not in black_list:
                                tmp_cost.append(m[i-1][j-1] + cost + m[i][j])
                                tmp_path.append((i-1,j-1))
                        if (i-1,j) not in black_list:
                                tmp_cost.append(m[i-1][j] + 1 + m[i][j])
                                tmp_path.append((i-1,j))
                        if (i,j-1) not in black_list:
                                tmp_cost.append(m[i][j-1] + 1 + m[i][j])
                                tmp_path.append((i,j-1))
                        
                        m[i][j],prev_node,subpath = min([(tmp_cost[x], x, tmp_path[x]) for x in range(len(tmp_cost))])
			path[(i,j)] = path[subpath] + [(i,j)]

		(best_score, best_row, best_col) = min([(m[i][j], i, j) for j in range(0, len(s2))])
                # setting the tmp_threshold
                tmp_threshold = threshold
                if beam:
                        tmp_threshold += best_score
                # updating black_list
                for j in range(0,len(s2)):
                        if m[i][j] >= tmp_threshold:
                                black_list[(i,j)]=1

	PrintPruneTrellisCost(path, s1, s2, black_list,m)
        #best_match = beamSearchResult(s1,s2,m,4)
        best_match = beamSearchResultList(s1,s2,m,4)
	#print best_match
        return best_match
		

dictionary = ['fb fe f pa ef og ph li ej','xa if ng hi uj ak']
#dictionary = ['fffpeople','xinhua']
tmp_word = ['pa', 'ef', 'og', 'ph', 'li', 'ej'];
beamSearch(tmp_word,dictionary,4,True)

def batch_correct(words, dictionary, output_fn):
	f = open(output_fn, 'w')
	begin_time = time.time()
	print "total words in the list %d" %(len(words))
	for word in words:
		if word not in dictionary and len(word) > 1:
			#print "---------------\ncorrecting %s not in dictionary" %(word)
			#tmp_dictionary = filter_by_start(word[0], dictionary)
			tmp_dictionary = filter_by_length(len(word) + 4, dictionary)
			#tmp_dictionary = filter_by_all(word[0], len(word) + 4, dictionary)
			start_time = time.time()
			#print "considering only %d out of %d words from the dictionary for search" %(len(tmp_dictionary), len(dictionary))
			word = normalizeWord(word);
			word = word.split(' ');
			best_words = beamSearch(word, tmp_dictionary, 2, True)
			word_list = ""
			for w,cost in best_words:
				word_list += w + ":" +str(cost) + "\t"

				
			output_line = word + '\t' + word_list + str(time.time()-start_time) + '\n'
			print output_line
			f.write(output_line)
			#print time.time() - start_time, "seconds lapsed in the search"
		#else:
			#print word
	f.close()
	print time.time() - begin_time, " seconds lapsed in the search"


#dictionary = load_words('dict.txt')
#words = load_words('typo_words.txt')
#batch_correct(words, dictionary, 'out.txt')
#dictionary = filter_by_all(word[0], len(word) + 1, dictionary)

