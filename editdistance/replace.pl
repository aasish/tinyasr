#!/usr/bin/perl
$exp_out_fn = shift; # exp_out_fn is the file contains the misspelling word and several candidates
$input_fn = shift; # input_fn is the file which needs to be corrected
$sed_fn = $exp_out_fn.".sed"; # sed file contains the sed commands for replacing misspellings to correct spellings
$output_fn = $input_fn."-".$exp_out_fn.".correct";
# step 1: use awk to generate
#        = awk -F'\t' '{split($2,a,":");print "s/\\<"$1"\\>/"a[1]"/g"}' ex2.out
$awk_cmd = "awk -F'".'\t'."' '{split(\$2,a,\":\");print \"s/\\\\<\"\$1\"\\\\>/\"a[1]\"/g\"}' $exp_out_fn >$sed_fn"; 
#print $awk_cmd."\n";
`$awk_cmd`;

#        = sed -f rep.2 typo_words.txt
$sed_cmd = "sed -f $sed_fn $input_fn > $output_fn";
`$sed_cmd`;


