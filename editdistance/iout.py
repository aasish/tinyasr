#!/usr/bin/python
import argparse

parse = argparse.ArgumentParser()
parse.add_argument('-i',  '--interactive', action="store_true", dest="interactive", default=False, help='interactive input')
parse.add_argument('-f', '--input', action="store", dest="filename", help='input_type eg. -f input_filename')
parse.add_argument('-o', '--output', action="store", dest="output_file", help='output_file eg. -o output.txt')
parse.add_argument('-b','--beam-width', action="store", dest="beam_width", help='set beam width eg. -b 2')
parse.add_argument('-x', '--fixed', action="store_false", dest="rel_beam", default=True, help="turns off the relative_beam")
parse.add_argument('-p', '--preprocess', action="store", dest="preprocess", help="-p [len|all|fc]")
parse.add_argument('-d', '--dict', action="append", dest="usr_dict", default=[], help="user_defined dictionary")
parse.add_argument('-w', '--word', action="store", dest="target_word", default='', help="user_defined word")
parse.add_argument('-t', '--trellis', action="store_true", dest="istrellis", default=False, help="print trellis or not: default is False")

res = parse.parse_args()



if res.interactive:
    name = raw_input(">enter your name\n")
    print name

elif res.filename and res.output_file:
  print 'both input and output files are mentioned'

elif res.beam_width:
  print 'beam_width is: %d' %(int(res.beam_width))

elif res.preprocess:
  print 'hi'
  print 'pre_process is %s' %(str(res.preprocess))
