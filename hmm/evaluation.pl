#!/usr/bin/perl
use File::Spec;
use Getopt::Long;

my $train_mode = -1;
my $decode_mode = -1;
my $ctl_fn = '';
my $result_fn = '';
my $help;
my $train_dir = '';
my $test_dir = '';
my $model_dir = '';

#getopts("htdi:c:o:r",\%options);
$option = GetOptions(
	"t" => \$train_mode,
	"h" => \$help, 
	"d" => \$decode_mode,
	"c=s" => \$ctl_fn,
	"r=s" => \$result_fn,
	"td=s" => \$train_dir,
	"md=s" => \$model_dir
);

if ($help)
{
	print "-t: train_mode -d decode_mode -c ctl_fn -r result_fn -td train_dir -md model_dir\n";
	exit(0);
}
if ($train_mode != -1)
{
	die "train_dir $train_dir does not exist" unless (-d $train_dir);
	print "training models ... \n";
	train($train_dir,$model_dir);
}
if ($decode_mode != -1)
{
	die "no ctl file" unless ($ctl_fn ne '');
	die "no result file" unless ($result_fn ne '');
	print "decoding ... \n";
	decode($model_dir,$ctl_fn,$result_fn);
}


sub train
{

	my $data_dir = shift;
	my $model_dir = shift;
	`mkdir $model_dir` unless (-d $model_dir);
	@digits = ("zero","one","two","three","four","five","seven","eight","nine");
	@models = ();
	foreach (@digits)
	{
		my $train_dir = File::Spec->catfile($data_dir, $_,"train");
		#my $model_dir = File::Spec->catfile($data_dir,"model");
		my $model_name = File::Spec->catfile($model_dir,$_.".m");
		#`mkdir $model_dir` unless (-d $model_dir);
		
		# run training process for each digit
		my $train_cmd = "./hmm.py -l -d $train_dir -m $model_name -s 5";
		
		`$train_cmd`; 
		print "$train_cmd\n";
		#push(@models,$model_name);
	}
	
}


sub decode
{
	my $model_dir = shift;
	my $ctl_fn = shift;
	my $result_fn = shift;
	
	my $list_of_models = `ls $model_dir`;
	
	my @models = split(/\n/,$list_of_models);
	foreach (@models)
	{
		print "$_\n";
	}
	#exit(0);
	open CTL, $ctl_fn or die "cannot open ctl file $ctl_fn";
	@test_data = <CTL>;
	close(CTL);
	open RESULT, ">$result_fn" or die "cannot open result file $result_fn";
	#$min_cost = 99999999999999;
	#$best_match = '';
	foreach $t (@test_data)
	{
		$min_cost = 99999999999999;
		$best_match = '';
		print $t;
		chomp($t);
		foreach my $m (@models)
		{
			print "m is $m\n";
			my $model = File::Spec->catfile($model_dir,$m);
			print "after cat, model is $model \n";
			$decode_cmd = "./hmm.py -t $t -m $model";
			print "decode_cmd = $decode_cmd\n";
			my $result = `$decode_cmd`;
			#print $result."\n";	
			$result =~ m/is (\d+\.\d+) \[/;
			my $cost = $1;
			print "SCORE = $cost\n\n\n";
			if ($cost < $min_cost)
			{
				print "find one!!!!!!\n";
				$min_cost = $cost;
				$best_match = $model;
			}
		}
		$best_match =~ m/\/([a-z]+)\.m/;	
		my $res = $1;
		print "best_match is $best_match\n";
		print RESULT "$res\t($t)\n";
	}
	close(RESULT);

}
