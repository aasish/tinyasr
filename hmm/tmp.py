import cPickle as pickle

class Temp:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def get_x(self):
        return self.x
    def get_y(self):
        return self.y


x = {"1": 1, "2" : 2}
y = (1, 2, 3)
z = ["1", "2", "3"]
"""
obj = Temp(x, y, z)
print obj.get_x(), obj.get_y()
print "now writing"
pickle.dump(obj, open('save.p','wb'))
"""
print "now reading"
reader = pickle.load(open('save.p', 'rb'))
print reader.x, reader.y
