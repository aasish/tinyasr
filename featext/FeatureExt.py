#!/usr/bin/python
import subprocess
import os
import numpy as np
#import pylab
#import matplotlib.pyplot as plt
#from mpl_toolkits.axes_grid1 import AxesGrid
import re
import argparse

SPHINXBASE = os.environ['SPHINXBASE']


SPHINXFE = os.path.join(SPHINXBASE,'sphinx_fe','sphinx_fe')
SPHINXCEPVIEW = os.path.join(SPHINXBASE, 'sphinx_cepview','sphinx_cepview')
#NFILTER = 40
LOWERFN = 50
UPPERFN = 4000
SAMPLE_RATE = 16000

if SAMPLE_RATE == 8000:
    LOWERFN = 50
    UPPERFN = 4000
else:
    LOWERFN = 50
    UPPERFN = 7000

WINTIME = 25
STEPTIME = 10
DITHER = 1
DELTA_WINDOW = 2
DOUBLE_DELTA_WINDOW = 4

def GetReadableMFCC(filename, NFILTER):
    mfc_file = GetMFCC(filename, NFILTER)
    mfc_output = '_'.join([mfc_file, 'view'])
    if os.path.isfile(mfc_output):
        subprocess.call(['rm','-f', mfc_output])
    command = ' '.join([SPHINXCEPVIEW, '-d', '13', '-f', mfc_file, '>', mfc_output, '2>/dev/null'])
    #print command
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    return mfc_output
    

def GetReadableLogMFCC(filename, NFILTER):
    log_mfc_file = GetLogMFCC(filename, NFILTER)
    log_mfc_output = '_'.join([log_mfc_file, 'view'])

    if os.path.isfile(log_mfc_output):
        subprocess.call(['rm', '-f', log_mfc_output])

    command = ' '.join([SPHINXCEPVIEW, '-i', str(NFILTER), '-d', str(NFILTER), '-f', log_mfc_file, '>', log_mfc_output, '2>/dev/null'])
    #print command
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    #return log_mfc_output

def GetMFCC(filename, NFILTER):
    mfc_file = ''.join([filename.split('.')[0],'.mfc'])
    if os.path.isfile('log'):
        subprocess.call(['rm', '-f', 'log'])

    if os.path.isfile(mfc_file):
        subprocess.call(['rm','-f', mfc_file])
        
    command = [SPHINXFE, '-i', filename, '-o', mfc_file, '-lowerf', str(LOWERFN), '-upperf', str(UPPERFN), '-nfilt', str(NFILTER), '-samprate', str(SAMPLE_RATE), '-transform', 'dct']
    #print ' '.join(command)
    fh = open("NUL","w")
    subprocess.call(command, stderr=fh, stdout=fh)
    
    return mfc_file
    


def GetLogMFCC(filename, NFILTER):
    log_mfc_file = ''.join([filename.split('.')[0],'.log_mfc'])
    
    if os.path.isfile(log_mfc_file):
        subprocess.call(['rm', '-f', log_mfc_file])

    if os.path.isfile('log'):
        subprocess.call(['rm', '-f', 'log'])
    
    command = [SPHINXFE, '-i', filename, '-o', log_mfc_file,'-logspec', 'yes', '-lowerf', str(LOWERFN), '-upperf', str(UPPERFN), '-nfilt', str(NFILTER), '-samprate', str(SAMPLE_RATE), '-transform', 'dct', '-smoothspec', 'yes', '> log', '2>/dev/null']
    #print filename, log_mfc_file
    #print ' '.join(command)
    subprocess.call(command)
    return log_mfc_file



def GetDeltas(c, w):
    S = c.shape
    d = np.zeros((S[0],S[1]))
    for n in range(0,S[1]):
        d[:,n] = c[:,(n+w/2) % S[1]] - c[:,n-w/2] #negative indices wrap around
    d = d/w
    return d

#pylab.specgram(x, NFFT=256, Fs=2, Fc=0, detrend=mlab.detrend_none, window=mlab.window_hanning, noverlap=128, cmap=None, xextent=None, pad_to=None, sides='default', scale_by_freq=None, **kwargs)

def cmn(C):
      m = np.mean(C,1)
      for i in range(0,12):
          C[i,:] = C[i,:] - m[i]
  
      return C

def GetEverything(filename, NFILTER):
    output_file = GetReadableMFCC(filename, NFILTER)

    cols = 12
    start = 1
    title = 'Mel Cepstra with ' +  str(NFILTER) + ' filters'
    x = np.loadtxt(output_file, usecols=(1,2,3,4,5,6,7,8,9,10,11,12))
    #print x
    #x = np.loadtxt(output_file)
    x = x.T
    x_shape = x.shape
    #print x_shape
    #x = cmn(x)
    d1 = GetDeltas(x, DELTA_WINDOW)
    d2 = GetDeltas(d1, DOUBLE_DELTA_WINDOW)
    C_out = np.zeros((3 * cols, x_shape[1]))
    #print C_out.shape
    C_out[0:cols,:] = x
    C_out[cols:2*cols] = d1
    C_out[2*cols:3*cols] = d2
    #print x
    #print x.shape, d1.shape, d2.shape
    return output_file, C_out.T

    
def PlotTheFeatures(filename, isLog, NFILTER):
    output_file = ''
    cols = 12
    start = 1
    title = ''
    if isLog:
        output_file = GetReadableLogMFCC(filename, NFILTER)
        cols = NFILTER
        start = 0
        title = 'Log spectra with ' + str(NFILTER) + ' filters'
        x = np.loadtxt(output_file)
    else:
        output_file = GetReadableMFCC(filename, NFILTER)
        cols = 12
        start = 1
        title = 'Mel Cepstra with ' +  str(NFILTER) + ' filters'
        x = np.loadtxt(output_file, usecols=(1,2,3,4,5,6,7,8,9,10,11,12))

    #lines = open(output_file).readlines()
    #x = np.zeros((len(lines), cols), float)
        
    #x = np.loadtxt(output_file)
    
    """
    for i  in range(0, len(lines)):
         lines[i] = re.sub("\s+" , " ", lines[i]).strip()
         x[i] = list(map(float, lines[i].split(' ')))[start:]
        
    
    """
    
    x = x.T
    #pylab.specgram(x,NFFT=512,Fs=2,noverlap=128)
    
    pylab.imshow(x, origin='lower',aspect='auto', interpolation="nearest")
    # pylab.title(title)
    pylab.show()
    return x, title

#MapTheFeatures("demo-0.wav")
#print GetReadableLogMFCC("demo-0.wav")

def PlotAGrid(filename, isLog):

    filt = [40, 25, 16]
    for i in range(3):
        plt.subplot(3,1,i+1)
        Z, title = PlotTheFeatures(filename, isLog, filt[i])
        plt.imshow(Z, origin='lower', aspect='auto', interpolation="nearest")
        plt.title(title)
        #grid[i].title(title)
    # This only affects axes in first column and second row as share_all = False.

    pylab.draw()
    pylab.show()


def main():
    parse = argparse.ArgumentParser()
    parse.add_argument('-l',  '--logspec', action="store_true", dest="logspec", default=False, help='logspec default=False')
    parse.add_argument('-f', '--input', action="store", dest="filename", help='input_type eg. -f input_filename')
    parse.add_argument('-n', '--nfilter', action="store", dest="nfilter", default=40, help='filter eg. -f input_filename')
    parse.add_argument('-o', '--output-dir', action="store", dest="output_dir", required=True, help='output directiory for the feature files')

    results = parse.parse_args()
    NFILTER = int(results.nfilter)
    filename =results.filename
    
    output_dir = results.output_dir
    if results.logspec:
        output_file = GetReadableLogMFCC(filename, NFILTER)
        #print "writing to %s" %(output_file)


    else:
        output_file = GetReadableMFCC(filename, NFILTER)
        output_file, x = GetEverything(filename, NFILTER)
        output_file = filename.strip('.wav') + output_file
        output_file = os.path.join(output_dir, output_file.split('/')[-1])
        print "saving the file %s" %(output_file)
        np.savetxt(open(output_file, 'w'), x)


    
if __name__ == "__main__":
    main()
